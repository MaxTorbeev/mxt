<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRedirectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redirects', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->nullable();
            $table->string('redirect_from');
            $table->string('redirect_to')->nullable();
            $table->unsignedInteger('subject_id')->nullable();
            $table->string('subject_type', 50)->nullable()->index();

            $table->userManagement();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redirects');
    }
}
