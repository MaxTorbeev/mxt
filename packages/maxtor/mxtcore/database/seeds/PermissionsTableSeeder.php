<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name' => 'access_dashboard',
                'label' => 'Доступ в панель управления',
                'created_user_id' => 1
            ],
            // Menu
            [
                'name' => 'show_menu_item',
                'label' => 'Просмотр пунктов меню',
                'created_user_id' => 1
            ],
            [
                'name' => 'create_menu_item',
                'label' => 'Создание пункта меню',
                'created_user_id' => 1
            ],
            [
                'name' => 'delete_menu_item',
                'label' => 'Удаление пункта меню',
                'created_user_id' => 1
            ],
            [
                'name' => 'show_menu_type',
                'label' => 'Просмотр типов меню',
                'created_user_id' => 1
            ],
            [
                'name' => 'create_menu_type',
                'label' => 'Создание типа меню',
                'created_user_id' => 1
            ],
            [
                'name' => 'delete_menu_type',
                'label' => 'Удаление типа меню',
                'created_user_id' => 1
            ],

            // Users
            [
                'name' => 'create_user',
                'label' => 'Создание нового пользователя',
                'created_user_id' => 1
            ],
            [
                'name' => 'delete_user',
                'label' => 'Удаление пользователя',
                'created_user_id' => 1
            ],
        ]);
    }
}
