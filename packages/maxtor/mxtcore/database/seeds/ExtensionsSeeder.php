<?php

use Illuminate\Database\Seeder;

class ExtensionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('extensions')->insert([]);
    }
}
