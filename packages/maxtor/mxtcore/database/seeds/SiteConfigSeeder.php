<?php

use Illuminate\Database\Seeder;

class SiteConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('site_configs')->insert([
            [
                'name' => 'site_name',
                'label' => 'Название сайта',
                'value' => 'Евростройхолдинг Казань',
                'extra_attributes' => null
            ],
            [
                'name' => 'metakey',
                'label' => 'Ключевые слова',
                'value' => 'Евростройхолдинг Казань, Деревянное домостроительство Казань, фальцевая кровля Казань',
                'extra_attributes' => null
            ],
            [
                'name' => 'metadesc',
                'label' => 'Мета-описание',
                'value' => 'Строительно-монтажные работы, выполняемые компанией, включают в себя полный комплекс необходимых мероприятий',
                'extra_attributes' => null
            ],
            [
                'name' => 'mainpage_header_phone',
                'label' => 'Номер телефона в шапке сайта на главной странице',
                'value' => '+79196214478',
                'extra_attributes' => null
            ],
            [
                'name' => 'company_address',
                'label' => 'Адрес',
                'value' => 'г. Казань, ул. Петра Витера',
                'extra_attributes' => null
            ],
            [
                'name' => 'feedback_disclaimer',
                'label' => 'Дисклеймер формы обратной связи',
                'value' =>
                    '
                    Нажимая кнопку «%s», я принимаю условия Пользовательского соглашения и даю
                    своё согласие сайту на обработку моих персональных данных, в соответствии с Федеральным
                    законом от 27.07.2006 года №152-ФЗ «О персональных данных», на условиях и для целей,
                    определенных Политикой конфиденциальности.
                    ',
                'extra_attributes' => null
            ],
            [
                'name' => 'company_phones',
                'label' => 'Номера телефонов компании',
                'value' => null,
                'extra_attributes' => json_encode([
                    'common' => [
                        'name' => 'common',
                        'label' => 'По общим вопросам клиентов',
                        'value' => '+79196214478',
                    ],
                    'roof' => [
                        'name' => 'roof',
                        'label' => 'Отдел продаж фальцевой кровли',
                        'value' => '+78432154434',
                    ],
                    'wooden-house-building' => [
                        'name' => 'wooden-house-building',
                        'label' => 'Отдел продаж деревянного домостроения',
                        'value' => '+78432164970',
                    ],
                    'rental-special-equipment' => [
                        'name' => 'rental-special-equipment',
                        'label' => 'Отдел продаж аренды спецтехники',
                        'value' => '+79196214478',
                    ],
                    'ventilation' => [
                        'name' => 'ventilation',
                        'label' => 'Отдел продаж вентиляции',
                        'value' => '+78432154434',
                    ],
                ])
            ]
        ]);
    }
}
