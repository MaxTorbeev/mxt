<?php

namespace MaxTor\MXTCore\Tests\Feature;

use App\User;
use Illuminate\Support\Facades\Hash;
use MaxTor\Personal\Models\Contact;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateUserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Гость и обычный пользователь без прав не может создать тип меню.
     *
     * @test
     */
    function guest_may_not_created_user()
    {
        $this->withExceptionHandling();

        $this->get('admin/users/create')->assertRedirect('/login');
        $this->post('admin/users')->assertRedirect('/login');
    }

    /** @test */
    function new_user_automatic_create_contact_here()
    {
        $this->withExceptionHandling();

        $user = create(User::class, ['name' => 'maxtor']);

        $this->assertInstanceOf(contact::class, $user->contact);
    }

    /**
     * Пользователь может посмотреть список пользователей
     *
     * @test
     */
    function auth_user_can_see_user_list()
    {
        $this->withOutExceptionHandling();

        $this->signIn(create(User::class), 'root', ['access_dashboard']);

        $this->get('/admin/users')->assertStatus(200);
    }

    /**
     * Пользователь с правами может создать нового пользователя
     *
     * @test
     */
    public function an_authenticated_user_can_create_new_user()
    {
        $this->withOutExceptionHandling();

        $this->signIn(create(User::class), 'root', ['access_dashboard', 'create_user']);

        $this->get('/admin/users/create')->assertStatus(200);

        $user = make(User::class, [
            'name' => 'maxtor',
            'email' => '1@1.ru',
            'new_password' => 'secret',
            'new_password_confirmation' => 'secret'
        ]);

        $response = $this->post('/admin/users', $user->toArray());

        $this->get($response->headers->get('Location'))
            ->assertSee($user->name);
    }


}
