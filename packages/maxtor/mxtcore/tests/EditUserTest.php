<?php

namespace MaxTor\MXTCore\Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EditUserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Пользователь с правами может изменить пользователя.
     *
     * @test
     */
    function an_authenticated_user_can_edit_user()
    {
        $this->withExceptionHandling();

        $this->signIn(create(User::class), 'root', ['access_dashboard', 'create_user']);

        $user = create(User::class, [
            'email' => '1@1.ru'
        ]);

        $userEditable = make(User::class, [
            'name' => 'Вася',
            'email' => $user->email
        ]);

        $this->get('/admin/users/' . $user->id . '/edit')->assertStatus(200);

        $response = $this->call('PATCH', '/admin/users/' . $user->id, $userEditable->toArray());

        $this->assertDatabaseHas('users', [
            'name' => $userEditable->name
        ]);
    }

    /**
     * Пользователь с правами может удалить пользователя.
     *
     * @test
     */
    function an_authenticated_user_can_delete_user()
    {
        $this->withOutExceptionHandling();

        $user = create(User::class, ['email' => '1@1.ru']);

        $this->call('DELETE', '/admin/users/' . $user->id);

        $this->assertDatabaseHas('users', ['email' => '1@1.ru']);

        $this->signIn(create(User::class), 'root', ['access_dashboard', 'delete_user']);
        $this->call('DELETE', '/admin/users/' . $user->id);

        $this->assertDatabaseMissing('users', ['email' => '1@1.ru']);
    }

}
