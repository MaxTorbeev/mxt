<?php
/**
 * Configure file for MXTCore
 */
return [
    'baseUrl'       => 'sk-atrium.local',
    'title'         => 'СК «Атриум»',
    'keywords'      => 'Благоустройство, ландшафтный дизайн',
    'description'   => 'Ландшафтный дизайн',
    'generator'     => 'MXTCore'
];
