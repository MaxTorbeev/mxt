<div class="form-group form-row mb-1">
    <label class="col-sm-2 col-form-label col-form-label-sm">{{ $field['name'] }}</label>
    <input type="hidden" name="extra_attributes[{{ $field['name'] }}][type]" value="{{ $field['type'] }}">
    <input type="hidden" name="extra_attributes[{{ $field['name'] }}][name]" value="{{ $field['name'] }}">
    <div class="col-sm-6">
        <input type="text" value="{{ $field['label'] }}" name="extra_attributes[{{ $field['name'] }}][label]" class="form-control form-control-sm">
    </div>
    <div class="col-sm-4">
        <input type="text" value="{{ $field['value'] }}"  name="extra_attributes[{{ $field['name'] }}][value]"  class="form-control form-control-sm">
    </div>
</div>
