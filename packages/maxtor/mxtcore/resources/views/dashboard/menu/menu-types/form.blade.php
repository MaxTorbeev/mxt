<div class="form-group row">
    {!! Form::label('name', 'Заголовок:', ['class' => 'col col-md-2 col-form-label']) !!}
    <div class="col col-xs-10">
        {!! Form::text('name', $menuType->name, ['class'=>'form-control']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('slug', 'Псевдоним:', ['class' => 'col col-md-2 col-form-label']) !!}
    <div class="col col-xs-10">
        {!! Form::text('slug', $menuType->slug, ['class'=>'form-control']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('description', 'Описание:', ['class' => 'col col-md-2 col-form-label']) !!}
    <div class="col col-xs-10">
        {!! Form::text('description', $menuType->description, ['class'=>'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::submit( $submitButtonText, ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.menu-types.index') }}" class="btn btn-dark">К списку</a>
</div>

