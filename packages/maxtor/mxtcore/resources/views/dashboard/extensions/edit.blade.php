@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.extensions.edit', $extension) }} @endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <i class="fa fa-plus"></i> Редактировать расширение
        </div>
        <div class="card-body">
            {!! Form::model($extension, ['url' => route('admin.extensions.update', $extension)]) !!}
            @method('PUT')
            @include ('mxtcore::dashboard.extensions.form', ['submitButtonText' => 'Редактировать расширение' ])
            {!! Form::close() !!}
        </div>
    </div>
@endsection