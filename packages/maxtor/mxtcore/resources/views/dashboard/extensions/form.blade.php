
<div class="row">
    <div class="col col-md-8">
        <div class="form-group row {{ $errors->has('name') ? 'has-danger' : '' }}">
            {!! Form::label('name', 'Имя расширения:', ['class' => 'col col-xs-4 col-form-label']) !!}
            <div class="col col-xs-8">
                {!! Form::text('name', $extension->name, ['class'=>'form-control']) !!}
                @if ($errors->has('name'))
                    <small class="form-control-feedback">{{ $errors->first('name') }}</small>
                @endif
            </div>
        </div>

        <div class="form-group row {{ $errors->has('controller_path') ? 'has-danger' : '' }}">
            {!! Form::label('controller', 'Используемый контроллер:', ['class' => 'col col-xs-4 col-form-label']) !!}
            <div class="col col-xs-8">
                {!! Form::text('controller', $extension->controller, ['class'=>'form-control']) !!}
                @if ($errors->has('controller'))
                    <small class="form-control-feedback">{{ $errors->first('controller') }}</small>
                @endif
            </div>
        </div>

        <div class="form-group row {{ $errors->has('enabled') ? 'has-danger' : '' }}">
            {!! Form::label('enable', 'Включить расширение? ', ['class' => 'col col-xs-4 col-form-label']) !!}
            <div class="col col-xs-8">
                {!! Form::select('published', ['0' => 'Нет', '1' => 'Да'], $extension->published, ['class'=>'form-control select']) !!}
                @if ($errors->has('published'))
                    <small class="form-control-feedback">{{ $errors->first('enabled') }}</small>
                @endif
            </div>
        </div>

        <div class="form-group">
            {!! Form::submit( $submitButtonText, ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('admin.extensions.index') }}" class="btn btn-dark">Отменить и перейти к списку</a>
        </div>
    </div>
</div>


