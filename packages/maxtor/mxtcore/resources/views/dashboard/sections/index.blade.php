@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.sections') }} @endsection

@section('content')

    <ul class="nav nav-pills mb-2">
        <li class="nav-item">
            <a class="nav-link btn btn-success" href="{{ route('admin.sections.create') }}">Добавить секцию сайта</a>
        </li>
    </ul>

    <div class="card">
        <div class="card-header">
            Секции сайта
        </div>

        <div class="card-body">
            @include('mxtcore::dashboard.system.data.table', [
                'models' => $sections,
                'attributes' => [
                    'priority' => 'Приоритет'
                ], 'routes' => [
                    'edit_route' => 'admin.sections.edit',
                    'delete_route' => 'admin.sections.destroy',
                ]
            ])
        </div>
    </div>
@endsection