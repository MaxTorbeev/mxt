<table class="table table-sm table-responsive-sm table-hover table-outline mb-0">
    <thead class="thead-inverse">
        <tr>
            @isset($routes['edit_route'])
                <th></th>
            @endisset
            <th>Код</th>
            <th>Название</th>

            @foreach($attributes as $attribute => $value)
                <th>{{ $value }}</th>
            @endforeach

            <th>Опубликовано</th>
            <th>Дата ред.</th>
            <th>Дата создания</th>
            @isset($routes['delete_route'])
                <th></th>
            @endisset
        </tr>
    </thead>

    <tbody>
        @foreach($models as $model)
            <tr>
                @isset($routes['edit_route'])
                <td>
                    <a href="{{ route($routes['edit_route'], ['id' => $model->id]) }}" class="btn btn-sm btn-success">
                        <i class="fa fa-pencil-square-o"></i>
                    </a>
                </td>
                @endisset

                <td>{{ $model->id }}</td>
                <td>
                    <a href="{{ route($routes['edit_route'], ['id' => $model->id]) }}">
                        {{ $model->name }}
                    </a>
                </td>

                @foreach($attributes as $attribute => $value)
                    @if($attribute == 'author')
                        @isset($model->author)
                            <td>{{ $model->author->name }}</td>
                        @endisset
                    @elseif($attribute == 'category_id')
                        @isset($model->category)
                            <td>{{ $model->category->name }}</td>
                        @endisset
                    @else
                        <td>{{ $model->{$attribute} }}</td>
                    @endif
                @endforeach

                <td>
                    @if($model->published)
                        <span class="badge badge-success">
                            @lang('mxtcore.published')
                        </span>
                    @else
                        <span class="badge badge-danger">
                            @lang('mxtcore.unpublished')
                        </span>
                    @endif
                </td>
                <td>{{ $model->updated_at }}</td>
                <td>{{ $model->created_at }}</td>
                @isset($routes['delete_route'])
                    <td>
                        <delete action="{{ route($routes['delete_route'], ['id' => $model->id]) }}">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </delete>
                    </td>
                @endisset
            </tr>
        @endforeach
        {{--{{ dd($model) }}--}}
    </tbody>
</table>