@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.users.create') }} @endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Новый пользователь
        </div>

        <div class="card-body">
            {!! Form::model($user = new \App\User(), ['url' => route('admin.users.store')]) !!}
            @include ('mxtcore::dashboard.users.form', ['submitButtonText' => 'Создать нового пользователя пользователя' ])
            {!! Form::close() !!}
        </div>
    </div>

@endsection