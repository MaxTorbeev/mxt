@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.users') }} @endsection

@section('content')

    <nav class="navbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="btn btn-success" href="{{ route('admin.users.create') }}">Новый пользователь</a>
            </li>
        </ul>
    </nav>

    <div class="card">
        <div class="card-header">
            Пользователи
        </div>
        <div class="card-body">
            {!! $table->build() !!}
            {{--@include('mxtcore::dashboard.system.data.table', [ 'models' => $users, 'attributes' => [], 'routes' => [--}}
                    {{--'edit_route' => 'admin.users.edit',--}}
                {{--]--}}
            {{--])--}}
        </div>
    </div>
@endsection