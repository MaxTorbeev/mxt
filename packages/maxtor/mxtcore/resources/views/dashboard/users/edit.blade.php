@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.users.edit', $user) }} @endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Редактирование пользователя «{{ $user->name }}»
        </div>

        <div class="card-body">
            {!! Form::model($user, ['url' => route('admin.users.update', ['id' => $user->id])]) !!}
            @method('PUT')
            @include ('mxtcore::dashboard.users.form', ['submitButtonText' => 'Редактировать пользователя' ])
            {!! Form::close() !!}
        </div>
    </div>
@endsection