@extends('mxtcore::layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-header">
            Новое значение
        </div>
        <div class="card-body">
            {!! Form::model($config = new \MaxTor\MXTCore\Models\SiteConfig(), ['url' => route('admin.site-configs.store')]) !!}
            @include ('mxtcore::dashboard.site-configs.form', ['submitButtonText' => 'Добавить новое значение' ])
            {!! Form::close() !!}
        </div>
    </div>
@endsection