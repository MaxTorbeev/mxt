<div class="form-row">
    <div class="form-group col-md-4 {{ $errors->has('name') ? 'has-danger' : '' }}">
        {!! Form::label('name', 'Имя (англ., без пробелов):') !!}
        {!! Form::text('name', $config->title, ['class'=>'form-control']) !!}
        @if ($errors->has('name'))
            <small class="form-control-feedback">{{ $errors->first('name') }}</small>
        @endif
    </div>
    <div class="form-group col-md-4 {{ $errors->has('label') ? 'has-danger' : '' }}">
        {!! Form::label('label', 'Метка:') !!}
        {!! Form::text('label', $config->label, ['class'=>'form-control']) !!}
        @if ($errors->has('label'))
            <small class="form-control-feedback">{{ $errors->first('label') }}</small>
        @endif
    </div>
</div>

<div class="form-group {{ $errors->has('value') ? 'has-danger' : '' }}">
    {!! Form::label('value', 'Значение:') !!}
    {!! Form::textarea('value', $config->value, ['class'=>'form-control']) !!}
    @if ($errors->has('value'))
        <small class="form-control-feedback">{{ $errors->first('value') }}</small>
    @endif
</div>

@if($config->exists)
    @if (view()->exists("mxtcore.configs.{$config->name}"))
        @include("mxtcore.configs.{$config->name}", compact('config'))
    @else
        @include("mxtcore.configs.default", compact('config'))
    @endif
@endif

<div class="panel form_buttonGroups form_buttonGroups-fixed">
    <div class="form-group">
        {!! Form::submit( $submitButtonText, ['class' => 'btn btn-primary']) !!}
        <button type="submit" name="action" value="save_and_close" class="btn btn btn-success">
            Сохранить и перейти к списку
        </button>
        <button type="submit" name="action" value="save_and_create" class="btn btn btn-success">
            Сохранить и создать новый
        </button>
        <a href="{{ route('admin.site-configs.index') }}" class="btn btn btn-warning text-right">
            Не сохранять и перейти к списку
        </a>
        @if($config->exists)
            <button type="submit" name="action" value="delete" class="pull-right btn btn btn-danger">Удалить</button>
        @endif
    </div>
</div>
