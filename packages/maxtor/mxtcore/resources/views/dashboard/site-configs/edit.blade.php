@extends('mxtcore::layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-header">
            Редактировать значение: {{ $config->label }}
        </div>
        <div class="card-body">
            {!! Form::model($config, ['url' => route('admin.site-configs.update', $config)]) !!}
            {{ method_field('PUT') }}
            @include ('mxtcore::dashboard.site-configs.form', ['submitButtonText' => 'Редактировать значение' ])
            {!! Form::close() !!}
        </div>
    </div>
@endsection