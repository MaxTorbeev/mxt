@extends('mxtcore::layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-header">
            Настройки сайта
            <a class="btn btn-success btn-sm" href="{{ route('admin.site-configs.create') }}">
                Добавить значение
            </a>
        </div>
        <div class="card-body">
            {!! $table->build() !!}
        </div>
    </div>
@endsection