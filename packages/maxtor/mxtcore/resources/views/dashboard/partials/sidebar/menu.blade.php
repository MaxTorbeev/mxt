<li class="nav-item">
    @if($item->url_path)
        <a href="{{ $item->url_path }}" class="nav-link">
            {{ str_repeat("--", $item->depth) }} {{$item->name}}
        </a>
    @else
        <span class="nav-link">{{ str_repeat("--", $item->depth) }} {{ $item->name }}</span>
    @endif
</li>
