<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="{{ route('dashboard') }}">
        <img class="navbar-brand-full" src="{{ config('mxtcore.dashboard.logo.full') }}" width="140" alt="{{ config('mxtcore.title', 'MaxTor blog') }}">
        <img class="navbar-brand-minimized" src="{{ config('mxtcore.dashboard.logo.minimized') }}" width="30" height="30" alt="{{ config('mxtcore.title', 'MaxTor blog') }}">
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
            <a class="nav-link" href="{{ route('admin.menu.index') }}">Пункты меню</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="{{ route('admin.users.index') }}">Пользователи</a>
        </li>

        <li class="nav-item px-3">
            <a href="{{ route('home') }}" class="nav-link" target="_blank">
                Перейти на сайт
            </a>
        </li>
    </ul>

    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                {{--{{ auth()->user()->name }}--}}
            </a>

            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    <strong>Account</strong>
                </div>

                <a class="dropdown-item" href="{{ url('/logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    Выход
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                </a>
            </div>
        </li>
    </ul>
    <button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
        <span class="navbar-toggler-icon"></span>
    </button>
</header>