<?php

namespace MaxTor\MXTCore\Forms;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use MaxTor\Forms\FormInterface;

abstract class AbstractForm
{
    protected $fields = [];

    protected function associate($attributes)
    {
        if(isset($attributes)){
            foreach ($this->fields as $fieldKey => $field) {
                foreach ($attributes as $attr){
                    if($field['name'] == $attr['name']){
                        $this->fields[$fieldKey] = $attr;
                    }
                }
            }
        }

        return $this->fields;
    }

    public function build()
    {
        $fields = '';

        if (isset($this->fields)) {
            foreach ($this->fields as $field) {
                switch ($field['type']){
                    case 'text':
                        $fields .= view('mxtcore::layouts.form.text', compact('field'))->render();
                        break;
                }
            }
        }

        return $fields;
    }
}
