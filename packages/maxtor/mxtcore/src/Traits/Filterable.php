<?php

namespace MaxTor\MXTCore\Traits;

trait Filterable
{
    public function scopeFilters($query, $filters)
    {
        return $filters->apply($query);
    }
}