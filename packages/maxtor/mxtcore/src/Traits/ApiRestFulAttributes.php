<?php

namespace MaxTor\MXTCore\Traits;

trait ApiRestFulAttributes
{
    public function getRestCreateAttribute()
    {
        return route('admin.' . self::getTable() . '.create');
    }

    public function getRestUpdateAttribute()
    {
        return route('admin.' . self::getTable() . '.update', $this);
    }

    public function getRestDestroyAttribute()
    {
        return route('admin.' . self::getTable() . '.destroy', $this);
    }
}