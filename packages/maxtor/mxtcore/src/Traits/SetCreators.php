<?php

namespace MaxTor\MXTCore\Traits;
use App\User;
use Illuminate\Support\Facades\App;

/**
 * Trait SetCreators.
 *
 * Заполняем поля id пользователя создателя и пользователя модификатора
 *
 * @deprecated
 * @package MaxTor\MXTCore\Traits
 */
trait SetCreators
{
    protected static function bootSetCreators()
    {

        if (!app()->environment('testing')) {
            static::creating(function ($model) {
                $model->created_user_id = auth()->user()->id;
            });

            static::updating(function ($model) {
                $model->modified_user_id = auth()->user()->id;
            });
        }
    }

    /**
     * An post is owned by a user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'created_user_id');
    }
}