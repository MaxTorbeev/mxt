<?php

namespace MaxTor\MXTCore\Traits;

use App\User;
use Illuminate\Support\Facades\App;

trait Viewable
{
    use \CyrildeWit\EloquentViewable\Viewable;

    public function getViewsCountAttribute()
    {
        return views($this)->count();
    }
}