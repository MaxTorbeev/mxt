<?php

namespace MaxTor\MXTCore\Controllers;

use Illuminate\Http\Request;
use MaxTor\MXTCore\Layouts\SiteConfigLayout;
use MaxTor\MXTCore\Models\SiteConfig;
use MaxTor\MXTCore\MXTCorePackageHelper;
use MaxTor\MXTCore\Requests\SiteConfigRequests;

final class SiteConfigsController extends AbstractAdminController
{
    protected $helper;

    public function __construct(MXTCorePackageHelper $helper)
    {
        $this->helper = $helper;
    }

    public function index()
    {
        $configs = SiteConfig::all();
        $table = new SiteConfigLayout($configs);

        return $this->view('index', compact('table'));
    }

    public function create()
    {
        return $this->view('create');
    }

    public function store(SiteConfigRequests $request)
    {
        $config = SiteConfig::create($request->all());

        if($request->get('action') == 'save_and_close')
            return redirect(route('admin.site-configs.index'))->with('flash', 'Сохранено');

        if($request->get('action') == 'save_and_create')
            return redirect(route('admin.site-configs.create'))->with('flash', 'Сохранено и создаем новое');

        return redirect(route('admin.site-configs.edit', ['site-configs' => $config->id]))
            ->with('flash', 'Успешно сохранено');
    }

    public function edit($id)
    {
        $config = SiteConfig::whereId($id)->firstOrFail();

        return $this->view('edit', compact('config'));
    }

    public function update($id, SiteConfigRequests $request)
    {
        $config = SiteConfig::whereId($id)->firstOrFail();

        $config->update($request->all());

        if($request->get('action') == 'save_and_close')
            return redirect(route('admin.site-configs.index'))->with('flash', 'Сохранено');

        if($request->get('action') == 'save_and_create')
            return redirect(route('admin.site-configs.create'))->with('flash', 'Сохранено и создаем новое');

        return redirect(route('admin.site-configs.edit', ['site-configs' => $config->id]))
            ->with('flash', 'Успешно сохранено');
    }

    public function destroy(SiteConfig $config)
    {
        if($config->delete()) {
            return redirect(route('admin.site-configs.index'))->with('flash', 'Успешно удалено');
        }
    }
}