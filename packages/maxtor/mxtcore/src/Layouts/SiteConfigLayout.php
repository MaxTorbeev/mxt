<?php

namespace MaxTor\MXTCore\Layouts;

use App\User;
use MaxTor\Content\Models\Category;
use MaxTor\Content\Models\Carousel;
use MaxTor\MXTCore\Layouts\AbstractTable;
use MaxTor\MXTCore\Fields\TD;

class SiteConfigLayout extends AbstractTable
{
    public $data = '';

    public function fields(): array
    {
        return [
            TD::name('id')
                ->title('Код')
                ->setRender(function ($model) {
                    return '<a href="' . route('admin.site-configs.edit', ['site-configs' => $model['id']]) . '">'
                        . $model['id'] .
                        '</a>';
                }),
            TD::name('label')->width(200),
            TD::name('name')
                ->title('Ключ')
                ->setRender(function ($model) {
                    return '<a href="' . route('admin.site-configs.edit', ['site-configs' => $model['id']]) . '">'
                        . $model['name'] .
                        '</a>';
                })
            ,
            TD::name('value')->title('Значение')->width(550),
            TD::name('updated_at')->title('Обновлено'),
            TD::name('created_at')->title('Создано'),
        ];
    }
}