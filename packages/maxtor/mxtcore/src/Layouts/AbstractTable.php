<?php

namespace MaxTor\MXTCore\Layouts;

use Faker\Test\Provider\Collection;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\View;
use MaxTor\MXTCore\Screen\Repository;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

abstract class AbstractTable
{
    /**
     * Template table layout.
     *
     * @var string
     */
    public $template = 'mxtcore::layouts.table.table';

    public $data;

    public $model;

    public $permissions = null;

    public $routePostfix = ['index', 'create'];
    public $routePrefix;
    public $routeParameters;

    public function __construct($model = null, $routePrefix = null, $routeParameters = null)
    {
        $this->model = $model;
        $this->routePrefix = $routePrefix;
        $this->routeParameters = $routeParameters;

        $this->checkPermissions();
    }

    /**
     * Building visual table.
     * @todo create filters form builder
     */
    public function build()
    {
        if (method_exists($this, 'getDefaultModel')) {
            $this->model = $this->getDefaultModel();
        }

        $form = $this->generatedTable($this->model);
//        $filters = $this->showFilterDashboard();

        return view($this->template, compact('form'));
    }

    /**
     * @todo create filters form builder
     * @return array
     */
    public function toArray()
    {
        $rows = [];

        foreach ($this->generatedTable($this->model)['data'] as $key => $datum) {
            foreach ($this->generatedTable($this->model)['fields'] as $tdKey => $td) {
                if (isset($td->render) && !is_null($td->render)) {
                    $rows[$key][$td->title]['urls'] = $this->routesToUrls();

                    $rows[$key][$td->title] = $td->handler($datum);
                } else {
                    $rows[$key][$td->title]['urls'] = $this->routesToUrls();
                    $rows[$key][$td->title] = array_get($datum, $td->name);
                }
            }
        }

        return [
            'table' => [
                'thead' => $this->generatedTable($this->model)['fields'],
                'rows' => $rows
            ]
        ];
    }

    /**
     * @return array
     */
    public function filters(): array
    {
        return [];
    }

    /**
     * Генерация таблицы.
     *
     * @todo Разобраться с пагинацией, убрать лишние переменные.
     *
     * @param $model
     * @return array
     */
    private function generatedTable($model): array
    {
        $paginate = null;

        switch (get_class($model)) {
            case 'Illuminate\Pagination\LengthAwarePaginator':
                $paginate = $model;
                $modelArray = $paginate->toArray()['data'];
                break;

            default:
                $modelArray = $model->toArray();
                break;
        }

        $data = new Repository($modelArray);

        return [
            'data' => $data->getContent(),
            'fields' => $this->fields(),
            'paginateLinks' => $paginate === null ? null : $paginate->links()
        ];
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [];
    }

    public function publishedBadgeHtml($model)
    {
        return $model['enabled'] == 0
            ? '<span class="badge badge-danger">' . __('mxtcore.unpublished') . '</span>'
            : '<span class="badge badge-success">' . __('mxtcore.published') . '</span>';
    }

    public function deleteButtonHtml($action)
    {
        return '<delete action="' . $action . '"><i class="fa fa-trash" aria-hidden="true"></i></delete>';
    }

    /**
     * Check permissions
     */
    protected function checkPermissions(): void
    {
        if ($this->permissions !== null) {
            foreach ($this->permissions as $permission) {
                if (!Gate::allows($permission)) {
                    throw new AccessDeniedHttpException();
                }
            }
        }
    }

    protected function routesToUrls()
    {
        $urls = [];

        if(!$this->routePrefix)
            return [];

        foreach ($this->routePostfix as $postfix){
            $urls[$postfix] = route($this->routePrefix . '.' . $postfix);
        }

        return $urls;
    }
}