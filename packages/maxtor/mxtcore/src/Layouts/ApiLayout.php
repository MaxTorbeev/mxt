<?php

namespace MaxTor\MXTCore\Layouts;

use MaxTor\Content\Models\Category;

class ApiLayout
{
    /**
     * Layout type name.
     *
     * @var $type - Layout type.
     * @see config/layouts.php
     */
    public $type;

    /**
     * @var $name - array layout data ['layout', 'filters', 'model']
     */
    public $name;

    public function __construct($type, $name)
    {
        $this->type = $type;
        $this->name = $name;
    }

    public function handle()
    {
        return $this->getLayout();
    }

    protected function getLayout()
    {
        $layout = config("layouts.{$this->type}.{$this->name}.layout", null);

        if($layout && class_exists($layout)){
            return (new $layout($this->getModel()))->getJson();
        }

        return response(['message' => __('layouts.errors.classNotFound', compact('className'))], 403);
    }

    /**
     * Get config with filters.
     *
     * @return \Illuminate\Config\Repository|mixed|null
     */
    protected function getModel()
    {
        $model = config("layouts.{$this->type}.{$this->name}.model", null);

        if($model && class_exists($model)){
            if($this->getFilters()){
                return  $model::latest()->filters($this->getFilters())->get() ;
            }

            return $model::all();
        }

        return null;
    }

    /**
     * Get filters in config file
     *
     * @return \Illuminate\Config\Repository|mixed|null
     */
    protected function getFilters()
    {
        $filters = config("layouts.{$this->type}.{$this->name}.filter", null);

        if($filters && class_exists($filters)){
            return (new $filters(request()));
        }

        return null;
    }
}