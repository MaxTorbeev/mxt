<?php

namespace MaxTor\MXTCore\Layouts;

use MaxTor\MXTCore\Fields\Builder;

abstract class AbstractRows
{
    /**
     * @var string
     */
    public $template = 'mxtcore::layouts.table.row';

    /**
     * @param $post
     *
     * @throws \Throwable
     *
     * @return array
     */
    public function build($post)
    {
        $form = new Builder($this->fields(), $post);

        return view($this->template, ['form' => $form->generateForm()])->render();
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [];
    }
}