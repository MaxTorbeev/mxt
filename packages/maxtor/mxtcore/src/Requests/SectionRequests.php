<?php

namespace MaxTor\MXTCore\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SectionRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
            case 'PATCH':
                return [
                    'name' => 'required|max:191',
                    'slug' => 'required|unique:sections,slug,' . $this->section->id,
                ];
            case 'POST':
            default:
            return [
                'name'              => 'required',
            ];
        }

    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'              => 'Имя должено быть указано',
//            'slug.unique'                => 'Псевдоним должен быть уникальным',
        ];
    }
}
