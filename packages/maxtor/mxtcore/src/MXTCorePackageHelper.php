<?php

namespace MaxTor\MXTCore;

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use MaxTor\MXTCore\Models\MenuType;
use Illuminate\Support\Facades\Blade;
use MaxTor\MXTCore\Models\Role;
use MaxTor\MXTCore\Widgets\Widget;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MXTCorePackageHelper extends AbstractPackageHelper
{
    public static function getPackageName()
    {
        return 'mxtcore';
    }

    /**
     * Breadcrumbs
     * @docs https://github.com/davejamesmiller/laravel-breadcrumbs/tree/master
     */
    public static function getAdminBreadcrumbs($bundle)
    {
        if (!Breadcrumbs::exists('admin.' . self::getPackageName())) {
            Breadcrumbs::for ('admin.' . self::getPackageName(), function ($trail) {
                $trail->parent('admin');
                $trail->push(self::getPackageName(), route('admin.' . self::getPackageName() . '.index'));
            });
        }

        Breadcrumbs::for ('admin.' . self::getPackageName() . '.' . $bundle . '.index', function ($trail) use ($bundle) {
            $trail->parent('admin.' . self::getPackageName());
            $trail->push($bundle, route('admin.' . self::getPackageName() . '.' . $bundle . '.index'));
        });

        Breadcrumbs::for ('admin.' . self::getPackageName() . '.' . $bundle . '.create', function ($trail) use ($bundle) {
            $trail->parent('admin.' . self::getPackageName() . '.' . $bundle . '.index');
            $trail->push('create ' . $bundle, route('admin.' . self::getPackageName() . '.' . $bundle . '.create'));
        });

        Breadcrumbs::for ('admin.' . self::getPackageName() . '.' . $bundle . '.edit', function ($trail, $model) use ($bundle) {
            $trail->parent('admin.' . self::getPackageName() . '.' . $bundle . '.index');
            $trail->push($model->name, route('admin.' . self::getPackageName() . '.' . $bundle . '.edit', ['id' => $model->id]));
        });
    }
}