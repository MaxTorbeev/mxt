<?php

namespace MaxTor\MXTCore\Console\Commands;

use Illuminate\Console\GeneratorCommand;

class MakeWidget extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:widget';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new widget class';
    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Widget';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return mxtcore_resources_path('stubs/widget.stub');
    }

    /**
     * Get the default namespace for the class.
     *
     * @param string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Http\Widgets';
    }
}