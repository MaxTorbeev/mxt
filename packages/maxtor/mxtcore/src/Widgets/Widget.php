<?php

namespace MaxTor\MXTCore\Widgets;

use DaveJamesMiller\Breadcrumbs\Exceptions\ViewNotSetException;
use Illuminate\Support\Str;

class Widget implements WidgetContractInterface
{
    public $packageName = 'mxtcore';

    public $cacheTime = 360;
    public $cacheKey;
    public $attributes = null;
    public $model = null;
    public $view = 'default';

    /**
     * @inheritdoc
     */
    public function get(?string $key, $attributes = null)
    {
        $class = config('widgets.' . $key);

        if (!class_exists($class)) {
            return view('mxtcore::components.alert', [
                'type' => 'danger',
                'title' => __('mxtcore.widget.errors.title'),
                'slot' => __('mxtcore.widget.errors.classNotFound', compact('class'))
            ]);
        }

        $widget = new $class();

        try {
            return $widget->handle($attributes);
        } catch (\Exception $e) {
            return view('mxtcore::components.alert', [
                'type' => 'danger',
                'title' => __('mxtcore.widget.errors.title'),
                'slot' => $e->getMessage()
            ]);
        }

    }

    /**
     * @inheritdoc
     */
    public function handle($attributes = null)
    {
        $this->attributes = $attributes;
        $this->view = $attributes['view'] ?? 'default';
        $this->cacheKey = $attributes['cacheKey'] ?? rand(0, 10);
        $this->model = $attributes['model'] ?? null;
    }

    public function view($view = null, $data = [], $mergeData = [])
    {
//        try {

            return view($this->viewName($view), $data, $mergeData)->render();

//        } catch (\Exception  $e) {
//            dd($e->getMessage());

//            return view('mxtcore::components.alert', [
//                'type' => 'danger',
//                'title' => __('mxtcore.widget.errors.title'),
//                'slot' => $e->getMessage()
//            ])->render();
//        }
    }
    protected function viewName($view)
    {
        if (view()->exists($view))
            return $view;

        return $this->packageName . '::widgets.' . $this->getWidgetViewFolder() . '.' . $view;
    }


    public function getDefaultCacheKey(string $key = null)
    {
        return rtrim('widgets::'
            . $this->packageName
            . $this->getWidgetViewFolder()
            . '.' . $this->view
            . '.' . $this->cacheKey . '.'
            . '.' . $key, ". \t\n");
    }

    public function getWidgetViewFolder()
    {
        return Str::snake(str_replace('Widget', '', class_basename($this)), '-');
    }
}
