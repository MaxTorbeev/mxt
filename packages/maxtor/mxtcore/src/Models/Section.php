<?php

namespace MaxTor\MXTCore\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use MaxTor\Content\Traits\ContentScopes;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\UserManagement;

class Section extends Model
{
    use Cacheable, UserManagement, Sluggable, ContentScopes;

    protected $guarded = ['id'];

    protected $cacheKeys = ['sections', 'sectionsOnHomepage'];

    public function setAttribsAttribute($value)
    {
        $this->attributes['attribs'] = json_encode($value);
    }

    public function setHomepageOnlyAttribute($value)
    {
        $this->attributes['homepage_only'] = $value === 'on' ? 1 : 0;
    }

    public function getAttribsAttribute($value)
    {
        return json_decode($value);
    }
//
//    public function getAttrib($key)
//    {
//        foreach ($this->attribs as $attribKey => $item){
//            if($item->name === $key){
//                return $item->value;
//            }
//        }
//
//        return false;
//    }
}
