<?php

namespace MaxTor\MXTCore\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use MaxTor\Content\Traits\ContentScopes;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\Filterable;
use MaxTor\MXTCore\Traits\UserManagement;

class SiteConfig extends Model
{
    use Cacheable, UserManagement, ContentScopes, ContentScopes, Filterable;

    protected $guarded = ['id', 'action'];

    public function getCacheKey()
    {
        return "siteConfig:{$this->name}:{$this->updated_at}";
    }

    public function setExtraAttributesAttribute($value)
    {
        $this->attributes['extra_attributes'] = json_encode($value);
    }

    public function getExtraAttributesAttribute($value)
    {
        return json_decode($value, true);
    }
}
