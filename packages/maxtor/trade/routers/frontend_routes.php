<?php

Route::get('/', [
    'as' => 'frontend.trade.index',
    'uses' => 'CatalogController@index'
]);

Route::get('/{categories}', [
    'as' => 'frontend.trade.catalog',
    'uses' => 'CatalogController@show'
])->where('categories', '^[a-zA-Z0-9-_\/]+$');


// Home > Catalog
Breadcrumbs::for('home.trade.catalog', function ($trail) {
    $trail->parent('home');
    $trail->push(__('trade.catalog'), route('frontend.trade.index'));
});

// Home > Catalog > Categories
Breadcrumbs::for('home.trade.catalog.categories', function ($trail, $category) {
    $trail->parent('home.trade.catalog');

    if ($category->parent()->count()) {
        foreach ($category->parent()->get() as $parent) {
            $trail->push($parent->name, $parent->frontend_url);
        }
    }
    $trail->push($category->name, $category->frontend_url);
});

// Home > Catalog > Categories > Goods
Breadcrumbs::for ('home.trade.catalog.categories.goods', function ($trail, $goods) {
    $trail->parent('home.trade.catalog.categories', $goods->category);
    $trail->push($goods->name, $goods->frontend_url);
});




