<?php

use MaxTor\Trade\TradePackageHelper;
use Illuminate\Support\Facades\Route;

Route::resource('trade', 'TradeController', ['as' => 'admin']);
Route::resource('goods-categories', 'GoodsCategoriesController', ['as' => 'admin.trade']);
Route::resource('goods', 'GoodsController', ['as' => 'admin.trade']);

Route::post('/goods-categories/{goodsCategories}/photos', [
    'as' => 'admin.trade.goods-categories.photo',
    'uses' => 'GoodsCategoriesController@addPhoto'
]);

Route::post('/goods-categories/{goodsCategories}/attachments', [
    'as' => 'admin.trade.goods-categories.attachment',
    'uses' => 'GoodsCategoriesController@addAttachment'
]);

Route::post('/goods/{goods}/photos', [
    'as' => 'admin.trade.goods.photo',
    'uses' => 'GoodsController@addPhoto'
]);

Route::post('/goods/{goods}/attachments', [
    'as' => 'admin.trade.goods.attachment',
    'uses' => 'GoodsController@addAttachment'
]);

TradePackageHelper::getAdminBreadcrumbs('goods-categories');
TradePackageHelper::getAdminBreadcrumbs('goods');
