<?php

use \Illuminate\Support\Facades\Route;

Route::match(['get', 'post'],'/orders/add/{goods}', [
    'as' => 'admin.trade.orders.add',
    'uses' => 'OrdersController@add'
]);

Route::get('mailable', function () {
    $feedback = \MaxTor\Content\Models\Feedback::find(15);

    $feedback->mailTemplate = 'trade::emails.order.create';
    $feedback->sendingModel = \MaxTor\Trade\Models\Order::find(37);

    return (new \MaxTor\Content\Mail\BackCall($feedback))->render();
});

Route::resource('prices/periods', 'PricePeriodsController', ['as' => 'admin.trade.price']);
