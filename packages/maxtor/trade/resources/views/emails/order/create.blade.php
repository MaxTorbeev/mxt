@component('mail::message')
# {{ $feedback->mailSubject }}

* Имя: {{ $feedback->name }}
* Телефон: {{ $feedback->phone }}
* Email: {{ $feedback->email }}
* Сообщение: {{ $feedback->sendingModel->note }}

@foreach($feedback->sendingModel->cart as $goods)
@component('mail::table')
|                                   |                          |
| --------------------------------- | ------------------------ |
| ![alt text]({{ $goods->goods_photo ?? null }}) | [{{ $goods->goods_name }}]({{ $goods->goods_frontend_url }})
@endcomponent
@endforeach


@endcomponent

