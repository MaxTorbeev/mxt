@if($model->photos)
    <div class="swiper-container my-3"
         data-slides
         data-direction="horizontal"
         data-free-mode="true"
         data-slides-per-view="1"
         data-space-between="20"
    >
        <div class="swiper-wrapper">
            @foreach($model->photos as $photo)
                <div class="swiper-slide">
                    <img class="card-img"
                         src="{{ asset($photo->getThumb(1090, 500)) }}"
                         alt="{{ $model->photo_preview->original_name }}">
                </div>
            @endforeach
        </div>
    </div>
@endif