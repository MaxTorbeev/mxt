<div class="swiper-container"
     data-slides
     data-direction="horizontal"
     data-free-mode="true"
     data-slides-per-view="1"
     data-space-between="20"
>
    <div class="swiper-wrapper">
        @if($model->photo_preview)
            <div class="card card-withImageOverlay bg-dark text-white">
                <img class="card-img"
                     src="{{ asset($model->photo_preview->getThumb(1090, 500)) }}"
                     alt="{{ $model->photo_preview->original_name }}">
                <div class="card-img-overlay card-img-overlay-center px-5 py-5">
                    <h2 class="card-title">{{ $model->page_title ?? $model->name }}</h2>
                    @isset($attributes['description'])
                        <div class="card-text">{!! $attributes['description'] !!}</div>
                    @endisset

                    @isset($attributes['btns'])

                    @endisset
                </div>
            </div>
        @endif
    </div>
</div>
