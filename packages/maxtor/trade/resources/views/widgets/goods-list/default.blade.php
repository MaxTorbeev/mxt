@inject('goodsCard', 'MaxTor\Trade\Widgets\GoodsCardWidget')

@isset($attributes['title'])
    <h2 class="my-3 h1">{{ $attributes['title'] }}</h2>
@endisset

@isset($attributes['description'])
    <div class="lead mb-4">
        {{ $attributes['description'] }}
    </div>
@endisset

<div class="row">
    @foreach($goods as $item)
        <div class="col-md-3">
            {!! $goodsCard->handle(['view' => $attributes['card-view'] ?? 'block', 'goods' => $item]) !!}
        </div>
    @endforeach
</div>
