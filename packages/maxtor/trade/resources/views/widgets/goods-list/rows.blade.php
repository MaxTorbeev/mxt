@inject('goodsCard', 'MaxTor\Trade\Widgets\GoodsCardWidget')

@isset($attributes['title'])
    <h2 class="my-3 display-4 text-center">{{ $attributes['title'] }}</h2>
@endisset

@isset($attributes['description'])
    <div class="lead mb-4 text-center">
        {{ $attributes['description'] }}
    </div>
@endisset

@foreach($goods as $item)
    {!! $goodsCard->handle(['view' => $attributes['card-view'] ?? 'block', 'goods' => $item]) !!}
@endforeach
