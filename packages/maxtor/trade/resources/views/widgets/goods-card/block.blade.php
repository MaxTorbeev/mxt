<div class="card card-goods goods mb-3">
    <a href="{{ $goods->frontend_url }}">
        @isset($goods->photo_preview->id)
            <img class="card-img-top" src="{{ $goods->photo_preview->getThumb(340, 220) }}" alt="{{ $goods->name  }}">
        @else
            <img class="card-img-top" src="{{ config('mxtcore.common.defaults.photo')  }}" alt="{{ $goods->name  }}">
        @endisset
    </a>
    <div class="card-body py-3 px-3">
        <h5 class="card-title">
            <a href="{{ $goods->frontend_url }}">{{ $goods->name }}</a>
        </h5>
        <div class="card-text">
            <div class="goods_summary">
                <div class="goods_summary-list goods_summary-small mb-0">
                    <div class="goods_summary_row">
                        <div class="goods_summary_title">Тип</div>
                        <div class="goods_summary_value">
                            <a href="{{ $goods->category->frontend_url }}">{{ $goods->category->name }}</a>
                        </div>
                    </div>
                    @if($goods->extra_attributes)
                        @foreach($goods->extra_attributes as $attribute)
                            @if($loop->index === 3)
                                @break
                            @endif
                            @if(!is_null($attribute['value']))
                                <div class="goods_summary_row">
                                    <div class="goods_summary_title">{{ $attribute['label'] }}</div>
                                    <div class="goods_summary_value">{{ $attribute['value'] }}</div>
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
            @isset($goods->base_price->value)
                <div class="goods_price text-right my-2">
                    <span class="goods_price_cost goods_price_cost-md">
                        Цена @money($goods->base_price->value, $goods->base_price->currency->name)/{{ $goods->base_price->unit->name }}
                    </span>
                    @isset($goods->base_price->period)
                        <small class="text-muted">{{ $goods->base_price->period->name }} </small>
                    @endisset
                </div>
            @endisset
        </div>

        <div class="row no-gutters align-content-center">
            <div class="col-md-7">
                <add-to-cart
                             class="mx-1"
                             size="sm"
                             :can-buy="true"
                             :goods-id="{{ $goods->id }}"
                             btn-title="{{ $goods->is_rent ? 'Арендовать' : 'Заказать' }}">
                </add-to-cart>
            </div>
            <div class="col-md-5">
                <a href="{{ $goods->frontend_url }}" class="btn btn-success btn-block btn-sm mx-1">{{ __('content.more') }}</a>
            </div>
        </div>
    </div>
</div>
