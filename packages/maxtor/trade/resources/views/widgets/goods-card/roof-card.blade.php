<div class="card card-goods goods mb-3">
    <img class="card-img-top"
         src="{{ asset($goods->photo_preview->getThumb(335, 222)) }}" alt="">
    <div class="card-body pb-4">
        <h5 class="card-title">{{ $goods->name }}</h5>
        <p class="card-text">
            {!! $goods->full_text !!}
        </p>
    </div>
    <div class="card-footer-absolute">
        @money($goods->base_price->value, $goods->base_price->currency->name) / {{ $goods->base_price->unit->name }}
    </div>
</div>