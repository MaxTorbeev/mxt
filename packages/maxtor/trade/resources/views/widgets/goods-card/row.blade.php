<div class="card card-goods goods card-hovered  mb-3">
    <div class="row no-gutter">
        <div class="col-md-3">
            <div class="card-goods_photo">
                @isset($goods->photo_preview->id)
                    <img class="card-img-top"
                         src="{{ $goods->photo_preview->getThumb(245, 200) }}"
                         alt="{{ $goods->photo_preview->original_name }}">
                @else
                    <img class="card-img-top"
                         src="{{ config('mxtcore.common.defaults.photo')  }}"
                         alt="{{ $goods->name  }}">
                @endisset
            </div>
        </div>
        <div class="col-md-6">
            <h5 class="card-title pt-4">
                <a href="{{ $goods->frontend_url }}" title="{{ $goods->name  }}">{{ $goods->name }}</a>
            </h5>
            <div class="card-text">
                <div class="goods_summary">
                    <div class="goods_summary-list goods_summary-small mb-0">
                        <div class="goods_summary_row">
                            <div class="goods_summary_title">Тип</div>
                            <div class="goods_summary_value">
                                <a href="{{ $goods->category->frontend_url }}">{{ $goods->category->name }}</a>
                            </div>
                        </div>
                        @if($goods->extra_attributes)
                            @foreach($goods->extra_attributes as $attribute)
                                @if($loop->index === 4)
                                    @break
                                @endif
                                @if(!is_null($attribute['value']))
                                    <div class="goods_summary_row">
                                        <div class="goods_summary_title">{{ $attribute['label'] }}</div>
                                        <div class="goods_summary_value">{{ $attribute['value'] }}</div>
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="mr-3 pt-4">
                <div class="goods_price">
                    <span class="goods_price_title text-muted">Цена</span>
                    <span class="goods_price_cost goods_price_cost-md">
                        @money($goods->base_price->value, $goods->base_price->currency->name)/{{ $goods->base_price->unit->name }}
                    </span>
                </div>
                <a href="{{ $goods->frontend_url }}" class="btn btn-success btn-sm btn-block mt-md-3">Подробнее</a>
            </div>
        </div>
    </div>
</div>
