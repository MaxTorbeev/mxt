<div class="card card-goods mb-3">
    <div class="card-body px-0 pb-4">
        <div class="row no-gutter align-items-center">
            <div class="col-md-4">
                <img src="{{ asset($goods->photo_preview->getThumb(153, 115)) }}" class="img-fluid" alt="{{ $goods->photo_preview->original_name }}">
            </div>
            <div class="col-md-8">
                <h5 class="card-title">
                    <a href="{{ $goods->frontend_url }}">
                        {{ $goods->name }}
                    </a>
                </h5>
                <div class="card-text">
                    {!! $goods->full_text !!}
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer-absolute">
        @money($goods->base_price->value, $goods->base_price->currency->name) / {{ $goods->base_price->unit->name }}
    </div>
</div>