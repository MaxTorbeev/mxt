<div class="card-group mt-3 w-100">
    @foreach($categories as $category)
    <div class="card">
        <a href="{{ $category->frontend_url }}">
            @isset($category->photo_preview->id)
                <img class="card-img-top" src="{{ $category->photo_preview->getThumb(270, 150) }}" alt="">
            @else
                <img class="card-img-top" src="{{ config('mxtcore.common.defaults.photo')  }}" alt="">
            @endisset
        </a>
        <div class="card-body py-3 px-3">
            <h5 class="card-title text-center mb-0">
                <a href="{{ $category->frontend_url }}" title="{{ $category->name }}">
                    {{ $category->name }}
                </a>
            </h5>
        </div>
    </div>
    @endforeach
</div>
