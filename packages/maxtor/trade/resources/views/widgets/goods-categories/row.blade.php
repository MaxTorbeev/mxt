@foreach($categories as $category)
    <div class="card card-goods mb-3">
        <div class="row no-gutter align-items-center">
            <div class="col-md-4">
                <div class="card-goods_photo">
                    <a href="{{ $category->frontend_url }}">
                        @isset($category->photo_preview->id)
                            <img src="{{ $category->photo_preview->getThumb(185, 100) }}" alt="{{ $category->name }}">
                        @else
                            <img src="{{ config('mxtcore.common.defaults.photo')  }}" alt="{{ $category->name }}">
                        @endisset
                    </a>
                </div>
            </div>
            <div class="col-md-8">
                <h5 class="h5 pr-2">
                    <a href="{{ $category->frontend_url }}">{{ $category->name }}</a>
                </h5>
            </div>
        </div>
    </div>
@endforeach
