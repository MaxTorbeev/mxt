<div class="card bg-light pt-3 px-3">
    @foreach($price['types'] as $type)
        <div class="form-group form-row">
            <label class="col-sm-2 col-form-label"> {{ $type->label }} </label>
            <div class="col-sm-3">
                {!! Form::text(
                    'price[' . $type->name . '][value]',
                    $model->prices()->where('price_type_id', $type->id)->value('value') ?? null,
                    ['class'=>'form-control'])
                !!}
            </div>
            <div class="col-sm-2">
                {!! Form::select(
                    'price[' . $type->name . '][currency]',
                    $price['currency'],
                    $model->prices()->where('price_type_id', $type->id)->value('currency_id') ?? null,
                    ['class'=>'form-control '])
                !!}
            </div>
            <div class="col-sm-2">
                {!! Form::select(
                    'price[' . $type->name . '][unit]',
                    $price['unit'],
                    $model->prices()->where('price_type_id', $type->id)->value('unit_id') ?? null,
                    ['class'=>'form-control '])
                !!}
            </div>

            <div class="col-sm-3">
                <price-periods price-type="{{ $type->name  }}"
                               :current-period="{{ $model->prices()->where('price_type_id', $type->id)->value('period_id') ?? 0 }}"
                >

                </price-periods>
            </div>
        </div>
    @endforeach
</div>
