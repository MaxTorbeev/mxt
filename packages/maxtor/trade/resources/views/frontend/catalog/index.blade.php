@extends('content::frontend.layouts.content')

@inject('feedbackFormWidget', 'MaxTor\Content\Widgets\FeedbackFormWidget')
@inject('goodsCategoriesWidget', 'MaxTor\Trade\Widgets\GoodsCategoriesWidget')

@section('meta-keywords')@endsection
@section('meta-description')@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('home.trade.catalog') }}
@endsection

@section('title')
    {{ __('trade.catalog') }}
@endsection

@section('content')
    @parent

    <div class="row">
        <div class="col-md-4">
            {!! $goodsCategoriesWidget->handle(['view' => 'row']) !!}
        </div>
        <div class="col-md-8">
            <h2 class="h1 mt-4 text-bold">Обратная связь</h2>

            {!! $feedbackFormWidget->handle([
             'view' => 'feedback-contact-page',
         ]) !!}
        </div>
    </div>
@endsection
