@extends('trade::frontend.layouts.goods')

@inject('feedbackFormWidget', 'MaxTor\Content\Widgets\FeedbackFormWidget')
@inject('goodsCategoriesWidget', 'MaxTor\Trade\Widgets\GoodsCategoriesWidget')
@inject('goodsListWidget', 'MaxTor\Trade\Widgets\GoodsListWidget')

@section('title')
    {{ $goods->name }}
@endsection

@section('price')
    <div class="goods goods_price">

        @isset($goods->base_price->value)
            <div class="goods_price_title">
                Цена:
            </div>
            <div class="goods_price_cost">
                @money($goods->base_price->value,
                $goods->base_price->currency->name)/{{ $goods->base_price->unit->name }}
            </div>
            @isset($goods->base_price->period)
                <small class="text-muted">{{ $goods->base_price->period->name }} </small>
            @endisset
        @endisset

        <add-to-cart class="my-2"
                     size="lg"
                     :can-buy="true"
                     :goods-id="{{ $goods->id }}"
                     btn-title="{{ $goods->is_rent ? 'Арендовать' : 'Заказать' }}">
        </add-to-cart>

        @foreach($goods->attachments as $attachment)
            <a href="{{ $attachment->url }}" class="btn btn-outline-dark btn-block btn-sm mb-2">
                {{ $attachment->original_name }}
            </a>
        @endforeach
    </div>
@endsection

@section('gallery')
    @include('content::components.gallery', ['model' => $goods])
@endsection

@section('full_text')
    {!! $goods->full_text  !!}
@endsection

@section('tags')
    <div class="goods_tags my-4">
        @foreach($goods->tags as $tag)
            <a href="{{ route('frontend.content.tags.show', $tag) }}" class="btn btn-outline-dark btn-sm">{{ $tag->name }}</a>
        @endforeach
    </div>
@endsection

@section('extra_attributes')
    @isset($goods->extra_attributes)
        <h2 class="h2 text-bold mt-3 mb-2">Характеристики</h2>

        <div class="goods_summary">
            <div class="goods_summary-list mb-0 row">
                @foreach($goods->extra_attributes as $attribute)
                    @if(!is_null($attribute['value']))
                        <div class="col-md-6">
                            <div class="goods_summary_row">
                                <div class="goods_summary_title text-bold">{{ $attribute['label'] }}</div>
                                <div class="goods_summary_value">{{ $attribute['value'] }}</div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    @endisset
@endsection

@section('content')
    @parent

    {{--<section class="section my-4">--}}
        {{--<h2 class="h2 text-bold">Вместе с «{{ $goods->name }}» {{ $goods->is_rent ? 'арендуют' : 'покупают' }}</h2>--}}
    {{--</section>--}}
@endsection

@section('aside_right')
    @parent
    {!!
    $feedbackFormWidget->handle([
        'view' => 'consultation',
        'action-url' => route('frontend.content.feedback.consultation')
    ])
    !!}
@endsection

