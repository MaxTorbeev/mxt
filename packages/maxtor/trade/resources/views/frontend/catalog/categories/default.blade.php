@extends('trade::frontend.layouts.category')

@inject('postListWidget', 'MaxTor\Content\Widgets\PostListWidget')
@inject('feedbackFormWidget', 'MaxTor\Content\Widgets\FeedbackFormWidget')
@inject('goodsListWidget', 'MaxTor\Trade\Widgets\GoodsListWidget')
@inject('goodsCategoriesWidget', 'MaxTor\Trade\Widgets\GoodsCategoriesWidget')
@inject('goodsCategoryCarouselWidget', 'MaxTor\Trade\Widgets\GoodsCategoryCarouselWidget')

@section('header_top_phone')
    @include('helpers.phone', ['phone' => site_config_extra('company_phones.' . $category->root->slug .'.value')])
@endsection

@section('title')
    {{ $category->name }}
@endsection

@section('content')
    @parent

    {!! $goodsCategoryCarouselWidget->handle([
            'model' => $category,
    ]) !!}

    <div class="lead my-3">
        {!! $category->description !!}
    </div>

    @if($category->isRoot())
        <div class="card card-legend card-withShadow mt-4">
            <div class="legend-title legend-title-blue">
                Вам может пригодится
            </div>
            <div class="card-body">
                {!! $goodsCategoriesWidget->handle(['exclude_id' => $category->id]) !!}
            </div>
        </div>
    @endif
@endsection

@section('bottom')
    @if($category->isRoot())
        <div class="container">
            {!!
            $goodsListWidget->handle([
                'category_id' => $category->id
            ])
            !!}
        </div>
    @endif
@endsection


@section('aside_right')
    {!! $feedbackFormWidget->handle([
        'view' => 'feedback',
        'action_url' => route('frontend.content.feedback.backcall'),
        'title' => 'Оставьте заявку и получите скидку на наши услуги'
    ]) !!}

    <div class="my-3">
        @foreach($category->attachments as $attachment)
            <a href="{{ $attachment->url }}" class="btn btn-danger btn-block">{{ $attachment->original_name }}</a>
        @endforeach
    </div>

    {!! $postListWidget->handle([
        'view' => 'vertical',
        'category_slug' => 'articles'
    ]) !!}
@endsection

