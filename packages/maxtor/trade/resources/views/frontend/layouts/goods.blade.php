@extends('layouts.app')

@section('breadcrumbs')
    {{ Breadcrumbs::render('home.trade.catalog.categories.goods', $goods) }}
@endsection

@section('content')
    <div class="goods-page">
        <h1 class="page-title h1 text-bold">@yield('title')</h1>
        @parent

        @yield('gallery')
        <div class="lead my-3">
            @yield('full_text')
        </div>
        @yield('extra_attributes')
        @yield('tags')
    </div>
@endsection

@section('aside_right')
    @yield('price')
@endsection
