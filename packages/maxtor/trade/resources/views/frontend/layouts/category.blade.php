@extends('layouts.app')

@section('head-meta')
    <title>@yield('title') - {{ site_config('site_name')->value }}</title>
    <meta name="keywords" content="@yield('meta-keywords')">
    <meta name="description" content="@yield('meta-desc')">
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('home.trade.catalog.categories', $category) }}
@endsection

@section('content')
    @parent
@endsection
