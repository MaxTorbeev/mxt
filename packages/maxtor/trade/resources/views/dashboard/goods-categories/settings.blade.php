@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.trade.goods-categories.settings', $goodsCategory) }} @endsection

@section('content')
    <div class="card">
        <div class="card-header">
            Аттрибуты
            <a href="{{ route( 'admin.trade.goods-categories.attributes.create', $goodsCategory ) }}"
               class="btn btn-sm btn-success mx-3">Добавить
            </a>
        </div>
        <div class="card-body">

        </div>
    </div>
@stop