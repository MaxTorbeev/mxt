@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.trade.goods-categories.create') }} @endsection

@section('content')
    <div class="card">
        <div class="card-header">
            Новая категория
        </div>

        <div class="card-body">
            {!! Form::model($goodsCategory  = new \MaxTor\Trade\Models\GoodsCategory(), ['url' => route('admin.trade.goods-categories.store'), 'files' => true, 'method' => 'POST']) !!}
                @include ('trade::dashboard.goods-categories.form', ['submitButtonText' => 'Добавить категорию'])
            {!! Form::close() !!}
        </div>
    </div>
@stop