@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.trade.goods-categories.edit', $goodsCategory) }} @endsection

@section('content')
    <div class="card">
        <div class="card-header">
            Редактировать категорию
        </div>

        <div class="card-body">
            <div class="form-row">
                <div class="col-md-9">
                    {!! Form::model($goodsCategory, ['url' => route('admin.trade.goods-categories.update', $goodsCategory), 'files' => true, 'method' => 'PATCH']) !!}
                    @include ('trade::dashboard.goods-categories.form', ['submitButtonText' => 'Редактировать категорию'])
                    {!! Form::close() !!}
                </div>
                <div class="col-md-3">
                    @include('content::forms.attachments', [
                        'data' =>
                        [
                            'photos' => [
                                'upload-url' => route('admin.trade.goods-categories.photo', ['id' => $goodsCategory->id]),
                                'thumbs-url' => route('api.content.photo.list', [
                                    'subject_id' => $goodsCategory->id,
                                    'type' => $goodsCategory->getPhotoType()
                                ])
                            ],
                            'attachments' => [
                                'upload-url' => route('admin.trade.goods-categories.attachment', ['id' => $goodsCategory->id]),
                                'thumbs-url' => route('api.content.attachment.list', [
                                    'subject_id' => $goodsCategory->id,
                                    'type' => $goodsCategory->getAttachmentType()
                                ])
                            ]
                        ]
                    ])
                </div>
            </div>

        </div>
    </div>
@stop
