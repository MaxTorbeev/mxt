<div class="form-row">
    <div class="col-md-8">
        {!! Form::label('name', 'Название:', ['class' => 'sr-only']) !!}
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text">Название</div>
            </div>
            {!! Form::text('name', null, ['class'=>'form-control']) !!}
            @if ($errors->has('name'))
                <small class="form-control-feedback">{{ $errors->first('name') }}</small>
            @endif
        </div>
    </div>

    <div class="col-md-4">
        {!! Form::label('slug', 'Ссылка:', ['class' => 'sr-only']) !!}
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="fa fa-link" aria-hidden="true"></i></div>
            </div>
            {!! Form::text('slug', null, ['class'=>'form-control']) !!}
            @if ($errors->has('slug'))
                <small class="form-control-feedback">{{ $errors->first('slug') }}</small>
            @endif
        </div>
    </div>
</div>

<div class="input-group mb-2">
    <div class="input-group-prepend">
        <div class="input-group-text">Заголовок на странице</div>
    </div>
    {!! Form::text('page_title', null, ['class'=>'form-control']) !!}
    @if ($errors->has('page_title'))
        <small class="form-control-feedback">{{ $errors->first('page_title') }}</small>
    @endif
</div>

<div class="form-row">
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::label('description', 'Описание:') !!}
            @if ($errors->has('description'))
                <small class="form-control-feedback">{{ $errors->first('description') }}</small>
            @endif
            <tinymce name="description" html="{{ $goodsCategory->description }}"></tinymce>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fa fa-calendar"></i>
                </div>
            </div>
            {!! Form::input('text', 'published_at', $goodsCategory->published_at, ['class'=>'form-control', 'data-datetimepicker', 'data-timepicker' => 'true' ]) !!}
        </div>

        <div class="form-group">
            {!! Form::label('parent_id', 'Родительская категория:') !!}

            <select id="parent_id" name="parent_id" class="form-control select2">
                <option value="0">Корневая категория</option>
                @foreach(\MaxTor\Trade\Models\GoodsCategory::pluck('name', 'id') as $catId => $parentCategory)
                    <option value="{{ $catId }}" {{ $catId == $goodsCategory->parent_id ? 'selected' : ''}}>{{ $parentCategory }}</option>
                @endforeach
            </select>
        </div>

        @include ('mxtcore::forms.user-management', ['model' => $goodsCategory])
        @include ('content::forms.publish-timestamps', ['model' => $goodsCategory])
        @include ('content::forms.seo', ['model' => $goodsCategory])
    </div>
</div>

<div class="panel form_buttonGroups form_buttonGroups-fixed">
    <div class="form-group">
        {!! Form::submit( $submitButtonText, ['class' => 'btn btn-primary']) !!}
        <button type="submit" name="action" value="save_and_close" class="btn btn btn-success">Сохранить и перейти к списку</button>
        <button type="submit" name="action" value="save_and_create" class="btn btn btn-success">Сохранить и создать новый</button>
        <a href="{{ route('admin.trade.goods-categories.index') }}" class="btn btn btn-warning text-right">Не сохранять и перейти к списку</a>

        @if($goodsCategory->exists)
            <button type="submit" name="action" value="delete" class="pull-right btn btn btn-danger">Удалить</button>
        @endif
    </div>
</div>
