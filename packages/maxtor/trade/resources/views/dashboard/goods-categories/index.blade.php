@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.trade.goods-categories.index') }} @endsection

@section('content')
    <ul class="nav nav-pills mb-2">
        <li class="nav-item">
            <a class="nav-link btn btn-success" href="{{ route('admin.trade.goods-categories.create') }}">Добавить категорию</a>
        </li>
    </ul>

    <div class="card">
        <div class="card-header">
            Категории
        </div>

        <div class="card-body">
            {!! $table->build() !!}
        </div>
    </div>
@stop