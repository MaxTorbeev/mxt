@extends('mxtcore::layouts.dashboard')


@section('breadcrumbs') {{ Breadcrumbs::render('admin.trade.goods.index') }} @endsection

@section('content')
    <ul class="nav nav-pills mb-2">
        <li class="nav-item">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#create-goods-model">
                Добавить товар
            </button>
        </li>
    </ul>

    <div class="card">
        <div class="card-header">
            Товары
        </div>

        <div class="card-body">
            {!! $table->build() !!}
        </div>
    </div>

    @include('trade::dashboard.goods.create-modal')
@stop