@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.trade.goods.edit', $goods) }} @endsection

@section('content')
    <div class="card">
        <div class="card-header">
            Редактировать товар
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="col-md-9">
                    {!! Form::model($goods, ['url' => route('admin.trade.goods.update', $goods), 'files' => true, 'method' => 'PATCH']) !!}
                    @include ('trade::dashboard.goods.form', ['submitButtonText' => 'Редактировать товар'])
                    {!! Form::close() !!}
                </div>

                <div class="col-md-3">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a id="nav-photos-tab" class="nav-item nav-link active" data-toggle="tab" href="#photos-tab"
                               role="tab" aria-controls="nav-home" aria-selected="true">
                                Фото
                            </a>
                            <a id="nav-attach-tab" class="nav-item nav-link" data-toggle="tab" href="#attach-tab"
                               role="tab" aria-controls="nav-profile" aria-selected="false">
                                Прикрепленные файлы
                            </a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div id="photos-tab" class="tab-pane fade show active">
                            <upload-files url="{{ route('admin.trade.goods.photo', ['id' => $goods->id]) }}"
                                          field-name="photos"></upload-files>

                            <file-thumbs url="{{ route('api.content.photo.list', [
                                    'subject_id' => $goods->id,
                                    'type' => $goods->getPhotoType()
                                ]) }}"></file-thumbs>
                        </div>

                        <div id="attach-tab" class="tab-pane fade ">
                            <upload-files url="{{ route('admin.trade.goods.attachment', ['id' => $goods->id]) }}"
                                          field-name="attachments">
                            </upload-files>
                            <file-thumbs url="{{ route('api.content.attachment.list',
                                [
                                    'subject_id' => $goods->id,
                                    'type' => $goods->getPhotoType()
                                ]
                            ) }}"></file-thumbs>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
