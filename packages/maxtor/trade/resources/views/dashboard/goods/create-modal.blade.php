<div id="create-goods-model" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        {!! Form::model($goods  = new \MaxTor\Trade\Models\Goods(), ['url' => route('admin.trade.goods.store'), 'files' => true, 'method' => 'POST']) !!}

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Добавить товар</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::label('name', 'Название:', ['class' => 'sr-only']) !!}
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Название</div>
                    </div>
                    {!! Form::text('name', null, ['class'=>'form-control']) !!}
                    @if ($errors->has('name'))
                        <small class="form-control-feedback">{{ $errors->first('name') }}</small>
                    @endif
                </div>

                {!! Form::label('goods_category_id', 'Категория:', ['class' => 'sr-only']) !!}
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Категория</div>
                    </div>

                    <select id="goods_category_id" name="goods_category_id" class="form-control">
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ str_repeat('--', $category->depth) . $category->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <input type="submit" class="btn btn-success" value="Добавить">
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>