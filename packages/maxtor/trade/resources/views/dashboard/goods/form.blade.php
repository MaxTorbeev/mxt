<div class="form-row">
    <div class="col-md-6">
        {!! Form::label('name', 'Название:', ['class' => 'sr-only']) !!}
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text">Название</div>
            </div>
            {!! Form::text('name', $goods->name, ['class'=>'form-control']) !!}
            @if ($errors->has('name'))
                <small class="form-control-feedback">{{ $errors->first('name') }}</small>
            @endif
        </div>
    </div>

    <div class="col-md-4">
        {!! Form::label('goods_category_id', 'Категория:', ['class' => 'sr-only']) !!}
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text">Категория</div>
            </div>
            {!! Form::select('goods_category_id', $categories, null, ['class'=>'form-control select2']) !!}
        </div>
    </div>

    <div class="col-md-2">
        {!! Form::label('slug', 'Ссылка:', ['class' => 'sr-only']) !!}
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="fa fa-link" aria-hidden="true"></i></div>
            </div>
            {!! Form::text('slug', null, ['class'=>'form-control']) !!}
            @if ($errors->has('slug'))
                <small class="form-control-feedback">{{ $errors->first('slug') }}</small>
            @endif
        </div>
    </div>
</div>

@include('trade::forms.price', ['price' => $price, 'model' => $goods])

<div class="form-row">
    <div class="col-sm-9">
        <div class="form-group  {{ $errors->has('full_text') ? 'has-danger' : '' }}">
            {!! Form::label('full_text', 'Полное описание:') !!}
            <tinymce name="full_text" html="{{ $goods->full_text }}"></tinymce>
            @if ($errors->has('full_text'))
                <small class="form-control-feedback">{{ $errors->first('full_text') }}</small>
            @endif
        </div>

        {!! $extraAttributes->build() !!}

        {{--@if (view()->exists("trade.goods.forms.extra.{$goods->category->slug}"))--}}
            {{--@include("trade.goods.forms.extra.{$goods->category->slug}", compact('goods'))--}}
        {{--@else--}}
            {{--@include("trade.goods.forms.extra.default", compact('goods'))--}}
        {{--@endif--}}
    </div>

    <div class="col-sm-3">
        <div class="form-group  {{ $errors->has('is_rent') ? 'has-danger' : '' }}">
            {!! Form::label('is_rent', 'В аренду:') !!}
            {!! Form::select('is_rent', ['Нет', 'Да'], null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group  {{ $errors->has('_lft') ? 'has-danger' : '' }}">
            {!! Form::label('_lft', 'Показывать после:') !!}
            {!! Form::select('_lft', $goods->where('id', '<>', $goods->id)->where('goods_category_id', $goods->goods_category_id)->pluck('name', 'id'), null, ['class'=>'form-control select2']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('tags', 'Tags:') !!}
            {!! Form::select('tags[]', $tags, $goods->tags, ['class'=>'form-control select2', 'multiple', 'data-tags' => 'true']) !!}
        </div>
        @include ('mxtcore::forms.user-management', ['model' => $goods])
        @include ('content::forms.publish-timestamps', ['model' => $goods])
        @include ('content::forms.seo', ['model' => $goods])
    </div>
</div>

<div class="panel form_buttonGroups form_buttonGroups-fixed">
    <div class="form-group">
    {!! Form::submit( $submitButtonText, ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.trade.goods.index') }}" class="btn btn btn-secondary">Отменить и перейти к списку</a>
    </div>
</div>

@section('footer')

@endsection
