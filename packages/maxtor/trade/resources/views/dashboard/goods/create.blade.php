@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.trade.goods.create') }} @endsection

@section('content')
    <div class="card">
        <div class="card-header">
            Новый товар
        </div>

        <div class="card-body">
            {!! Form::model($goods  = new \MaxTor\Trade\Models\Goods(), ['url' => route('admin.trade.goods.store'), 'files' => true, 'method' => 'POST']) !!}
                @include ('trade::dashboard.goods.form', ['submitButtonText' => 'Добавить товар'])
            {!! Form::close() !!}
        </div>
    </div>
@stop