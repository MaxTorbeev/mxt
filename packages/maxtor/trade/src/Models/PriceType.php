<?php

namespace MaxTor\Trade\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\UserManagement;

class PriceType extends Model
{
    use Cacheable, UserManagement, SoftDeletes;

    protected $table = 'price_types';

    protected $guarded = ['id'];

    public function prices()
    {
        return $this->hasMany(Price::class, 'id', 'price_type_id');
    }
}
