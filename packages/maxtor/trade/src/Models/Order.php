<?php

namespace MaxTor\Trade\Models;

use Illuminate\Database\Eloquent\Model;
use Laravelrus\LocalizedCarbon\Traits\LocalizedEloquentTrait;
use MaxTor\Content\Traits\ContentScopes;
use MaxTor\Content\Traits\RecordsTag;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\SetCreators;
use MaxTor\MXTCore\Traits\UserManagement;

class Order extends Model
{
    use Cacheable;

    public function setCustomerAttribute($value)
    {
        $customer = Customer::where('user_id', auth()->id())->first();

        if(!$customer){
            $customer = new Customer();
        }

        $customer->firstname = $value['firstname'] ?? null;
        $customer->email = $value['email'] ?? null;
        $customer->phone = $value['phone'] ?? null;

        $customer->save();

        $this->attributes['customer_id'] = $customer->id;
    }

    public function setCartAttribute($card)
    {
        $this->attributes['cart'] = json_encode($card);
    }

    public function getCartAttribute($cart)
    {
        return json_decode($cart);
    }

    public function setStateAttribute($status)
    {
        $this->attributes['state_id'] = OrderState::where('name', $status)->value('id');
    }

    public function state()
    {
        return $this->hasOne(OrderState::class, 'id', 'state_id');
    }

    public function customer()
    {
        return $this->hasOne(Customer::class, 'id', 'customer_id');
    }
}