<?php

namespace MaxTor\Trade\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Laravelrus\LocalizedCarbon\Traits\LocalizedEloquentTrait;
use MaxTor\Content\Traits\ContentScopes;
use MaxTor\Content\Traits\PublishTimestampsScopes;
use MaxTor\Content\Traits\RecordComment;
use MaxTor\Content\Traits\RecordsAttachment;
use MaxTor\Content\Traits\RecordsPhoto;
use MaxTor\Content\Traits\RecordsTag;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\UserManagement;
use MaxTor\MXTCore\Traits\Viewable;
use CyrildeWit\EloquentViewable\Contracts\Viewable as ViewableContract;

class Goods extends Model implements ViewableContract
{
    use RecordsPhoto,
        RecordsTag,
        RecordsAttachment,
        Sluggable,
        Cacheable,
        UserManagement,
        ContentScopes,
        LocalizedEloquentTrait,
        RecordComment,
        PublishTimestampsScopes,
        Viewable;

    protected $guarded = ['id'];
    protected $appends = ['views'];
    protected $hidden = ['deleted_at', 'created_at', 'locked', 'preview_photo_id', 'role_id', 'permission_id'];
    protected $cacheKeys = [
        'trade:goods-list:default',
        'widgets::trade.goods-card'
    ];

    public function getFrontendUrlAttribute()
    {
        $url = Cache::remember($this->cacheKey() . ':frontend_url', 60, function () {
            $url = '';
            $post = Goods::where('id', $this->id)->firstOrFail();
            $category = $post->category;
            $categories = [$category];

            while (!is_null($category) && !is_null($category = $category->parent)) {
                $categories[] = $category;
            }

            foreach (array_reverse($categories) as $category) {
                $url .= $category->slug . '/';
            }

            $url .= $post->slug;

            return $url;
        });

        return route('frontend.trade.catalog', ['slugs' => $url]);
    }


    /**
     * Записываем значения цены и связанных таблиц.
     *
     * @param $prices - массив, где ключ - тип цены.
     */
    public function setPriceAttribute($prices)
    {
        $this->prices()->delete();

        foreach ($prices as $type => $price) {
            $this->prices()->save(Price::create([
                'price_type_id' => PriceType::whereName($type)->value('id'),
                'currency_id' => PriceCurrency::whereId($price['currency'])->value('id'),
                'unit_id' => PriceUnit::whereId($price['unit'])->value('id'),
                'value' => $price['value'],
                'period_id' => $price['period'] ?? null
            ]));
        }
    }

    public function setExtraAttributesAttribute($value)
    {
        $this->attributes['extra_attributes'] = json_encode($value);
    }

    public function getExtraAttributesAttribute($value)
    {
        return json_decode($value, true);
    }

    public function getViewsAttribute()
    {
        return views($this)->count();
    }

    public function getBasePriceAttribute()
    {
        return $this->prices()
            ->rightJoin('price_types as type', 'prices.price_type_id', '=', 'type.id')
            ->where('type.is_base', true)
            ->first();
    }

    public function category()
    {
        return $this->hasOne(GoodsCategory::class, 'id', 'goods_category_id');
    }

    public function prices()
    {
        return $this->belongsToMany(Price::class, 'goods_price');
    }

    public function scopeFilters($query, $filters)
    {
        return $filters->apply($query);
    }

    public function extraAttributes($form)
    {
        if(!$this->category) return $form;

        if (method_exists($form, Str::camel($this->category->slug, '-'))) {
            $form->{Str::camel($this->category->slug, '-')}($this->extra_attributes);
        }

        return $form;
    }
}
