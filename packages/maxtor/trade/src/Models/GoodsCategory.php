<?php

namespace MaxTor\Trade\Models;

use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Kalnoy\Nestedset\NodeTrait;
use MaxTor\Content\Traits\ContentScopes;
use MaxTor\Content\Traits\RecordsAttachment;
use MaxTor\Content\Traits\RecordsPhoto;
use MaxTor\Content\Traits\Searchable;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\Filterable;
use MaxTor\MXTCore\Traits\UserManagement;
use CyrildeWit\EloquentViewable\Contracts\Viewable as ViewableContract;
use MaxTor\MXTCore\Traits\Viewable;

class GoodsCategory extends Model implements ViewableContract
{
    use Cacheable,
        UserManagement,
        SoftDeletes,
        ContentScopes,
        Searchable,
        RecordsPhoto,
        RecordsAttachment,
        Filterable,
        Viewable;

    use Sluggable, NodeTrait {
        NodeTrait::replicate as replicateNode;
        Sluggable::replicate as replicateSlug;
    }

    protected $guarded = ['id'];
//    protected $appends = ['views'];
    protected $cacheKeys = [
        'widgets::trade.goods-categories.default',
    ];

    public function replicate(array $except = null)
    {
        $instance = $this->replicateNode($except);
        (new SlugService())->slug($instance, true);

        return $instance;
    }

    public function getFrontendUrlAttribute()
    {
        $url = Cache::remember($this->cacheKey() . ':frontend_url', 60, function () {
            $url = '';
            $category = $this;
            $categories = [$category];

            while (!is_null($category) && !is_null($category = $category->parent)) {
                $categories[] = $category;
            }

            foreach (array_reverse($categories) as $category) {
                $url .= $category->slug . '/';
            }

            return $url;
        });

        return route('frontend.trade.catalog', ['slugs' => $url]);
    }

    public function getViewsAttribute()
    {
        return views($this)->count();
    }

    public function getPageTitleAttribute()
    {
        return $this->page_title ?? $this->name;
    }

    public function getRootAttribute()
    {
        return Cache::remember($this->cacheKey() . ':root', 60, function () {
            if ($this->isRoot()) return $this;

            $category = $this;
            $categories = [];

            while (!is_null($category) && !is_null($category = $category->parent)) {
                $categories[] = $category;
            }

            return end($categories);
        });
    }

    public function goods()
    {
        return $this->hasMany(Goods::class, 'goods_category_id')->published();
    }

    public function parent()
    {
        return $this->belongsTo(GoodsCategory::class, 'parent_id')->with('parent');
    }

    public function children()
    {
        return $this->hasMany(GoodsCategory::class, 'parent_id')->with('children');
    }
}
