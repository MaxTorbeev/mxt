<?php

namespace MaxTor\Trade\Models;

use Illuminate\Database\Eloquent\Model;
use Laravelrus\LocalizedCarbon\Traits\LocalizedEloquentTrait;
use MaxTor\Content\Traits\ContentScopes;
use MaxTor\Content\Traits\RecordsTag;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\SetCreators;
use MaxTor\MXTCore\Traits\UserManagement;

class OrderState extends Model
{
    use Cacheable,
        UserManagement;

    protected $table = 'orders_states';

    protected $guarded = ['id'];
    protected $cacheKeys = [];
}