<?php

namespace MaxTor\Trade\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\UserManagement;

class Customer extends Model
{
    use Cacheable, UserManagement, SoftDeletes;

    protected $guarded = ['id'];

    public function setPhoneAttribute($phone)
    {
        $this->attributes['phone'] = parse_phone($phone, true, ['plain' => true]);
    }

    public function getFullNameAttribute()
    {
        return "{$this->firstname} {$this->lastname}";
    }
}
