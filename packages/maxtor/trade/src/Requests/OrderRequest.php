<?php

namespace MaxTor\Trade\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
            case 'PATCH':
                return [
                    'firstname' => 'required|max:191',
                    'email' => 'required|email|unique:users,email,' . $this->user->id,
                ];

            case 'POST':
            default:
                return [
                    'firstname' => 'required|max:191',
                    'email' => 'required|email',
                    'phone' => 'required',
                ];
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Название должено быть указано',
            'from.integer' => 'Должно быть целове число',
            'to.integer' => 'Должно быть целове число',
        ];
    }
}