<?php

namespace MaxTor\Trade\Layouts\Admin;

use App\User;
use MaxTor\Content\Models\Category;
use MaxTor\MXTCore\Layouts\AbstractTable;
use MaxTor\MXTCore\Fields\TD;
use MaxTor\Trade\Models\GoodsCategory;

class GoodsCategoriesListLayout extends AbstractTable
{

    public $permissions = ['create_goods_category'];

    public $data = '';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('id')
                ->width(20)
                ->title('Код'),

            TD::name('photo')
                ->title('Фото')
                ->width(50)
                ->setRender(function ($model) {
                    return $this->photoPreview($model);
                }),

            TD::name('name')
                ->title('Название')
                ->width(550)
                ->setRender(function ($model) {
                    return $this->getName($model);
                }),
            TD::name('author')
                ->title('Родитель')
                ->setRender(function ($model) {
                    return $this->editCategoryAnchor($model['parent_id']);
                }),

            TD::name('delete')->title('')
                ->width(50)
                ->setRender(function ($model) {
                    return '<delete action="' . route('admin.trade.goods-categories.destroy', ['id' => $model['id']]) . '">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </delete>';
                }),
        ];
    }


    private function getName($model)
    {
        return '<a href="' . route('admin.trade.goods-categories.edit', ['id' => $model['id']]) . '">'
            . str_repeat('--', $model['depth']) . $model['name'] .
            '</a>';
    }

    private function editCategoryAnchor($categoryId)
    {
        return $categoryId !== null
            ? '<a href="' . route('admin.trade.goods-categories.edit', ['id' => $categoryId]) . '">'
            . GoodsCategory::whereId($categoryId)->value('name') .
            '</a>'
            : 'Корневая категория';
    }

    private function photoPreview($model)
    {
        if (GoodsCategory::whereId([$model['id']])->first()->photo_preview) {
            return '<img src="' . GoodsCategory::whereId([$model['id']])->first()->photo_preview->getThumb(50) . '" alt="">';
        }

        return '<img src="' . config('mxtcore.common.defaults.photo') . '" width="50" height="50" alt="">';
    }
}
