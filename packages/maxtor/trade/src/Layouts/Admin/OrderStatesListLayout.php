<?php

namespace MaxTor\Trade\Layouts\Admin;

use App\User;
use MaxTor\Content\Models\Category;
use MaxTor\Content\Models\Post;
use MaxTor\MXTCore\Layouts\AbstractTable;
use MaxTor\MXTCore\Fields\TD;
use MaxTor\Trade\GoodsCategory;

class OrderStatesListLayout extends AbstractTable
{
    public function fields(): array
    {
        return [
            TD::name('id')->title('Код'),
            TD::name('name')
                ->title('Название')
                ->width(550)
        ];
    }
}