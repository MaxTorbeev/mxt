<?php

namespace MaxTor\Trade\Layouts\Admin;

use App\User;
use MaxTor\Content\Models\Category;
use MaxTor\Content\Models\Post;
use MaxTor\MXTCore\Layouts\AbstractTable;
use MaxTor\MXTCore\Fields\TD;
use MaxTor\Trade\Models\Goods;
use MaxTor\Trade\Models\GoodsCategory;

class GoodsListLayout extends AbstractTable
{

    /**
     * @var string
     */
    public $data = '';

    /**
     * @return array
     */
    public function fields(): array
    {

        return [
            TD::name('id')
                ->width(50)
                ->title('Код'),

            TD::name('photo')
                ->title('Фото')
                ->width(50)
                ->setRender(function ($model) {
                    return $this->photoPreview($model);
                }),

            TD::name('name')->title('Название')
                ->width(550)
                ->setRender(function ($model) {
                    return $this->editAnchor($model);
                }),

            TD::name('goods_category_id')->title('Категория')
                ->setRender(function ($model) {
                    return $this->editCategoryAnchor($model);
                }),

            TD::name('views')
                ->title('Просмотры')
                ->setRender(function ($model) {
                    return $model['views'];
                }),

            TD::name('updated_at')->title('Обновлено'),
            TD::name('created_at')->title('Создано'),

            TD::name('delete')->title('')
                ->setRender(function ($model) {
                    return $this->deleteButtonHtml(route('admin.trade.goods.destroy', ['id' => $model['id']]));
                }),
        ];
    }

    private function editAnchor($model)
    {
        return '<a href="' . route('admin.trade.goods.edit', ['id' => $model['id']]) . '">' . $model['name'] . '</a>';
    }

    private function editCategoryAnchor($model)
    {
        if (GoodsCategory::whereId($model['goods_category_id'])->exists())
            return
                '<a href="' . route('admin.trade.goods-categories.edit', ['id' => $model['goods_category_id']]) . '">'
                . GoodsCategory::whereId($model['goods_category_id'])->first()->name .
                '</a>';
    }

    private function photoPreview($model)
    {
        if (Goods::whereId([$model['id']])->first()->photo_preview) {
            return '<img src="' . Goods::whereId([$model['id']])->first()->photo_preview->getThumb(50) . '" alt="">';
        }

        return '<img src="' . config('mxtcore.common.defaults.photo') . '" width="50" height="50" alt="">';
    }
}
