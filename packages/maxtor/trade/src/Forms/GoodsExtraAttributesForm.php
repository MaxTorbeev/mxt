<?php

namespace MaxTor\Trade\Forms;

use Illuminate\Support\Str;
use MaxTor\MXTCore\Forms\AbstractForm;
use MaxTor\Trade\Models\Goods;

class GoodsExtraAttributesForm extends AbstractForm
{
    public function roof(): array
    {
        return [

        ];
    }

    public function betononasos($attributes)
    {
        $this->fields = [
            [
                'type' => 'text',
                'name' => 'feed-height',
                'label' => 'Высота подачи',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'feed-range',
                'label' => 'Дальность подачи',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'min-opening-height',
                'label' => 'Минимальная высота раскрытия',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'pipe-diameter',
                'label' => 'Диаметр трубопровода',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'hose-length',
                'label' => 'Длина шланга',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'angle-rotation',
                'label' => 'Угол поворота',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'span-front-legs',
                'label' => 'Размах передних опор',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'span-rear-legs',
                'label' => 'Размах задних опор',
                'value' => null,
            ]
        ];

        $this->associate($attributes);

        return $this;
    }

    public function ekskavatorPogruzchik($attributes)
    {
        $this->fields = [
            [
                'type' => 'text',
                'name' => 'engine-power',
                'label' => 'Мощность двигателя (л.с)',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'capacity-bucket',
                'label' => 'Емкость экскаваторного ковша (м. куб.)',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'digging-depth',
                'label' => 'Глубина копания (м)',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'hydraulic-hammer',
                'label' => 'Гидромолот (HM)',
                'value' => null,
            ]
        ];

        $this->associate($attributes);

        return $this;
    }


    /**
     * Строительные леса.
     *
     * @param $attributes
     * @return $this
     */
    public function scaffolding($attributes): self
    {
        $this->fields = [
            [
                'type' => 'text',
                'name' => 'height',
                'label' => 'Высота (м)',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'pipe-diameter',
                'label' => 'Диаметр трубы (мм)',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'length',
                'label' => 'Длина (м)',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => Str::slug('Permissible load on the platform'),
                'label' => 'Допустимая нагрузка на помост (кгс/м2)',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => Str::slug('Frame pitch'),
                'label' => 'Шаг рам',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => Str::slug('Tier Step'),
                'label' => 'Шаг яруса',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => Str::slug('Tier Width'),
                'label' => 'Ширина яруса',
                'value' => null,
            ],
        ];

        $this->associate($attributes);

        return $this;
    }

    public function sadovyyDom($attributes)
    {
        $this->fields = [
            [
                'type' => 'text',
                'name' => 'foundation',
                'label' => 'Фундамент',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'wireframe-tying',
                'label' => 'Обвязка каркаса',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'floors',
                'label' => 'Полы',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'walls',
                'label' => 'Стены',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'ceiling',
                'label' => 'Потолок',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'doors',
                'label' => 'Двери',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'windows',
                'label' => 'Окна',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'platbands',
                'label' => 'Наличники',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'hardware',
                'label' => 'Метизы',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'paint',
                'label' => 'Краска',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'roof',
                'label' => 'Кровля',
                'value' => null,
            ],
        ];

        $this->associate($attributes);

        return $this;
    }
}
