<?php

namespace MaxTor\Trade\Controllers\Admin;

use Illuminate\Http\Request;
use MaxTor\Content\Requests\AttachmentRequest;
use MaxTor\Content\Requests\PhotoRequest;
use MaxTor\MXTCore\Controllers\AbstractAdminController;
use MaxTor\Trade\Layouts\Admin\GoodsCategoriesListLayout;
use MaxTor\Trade\Models\GoodsCategory;
use MaxTor\Trade\TradePackageHelper;

final class GoodsCategoriesController extends AbstractAdminController
{
    protected $helper;

    public function __construct(TradePackageHelper $helper)
    {
        $this->helper = $helper;
    }

    public function index()
    {
        $this->authorize('create_goods_category');

        $nodes = GoodsCategory::defaultOrder()->withDepth()->get()->toFlatTree();

        $table = new GoodsCategoriesListLayout($nodes);

        return $this->view('index', compact('table'));
    }

    public function create()
    {
        $this->authorize('create_goods_category');

        $categories = GoodsCategory::defaultOrder()->get()->toFlatTree();

        return $this->view('create', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->authorize('create_goods_category');

        $goodsCategory = GoodsCategory::create($request->all());

        return redirect(route('admin.trade.goods-categories.edit', ['menu' => $goodsCategory->id]))
            ->with('flash', 'Успешно');
    }

    public function edit(GoodsCategory $goodsCategory)
    {
        $this->authorize('create_goods_category');

        $categories = GoodsCategory::defaultOrder()->where('id', '!=', $goodsCategory->id)->get()->toFlatTree();

        return $this->view('edit', compact('goodsCategory', 'categories'));
    }

    public function update(GoodsCategory $goodsCategory, Request $request)
    {
        $this->authorize('create_goods_category');

        $goodsCategory->up();

        if ($goodsCategory->update($request->all())) {
            return redirect(route('admin.trade.goods-categories.edit', ['id' => $goodsCategory->id]))->with('flash', 'Категория была отредактирована');
        }

        return flash('flash', 'Не изменить категорию');
    }

    public function destroy(GoodsCategory $goodsCategory)
    {
        $this->authorize('delete_goods_category');

        if ($goodsCategory->protected == true) {
            return response(['message' => 'Категория защищена от удаления'], 403);
        }

        $goodsCategory->delete();

        return response(['message' => 'Категория успешно была удалена'], 204);
    }

    public function settings($id)
    {
        $goodsCategory = GoodsCategory::whereId($id)->published()->firstOrFail();

        return $this->view('settings', compact('goodsCategory'));
    }

    public function settingsCreate($id)
    {
        $goodsCategory = GoodsCategory::whereId($id)->published()->firstOrFail();

        return $this->view('settings-create', compact('goodsCategory'));
    }

    public function move(GoodsCategory $goodsCategory)
    {
        switch (request('direction')) {
            case 'up':
                $goodsCategory->up();
                break;

            case 'down':
                $goodsCategory->up();
                break;
        }

        return back();
    }

    public function addPhoto($id, PhotoRequest $request)
    {
        $photo = $this->makePhoto($request->file('photos'));
        GoodsCategory::whereId($id)->firstOrFail()->addPhoto($photo);

        return 'Done';
    }

    public function addAttachment($id, AttachmentRequest $request)
    {
        $attachment = $this->makeAttachment($request->file('attachments'));
        GoodsCategory::whereId($id)->firstOrFail()->addAttachment($attachment);

        return 'Done';
    }
}
