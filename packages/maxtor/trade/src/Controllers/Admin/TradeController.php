<?php

namespace MaxTor\Trade\Controllers\Admin;

use Illuminate\Http\Request;
use MaxTor\MXTCore\Controllers\AbstractAdminController;
use MaxTor\Trade\Models\GoodsCategory;
use MaxTor\Trade\TradePackageHelper;

final class TradeController extends AbstractAdminController
{
    protected $helper;

    public function __construct(TradePackageHelper $helper)
    {
        $this->helper = $helper;
    }

    public function index()
    {
    }
}