<?php

namespace MaxTor\Trade\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Models\Tag;
use MaxTor\Content\Requests\AttachmentRequest;
use MaxTor\Content\Requests\PhotoRequest;
use MaxTor\MXTCore\Controllers\AbstractAdminController;
use MaxTor\Trade\Filters\GoodsFilters;
use MaxTor\Trade\Forms\GoodsExtraAttributesForm;
use MaxTor\Trade\Layouts\Admin\GoodsListLayout;
use MaxTor\Trade\Models\Goods;
use MaxTor\Trade\Models\GoodsCategory;
use MaxTor\Trade\Models\Price;
use MaxTor\Trade\Models\PriceCurrency;
use MaxTor\Trade\Models\PricePeriod;
use MaxTor\Trade\Models\PriceType;
use MaxTor\Trade\Models\PriceUnit;
use MaxTor\Trade\TradePackageHelper;

final class GoodsController extends AbstractAdminController
{
    protected $helper;

    public function __construct(TradePackageHelper $helper)
    {
        $this->helper = $helper;

        parent::__construct();
    }

    public function index(GoodsFilters $filters)
    {
        $goods = Goods::latest()
            ->filters($filters)
            ->paginate(site_config('base_paginate', config('mxtcore.paginate', 20))->value);

        $categories = GoodsCategory::defaultOrder()->withDepth()->get()->toFlatTree();

        $table = new GoodsListLayout($goods);

        return $this->view('index', compact('table', 'categories'));
    }

    /**
     * Create a new blog post.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create_goods');

        $goodsCategories = GoodsCategory::all()
            ->pluck('name', 'id');
        $tags = Tag::all()->pluck('name', 'id');

        return $this->view('create', [
            'categories' => $goodsCategories,
            'tags' => $tags,
        ]);
    }

    public function store(Request $request)
    {
        $this->authorize('create_goods');

        $tags = $request->input('tags');

        $request->offsetUnset('tags');
        $goods = Goods::create($request->all());

        if ($tags)
            $this->syncTags($goods, $tags);

        return redirect(route('admin.trade.goods.edit', ['id' => $goods->id]))->with('flash', 'Сохранено');
    }

    public function edit(Goods $goods, GoodsExtraAttributesForm $form)
    {
        $this->authorize('create_goods');

        $extraAttributes = $goods->extraAttributes($form);

        return $this->view('edit', [
            'price' => [
                'types' => PriceType::enabled()->get(),
                'unit' => PriceUnit::enabled()->select('name', 'id')->get()->pluck('name', 'id'),
                'currency' => PriceCurrency::enabled()->select('abbr', 'id')->get()->pluck('abbr', 'id'),
            ],
            'categories' => $goodsCategories = GoodsCategory::all()->pluck('name', 'id'),
            'tags' => Tag::pluck('name', 'id'),
            'goods' => $goods,
            'extraAttributes' => $extraAttributes
        ]);
    }

    public function update(Goods $goods, Request $request)
    {
        $this->authorize('create_goods');

        if ($request->input('tags'))
            $this->syncTags($goods, $request->input('tags'));

        $request->offsetUnset('tags');

        $goods->update($request->all());

        return redirect(route('admin.trade.goods.edit', ['id' => $goods->id]))->with('flash', 'Сохранено');
    }

    public function destroy(Goods $goods)
    {
        $this->authorize('delete_goods');

        if($goods->delete()) {
            return redirect(route('admin.trade.goods.index'))->with('flash', 'Удалено');
        }
    }

    public function addPhoto(Goods $goods, PhotoRequest $request)
    {
        $photo = $this->makePhoto($request->file('photos'));
        $goods->addPhoto($photo);

        return 'Done';
    }

    public function addAttachment(Goods $goods, AttachmentRequest $request)
    {
        $attachment = $this->makeAttachment($request->file('attachments'));
        $goods->addAttachment($attachment);

        return 'Done';
    }
}
