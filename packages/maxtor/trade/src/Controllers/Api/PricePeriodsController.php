<?php

namespace MaxTor\Trade\Controllers\Api;

use Illuminate\Http\Request;
use MaxTor\MXTCore\Controllers\AbstractApiController;
use MaxTor\Trade\Models\PricePeriod;
use MaxTor\Trade\Requests\PricePeriodRequest;

final class PricePeriodsController extends AbstractApiController
{
    public function index()
    {
        return PricePeriod::select('id', 'name')->pluck('name', 'id');
    }

    public function store(PricePeriodRequest $request)
    {
        $period = PricePeriod::create($request->all());

        return $period->id;
    }
}