<?php

namespace MaxTor\Trade\Controllers\Api;

use MaxTor\MXTCore\Controllers\AbstractApiController;
use MaxTor\Trade\Models\Goods;
use MaxTor\Trade\Models\Order;
use MaxTor\Trade\Requests\OrderRequest;

final class OrdersController extends AbstractApiController
{
    public function add(Goods $goods, OrderRequest $request)
    {
        if ($request->isMethod('get')) {
            return $goods
                ->append(['frontend_url', 'photo_preview'])
                ->makeHidden([
                    'extra_attributes',
                    'enabled',
                    'modified_user_id',
                    'created_user_id',
                    'slug',
                    'intro_text',
                    'full_text',
                    'publish_up',
                    'publish_down'
                ]);
        }

        if ($request->isMethod('post')) {
            $order = new Order();

            $order->customer = [
                'firstname' => $request->get('firstname'),
                'email' => $request->get('email'),
                'phone' => $request->get('phone')
            ];

            $order->cart = [
                [
                    'goods_id' => $goods->id,
                    'goods_name' => $goods->name,
                    'goods_photo' => $goods->photo_preview ? $goods->photo_preview->thumb : null,
                    'goods_frontend_url' => $goods->frontend_url,
                ]
            ];

            $order->state = 'accepted';
            $order->note = $request->get('note');

            if($order->save()){
                return response()->json(['message' => 'Заказ успешно создан']);
            }

            return response()->json(['message' => 'Заказ создан с ошибкой'], 403);
        }
    }
}
