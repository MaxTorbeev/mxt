<?php

namespace MaxTor\Trade\Controllers\Frontend;

use App\Http\Controllers\Controller;
use MaxTor\MXTCore\Controllers\AbstractFrontendController;
use MaxTor\Trade\Models\Goods;
use MaxTor\Trade\Models\GoodsCategory;
use MaxTor\Trade\TradePackageHelper;

final class CatalogController extends AbstractFrontendController
{
    protected $helper;

    public function __construct(TradePackageHelper $helper)
    {
        $this->helper = $helper;
    }

    public function index()
    {
        return $this->view('index');
    }

    public function show($slugs)
    {
        $slugsInPath = explode('/', $slugs);
        $endSlug = end($slugsInPath);

        // Определяем не товар ли это
        if (Goods::where('slug', $endSlug)->exists()) {
            $goods = Goods::where('slug', $endSlug)->firstOrFail();

            if ($goods->frontend_url === route('frontend.trade.catalog', ['url' => $slugs]))
                return $this->catalogView($goods->slug, compact('goods'), 'goods');
        }

        // Если не пост, то возможно категория
        $category = GoodsCategory::where('slug', $endSlug)->firstOrFail();

        $categoryGoods = Goods::where('goods_category_id', $category->id)
            ->paginate(site_config('base_paginate', config('mxtcore.paginate'))->value);

        if ($category->frontend_url === route('frontend.trade.catalog', ['url' => $slugs])) {
            return $this->catalogView($category->slug, compact('category', 'categoryGoods'));
        }

        return abort(404);
    }

    protected function catalogView($name, $data = [], $section = 'categories')
    {
        if (view()->exists('pages.trade.catalog.' . $section . '.' . $name)) {
            return $this->view('pages.trade.catalog.' . $section . '.' . $name, $data);
        }

        return $this->view($section . '.default', $data);
    }
}
