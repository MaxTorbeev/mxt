<?php

namespace MaxTor\Trade\Widgets;

use MaxTor\MXTCore\Widgets\Widget;

class GoodsCardWidget extends Widget
{
    public $packageName = 'trade';

    public function handle($attributes = null)
    {
        parent::handle($attributes);

        try {
            return cache()->rememberForever($this->getDefaultCacheKey($this->attributes['goods']->cacheKey()), function (){
                return $this->view($this->view, ['goods' => $this->attributes['goods']]);
            });
        } catch (\Exception $e) {
            return view('mxtcore::components.alert', [
                'type' => 'danger',
                'title' => __('mxtcore.widget.errors.title'),
                'slot' => $e->getMessage()
            ])->render();
        }
    }
}
