<?php

namespace MaxTor\Trade\Widgets;

use Illuminate\Support\Arr;
use MaxTor\MXTCore\Widgets\Widget;
use MaxTor\Trade\Models\Goods;
use MaxTor\Trade\Models\GoodsCategory;

class GoodsListWidget extends Widget
{
    public $packageName = 'trade';

    public function handle($attributes = null)
    {
        parent::handle($attributes);

        $goods = $this->getGoods();

        return $this->view($this->view, compact('goods', 'attributes'));
    }

    public function getGoods()
    {
        if($this->attributes['category_id']){
            $categories = GoodsCategory::select('id')->descendantsAndSelf($this->attributes['category_id'])->toFlatTree()->toArray();

            return Goods::whereIn('goods_category_id', Arr::flatten($categories))
                ->limit($this->attributes['category_id'] ?? 10)
                ->get();
        }
    }
}
