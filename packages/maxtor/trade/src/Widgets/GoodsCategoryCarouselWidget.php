<?php

namespace MaxTor\Trade\Widgets;

use MaxTor\MXTCore\Widgets\Widget;
use MaxTor\Trade\Models\GoodsCategory;

class GoodsCategoryCarouselWidget extends Widget
{
    public $packageName = 'trade';

    public function handle($attributes = null)
    {
        parent::handle($attributes);

        return \Cache::rememberForever($this->getDefaultCacheKey($this->attributes['model']->slug), function () {
            return $this->view($this->view, [
                'attributes' => $this->attributes,
                'model' => $this->attributes['model'] ?? null,
            ]);
        });
    }
}
