<?php

namespace MaxTor\Trade\Widgets;

use MaxTor\MXTCore\Widgets\Widget;
use MaxTor\Trade\Models\GoodsCategory;

class GoodsCategoriesWidget extends Widget
{
    public $packageName = 'trade';

    public function handle($attributes = null)
    {
        parent::handle($attributes);

        $excludeId = $attributes['exclude_id'] ?? null;

        $categories = GoodsCategory::published()->whereNull('parent_id');

        if ($excludeId) {
            $categories->where('id', '<>', $excludeId);
        }

        return \Cache::rememberForever($this->getDefaultCacheKey() . ($excludeId ?? null), function () use ($categories) {
            return $this->view($this->view, [
                'categories' => $categories->get()
            ]);
        });
    }
}
