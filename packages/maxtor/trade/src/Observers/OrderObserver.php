<?php

namespace MaxTor\Trade\Observers;

use MaxTor\Content\Models\Feedback;
use MaxTor\MXTCore\Models\Role;
use MaxTor\Trade\Models\Order;

class OrderObserver
{
    protected $emails = [];

    public function __construct()
    {
        $this->emails = Role::where('name', 'root')->first()->users->pluck('email');
    }

    public function created(Order $order)
    {
        if($order->state()->value('send_email')){
            $this->emails->prepend($order->customer->email);
        }

        $this->createFeedback($order);
    }

    public function updated(Order $order)
    {
        if($order->state()->value('send_email')){
            $this->emails->prepend($order->customer->email);
        }
    }

    private function createFeedback(Order $order)
    {
        foreach ($this->emails as $email){
            $feedback = new Feedback();

            $feedback->email = $email;
            $feedback->sendingModel = $order;
            $feedback->mailSubject = 'Создан новый заказ';
            $feedback->mailTemplate = 'trade::emails.order.create';
            $feedback->phone = $order->customer->phone;
            $feedback->name = $order->customer->full_name;

            $feedback->save();
        }
    }
}