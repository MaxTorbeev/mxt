<?php

namespace MaxTor\Trade;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use MaxTor\Trade\Models\Goods;
use MaxTor\Trade\Models\GoodsCategory;
use MaxTor\Trade\Models\Order;
use MaxTor\Trade\Observers\OrderObserver;

class TradeServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'trade');

        //Migration
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        //Factories
        $this->app->make(EloquentFactory::class)->load(__DIR__ . '/../database/factories/');

        //Routes
        $this->mapApiRoutes();
        $this->mapAdminRoutes();
        $this->mapFrontendRoutes();

        $this->routeModelBindings();

        Order::observe(OrderObserver::class);
    }

    public function register()
    {

    }

    protected function mapFrontendRoutes(): void
    {
        Route::prefix('/services')
            ->middleware('web')
            ->namespace('MaxTor\Trade\Controllers\Frontend')
            ->group(__DIR__ . '/../routers/frontend_routes.php');
    }

    protected function mapApiRoutes(): void
    {
        Route::prefix('/api/trade')
            ->middleware('api')
            ->namespace('MaxTor\Trade\Controllers\Api')
            ->group(__DIR__ . '/../routers/api_routes.php');
    }

    protected function mapAdminRoutes(): void
    {
        Route::prefix('/admin/trade')
            ->middleware('web')
            ->namespace('MaxTor\Trade\Controllers\Admin')
            ->group(__DIR__ . '/../routers/admin_routes.php');
    }

    protected function routeModelBindings()
    {
        Route::bind('good', function ($value) {
            return Goods::whereId($value)->firstOrFail();
        });
    }
}
