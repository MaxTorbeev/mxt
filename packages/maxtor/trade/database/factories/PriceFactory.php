<?php

use App\User;
use MaxTor\Content\Models\Post;
use MaxTor\Content\Models\Category;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\MaxTor\Trade\Models\Price::class, function (Faker $faker) {
    return [
        'value' => $faker->randomDigitNotNull,
        'price_type_id' => function () {
            return factory(\MaxTor\Trade\Models\PriceType::class)->create()->id;
        },
        'currency_id' => function () {
            return factory(\MaxTor\Trade\Models\PriceCurrency::class)->create()->id;
        },
        'period_id' => function () {
            return factory(\MaxTor\Trade\Models\PricePeriod::class)->create()->id;
        },
        'unit_id' => function () {
            return factory(\MaxTor\Trade\Models\PriceUnit::class)->create()->id;
        },
    ];
});