<?php

use Illuminate\Database\Seeder;

class TradeDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PriceTypesDatabaseSeeder::class);
        $this->call(TradePermissionsTableSeeder::class);
        $this->call(GoodsCategoriesSeeder::class);
        $this->call(GoodsDatabaseSeeder::class);
        $this->call(PriceUnitsDatabaseSeeder::class);
        $this->call(PricePeriodsDatabaseSeeder::class);
        $this->call(PriceCurrencyDatabaseSeeder::class);
        $this->call(OrdersStatesSeeder::class);
    }
}
