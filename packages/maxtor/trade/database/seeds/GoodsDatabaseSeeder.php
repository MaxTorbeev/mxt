<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;


class GoodsDatabaseSeeder extends Seeder
{
    public function run()
    {
        DB::table('goods')->insert([
            [
                'slug' => Str::slug('Бетононасос MAN', '-'),
                'name' => 'Бетононасос MAN',
                'goods_category_id' => 6,
                'intro_text' => 'В нашей компании вы можете арендовать бетононасос марки Cifa на шасси грузовой автомашины MAN. Эта модель АБН зарекомендовал себя, как чрезвычайно надежная строительная техника, стрела которой позволяет производить монолитные работы высоте 36 метров.',
                'extra_attributes' => $this->extra(),
                'created_user_id' => 1
            ]
        ]);
    }

    protected function extra()
    {
        return json_encode([
            [
                'type' => 'text',
                'name' => 'feed-height',
                'label' => 'Высота подачи',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'feed-range',
                'label' => 'Дальность подачи',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'min-opening-height',
                'label' => 'Минимальная высота раскрытия',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'pipe-diameter',
                'label' => 'Диаметр трубопровода',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => '',
                'label' => 'Длина шланга',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'hose-length',
                'label' => 'Угол поворота',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'span-front-legs',
                'label' => 'Размах передних опор',
                'value' => null,
            ],
            [
                'type' => 'text',
                'name' => 'span-rear-legs',
                'label' => 'Размах задних опор',
                'value' => null,
            ],
        ]);
    }
}