<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PricePeriodsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('price_periods')->insert([
            [
                'name' => 'от 5и часов, включая дорогу',
                'from' => 5,
                'to' => null
            ]
        ]);
    }
}