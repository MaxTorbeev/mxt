<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\Services\SlugService;

class PriceCurrencyDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('price_currencies')->insert([
            [
                'name' => 'RUB',
                'label' => 'Российский рубль',
                'abbr' => 'руб.'
            ],
        ]);
    }
}