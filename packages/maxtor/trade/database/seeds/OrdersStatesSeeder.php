<?php

class OrdersStatesSeeder extends \Illuminate\Database\Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders_states')->insert([
            [
                'name' => 'accepted',
                'label' => 'Принят, ожидается обработка',
                'description' => 'Заказ принят и ожидается действие менеджера',
                'color' => '',
                'send_email' => true,
                'is_hidden' => false,
                'is_paid' => false,
            ],
            [
                'name' => 'completed',
                'label' => 'Выполен',
                'description' => 'Заказ выполнен',
                'color' => '',
                'send_email' => true,
                'is_hidden' => false,
                'is_paid' => false,
            ],
            [
                'name' => 'canceled',
                'label' => 'Отменен',
                'description' => 'Заказ отменен',
                'color' => '',
                'send_email' => true,
                'is_hidden' => true,
                'is_paid' => false,
            ],
        ]);
    }
}
