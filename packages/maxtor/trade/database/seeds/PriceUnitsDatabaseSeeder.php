<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\Services\SlugService;

class PriceUnitsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('price_units')->insert([
            ['name' => 'шт'],
            ['name' => 'час'],
            ['name' => 'кв. метр'],
        ]);
    }
}