<?php

use Illuminate\Database\Seeder;

class TradePermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name' => 'create_goods_category',
                'label' => 'Создание новой категории товаров',
                'created_user_id' => 1
            ],
            [
                'name' => 'edit_goods_category',
                'label' => 'Редактирование категории товаров',
                'created_user_id' => 1
            ],
            [
                'name' => 'delete_goods_category',
                'label' => 'Удаление категории товаров',
                'created_user_id' => 1
            ],

            [
                'name' => 'create_goods',
                'label' => 'Создание нового товара',
                'created_user_id' => 1
            ],
            [
                'name' => 'edit_goods',
                'label' => 'Редактирование товара',
                'created_user_id' => 1
            ],
            [
                'name' => 'delete_goods',
                'label' => 'Удаление товара',
                'created_user_id' => 1
            ],
        ]);
    }
}
