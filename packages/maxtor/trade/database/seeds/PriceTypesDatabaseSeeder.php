<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\Services\SlugService;
use MaxTor\Location\Models\Address;

class PriceTypesDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('price_types')->insert([
            [
                'name' => 'total-cost',
                'label' => 'Общая стоимость объекта',
                'is_base' => true
            ]
        ]);
    }
}
