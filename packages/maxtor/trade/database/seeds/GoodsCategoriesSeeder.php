<?php

use Illuminate\Support\Str;


class GoodsCategoriesSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        DB::table('goods_categories')->insert([
                [
                    'slug' => 'roof',
                    'name' => 'Фальцевая кровля',
                    'page_title' => 'Собственное производство фальцевой кровли в Казани',
                    'parent_id' => null,
                    'created_user_id' => 1
                ],
                [
                    'slug' => 'wooden-house-building',
                    'name' => 'Деревянные домостроения',
                    'page_title' => 'Производство деревянных домов из двойного бруса',
                    'parent_id' => null,
                    'created_user_id' => 1,
                ],
                [
                    'slug' => 'rental-special-equipment',
                    'name' => 'Аренда спецтехники и оборудования',
                    'page_title' => 'Аренда спецтехники и оборудования в Казани',
                    'parent_id' => null,
                    'created_user_id' => 1
                ],
                [
                    'slug' => 'ventilation',
                    'name' => 'Вентиляция',
                    'page_title' => '',
                    'parent_id' => null,
                    'created_user_id' => 1
                ],

                [
                    'slug' => Str::slug('Экскаватор-погрузчик', '-'),
                    'name' => 'Экскаватор-погрузчик',
                    'page_title' => '',
                    'parent_id' => 3,
                    'created_user_id' => 1
                ],
                [
                    'slug' => Str::slug('Бетононасос', '-'),
                    'name' => 'Бетононасос',
                    'page_title' => '',
                    'parent_id' => 3,
                    'created_user_id' => 1
                ],
                [
                    'slug' => Str::slug('Строительные леса', '-'),
                    'name' => 'Строительные леса',
                    'page_title' => '',
                    'parent_id' => 3,
                    'created_user_id' => 1
                ],
            ]
        );

        \MaxTor\Trade\Models\GoodsCategory::fixTree();
    }
}