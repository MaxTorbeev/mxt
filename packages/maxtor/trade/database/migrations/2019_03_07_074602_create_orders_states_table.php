<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_states', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->index();
            $table->string('label');
            $table->string('description')->nullable();
            $table->string('color')->nullable();
            $table->boolean('send_email')->nullable();
            $table->boolean('is_hidden')->default(false)->nullable();
            $table->boolean('is_paid')->default(false)->nullable();

            $table->userManagement();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_states');
    }
}
