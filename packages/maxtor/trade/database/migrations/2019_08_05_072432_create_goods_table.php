<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('goods_category_id')->nullable()->default(null);
            $table->text('preview_photo_id')->nullable();
            $table->string('name');
            $table->string('slug')->index();
            $table->text('intro_text')->nullable();
            $table->text('full_text');
            $table->boolean('is_rent')->nullable()->comment('В аренду');

            $table->json('extra_attributes')->nullable()->comment('Дополнительные аттрибуты товара');

            $table->publishTimestamps();
            $table->seo();
            $table->userManagement();
            $table->nestedSet();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('goods_price', function (Blueprint $table) {
            $table->unsignedInteger('goods_id');
            $table->foreign('goods_id')
                ->references('id')
                ->on('goods')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedInteger('price_id');
            $table->foreign('price_id')
                ->references('id')
                ->on('prices')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods_price');
        Schema::dropIfExists('goods');
    }
}
