<div class="card personal_card">
    @isset($title)
        <div class="card-heading">
            {{ $title }}
        </div>
    @endisset
    <div class="card-body">
        <div class="personal_card_photo">
            <a href="{{ $item->frontend_url }}" title="{{ $item->full_name }}">
                <img src="{{ $item->avatar(110, 110) }}" class="img-fluid" alt="">
            </a>
        </div>
        <div class="personal_card_positionName">
            @isset($item->position->name)
                {{ $item->position->name }}
            @endisset
        </div>
        <div class="personal_card_name">
            <a href="{{ $item->frontend_url }}">
                {{ $item->first_name }}
                {{ $item->last_name }}
            </a>
        </div>
    </div>
</div>
