@extends('layouts.app')

@section('head-meta')
    <title>{{ $contact->name }} - {{ $contact->position->name }}</title>
    <meta name="keywords" content="{{ $contact->position->name }}">
    <meta name="description" content="{{ $contact->metadesc }}">
@endsection

{{--@section('breadcrumbs'){{ Breadcrumbs::render('home.personal.contactPositions.contacts', $contact) }}@endsection--}}

@section('content')

    <div class="row">
        <div class="col-md-3">
            @isset($contact->photo_preview->url)
            <a href="{{ $contact->photo_preview->url }}" data-fancybox="gallery">
                <img src="{{ $contact->avatar(398, 398) }}" alt="{{ $contact->name }}" class="img-thumbnail">
            </a>
            @endisset

            @isset($contact->attribs->socials)
                <ul class="socialMenu">
                    @foreach($contact->attribs->socials as $socialName => $socialUrl)
                        <li>
                            <a href="{{ $socialUrl }}" class="socialMenu_item socialMenu_item-{{ $socialName }}" target="_blank" rel="nofollow">
                                <i class="fa fa-{{$socialName}}"></i>
                            </a>
                        </li>
                    @endforeach
                </ul>
            @endisset
        </div>
        <div class="col-md-9">
            <h1>{{ $contact->full_name }}</h1>
            <div>
                {!! $contact->description !!}
            </div>
            <div class="card ">
                <div class="card-body">
                    <div class="card-title">
                        <strong>Оставьте свой отзыв: </strong>
                    </div>
                    <reviews
                            api-url="{{ route('frontend.personal.contacts.reviews', ['contactPositionSlug' => $contact->position->slug, 'slug' => $contact->slug])}}"
                    ></reviews>
                </div>
            </div>

        </div>
    </div>

@endsection