<?php

namespace MaxTor\Personal\Controllers\Admin;


use App\User;
use Gate;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;

use MaxTor\Content\Layouts\Admin\CategoriesListLayout;
use MaxTor\Content\Models\Attachment;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Requests\AttachmentRequest;
use MaxTor\Content\Requests\CategoryRequest;
use MaxTor\Content\Requests\PhotoRequest;
use MaxTor\Content\Requests\PostRequest;
use MaxTor\MXTCore\Controllers\AbstractAdminController;
use MaxTor\MXTCore\Controllers\DashboardController;
use MaxTor\Personal\Models\Contact;
use MaxTor\Personal\Models\ContactPosition;

class ContactsController extends AbstractAdminController
{
    protected $contactPositions = [];

    public function __construct()
    {
        parent::__construct();

        $this->contactPositions = ContactPosition::published()->get()->pluck('name', 'id');
    }

    public function create(Contact $contact)
    {
        $positions = $this->contactPositions;

        return view('personal::dashboard.contacts.create', compact('contact', 'positions'));
    }

    public function store(Contact $contact, Request $request)
    {
        if ($contact->create($request->all())) {
            return redirect(route('admin.personal.contacts.edit', $contact))->with('flash', 'Создано');
        }
    }

    public function edit(Contact $contact)
    {
        $positions = $this->contactPositions;

        return view('personal::dashboard.contacts.edit', compact('contact', 'positions'));
    }

    public function update(Contact $contact, Request $request)
    {
        if ($contact->update($request->all())) {
            return redirect(route('admin.personal.contacts.edit', $contact))->with('flash', 'Обновлено');
        }

        return redirect(route('admin.personal.contacts.edit', $contact))->with('flash-errors', 'Что то поло не так!');
    }

    public function addAttachment($id, AttachmentRequest $request)
    {
        $attachment = $this->makeAttachment($request->file('attachments'));

        Contact::where('id', $id)->firstOrFail()->addAttachment($attachment);

        return 'Done';
    }

    public function addPhoto(Contact $contact, PhotoRequest $request)
    {
        $photo = $this->makePhoto($request->file('photos'));

        $contact->addPhoto($photo);

        return 'Done';
    }
}
