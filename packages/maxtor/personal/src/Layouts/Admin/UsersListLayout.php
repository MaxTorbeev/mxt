<?php

namespace MaxTor\Personal\Layouts\Admin;

use App\User;
use MaxTor\MXTCore\Layouts\AbstractTable;
use MaxTor\MXTCore\Fields\TD;
use MaxTor\Personal\Models\Contact;

class UsersListLayout extends AbstractTable
{
    /**
     * @var string
     */
    public $data = '';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('id')->title('Код'),

            TD::name('name')->title('Логин')
                ->setRender(function ($model) {
                    return $this->editAnchor($model);
                }),

            TD::name('contacts')->title('Контакты')
                ->setRender(function ($model) {
                    return $this->getContactsAnchor($model);
                }),

            TD::name('email')->title('email'),

            TD::name('updated_at')->title('Обновлено'),
            TD::name('created_at')->title('Создано'),
        ];
    }

    protected function getContactsAnchor($model)
    {
        $contact = Contact::where('user_id', $model['id'])->first();;

        if($contact){
            return '<a href="' . route('admin.personal.contacts.edit', $contact->id) . '">Редактировать контакты</a>';
        }

    }

    protected function editAnchor($model)
    {
        return '<a href="' . route('admin.users.edit', ['id' => $model['id']]) . '">' . $model['name'] . '</a>';
    }
}