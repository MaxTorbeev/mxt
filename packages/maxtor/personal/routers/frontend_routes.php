<?php
Route::get('/{contactPosition}', [
    'as' => 'frontend.personal.contactPositions.show',
    'uses' => 'ContactPositionsController@show'
]);

Route::get('/{contactPosition}/{contact}', [
    'as' => 'frontend.personal.contacts.show',
    'uses' => 'ContactsController@show'
]);

Route::get('/{contactPosition}/{contact}/reviews/', [
    'as' => 'frontend.personal.contacts.reviews',
    'uses' => 'ContactsController@reviews'
]);

Route::post('/{contactPosition}/{contact}/reviews/', [
    'as' => 'frontend.personal.contacts.addReview',
    'uses' => 'ContactsController@addReview'
]);

// Home > ContactPosition
Breadcrumbs::for ('home.personal.contactPositions', function ($trail, $contactPosition) {
    $trail->parent('home');
    $trail->push($contactPosition->name, route('frontend.personal.contactPositions.show', ['slug' => $contactPosition->slug]));
});

// Home > ContactPosition > Contact
//Breadcrumbs::for ('home.personal.contactPositions.contacts', function ($trail, $contact) {
//    $trail->parent('home.personal.contactPositions', $contact->position, $contact);
//
//    $trail->push($contact->name, route('frontend.personal.contacts.show', [
//        'contactPositionSlug' => $contact->position->slug, 'id' => $contact
//    ]));
//});