<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
                [
                    'name' => 'О нас',
                    'slug' => 'about',
                    'created_user_id' => 1,
                ],
                [
                    'name' => 'Новости и статьи',
                    'slug' => 'articles',
                    'created_user_id' => 1,
                ],
                [
                    'name' => 'Наши работы',
                    'slug' => 'our-works',
                    'created_user_id' => 1,
                ],
                [
                    'name' => 'Контакты',
                    'slug' => 'contacts',
                    'created_user_id' => 1,
                ]
            ]
        );

        \MaxTor\Content\Models\Category::fixTree();
    }
}
