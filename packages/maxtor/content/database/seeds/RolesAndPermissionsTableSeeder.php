<?php

use Illuminate\Database\Seeder;
use MaxTor\MXTCore\Models\Permission;

class RolesAndPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'editor',
                'label' => 'Редактор',
                'created_user_id' => 1
            ],
        ]);

        DB::table('permissions')->insert([
            // Posts
            [
                'name' => 'show_post',
                'label' => 'Просмотр постов',
                'created_user_id' => 1
            ],
            [
                'name' => 'create_post',
                'label' => 'Создание постов',
                'created_user_id' => 1
            ],
            [
                'name' => 'delete_post',
                'label' => 'Удаление постов',
                'created_user_id' => 1
            ],

            // Categories
            [
                'name' => 'show_category',
                'label' => 'Просмотр категории постов',
                'created_user_id' => 1
            ],
            [
                'name' => 'create_category',
                'label' => 'Создание категории постов',
                'created_user_id' => 1
            ],
            [
                'name' => 'delete_category',
                'label' => 'Удаление категории постов',
                'created_user_id' => 1
            ],

            // Tags
            [
                'name' => 'show_tag',
                'label' => 'Просмотр тегов',
                'created_user_id' => 1
            ],
            [
                'name' => 'create_tag',
                'label' => 'Создание тегов',
                'created_user_id' => 1
            ],
            [
                'name' => 'delete_tag',
                'label' => 'Удаление тегов',
                'created_user_id' => 1
            ],
        ]);

        $this->changePermissionsToRole('editor', [
            'show_post',
            'create_post',
            'delete_post',
            'show_category',
            'create_category',
            'delete_category',
            'show_tag',
            'create_tag',
            'delete_tag',
        ]);
    }

    /**
     * Change all permissions to change role.
     *
     * @param string $role
     * @return Void
     */
    protected function changePermissionsToRole($role = 'editor', array $permissions = []): Void
    {
        $user = \App\User::where('id', 1)->first();
        $user->assignRole($role);

        $changedRole = \MaxTor\MXTCore\Models\Role::where('name', $role)->first();

        $permissions = Permission::where('name', $permissions)->get();

        foreach ($permissions as $permission) {
            $changedRole->givePermissionTo($permission);
        }
    }
}
