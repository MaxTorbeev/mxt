<div class="summary summary-small">
    <ul class="summary-list">
        <li class="summary_row">
            <div class="summary_title">{{ site_config('company_address')->label }}</div>
            <div class="summary_value">
                {{ site_config('company_address')->value }}
            </div>
        </li>
        @foreach(site_config('company_phones')->extra_attributes as $phone)
            <li class="summary_row">
                <div class="summary_title">{{ $phone['label'] }}</div>
                <div class="summary_value">
                    <a href="tel:{{ parse_phone($phone['value'], true, ['plain' => true]) }}">
                        {{ parse_phone($phone['value']) }}
                    </a>
                </div>
            </li>
        @endforeach
    </ul>
</div>