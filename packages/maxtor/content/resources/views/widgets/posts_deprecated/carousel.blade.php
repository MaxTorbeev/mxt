<div class="swiper-container" data-slides
     data-direction="horizontal"
     data-free-mode="true"
     data-slides-per-view="3"
     data-space-between="20"
>
    <div class="swiper-wrapper">
        @foreach($posts as $post)
            <article class="swiper-slide card card-noBorder article article_card">
                <time datetime="{{ $post->published_at->format('Y-m-d\TH:i:sP') }}" class="text-left mt-2 article_card_date">
                    {{ $post->published_at->format('Y-m-d') }}
                </time>
                <h4 class="article_card_title">
                    <a href="{{ $post->frontend_url }}">
                        {{ $post->name }}
                    </a>
                </h4>
                <p class="article_card_intro">
                    {{ $post->intro_text }}
                </p>
            </article>
        @endforeach
    </div>

    <div class="sliderNavigators sliderNavigators-inWrapper swiper-navigation">
        <div class="prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
        <div class="next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
    </div>
</div>