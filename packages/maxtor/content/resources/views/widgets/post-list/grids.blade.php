@inject('postWidget', 'MaxTor\Content\Widgets\PostWidget')

<div class="row">
    @foreach($posts as $post)
        <div class="col-md-4">
            {!! $postWidget->handle(['view' => 'grid', 'model' => $post]) !!}
        </div>
    @endforeach
</div>
