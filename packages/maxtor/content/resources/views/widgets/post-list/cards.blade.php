@inject('postWidget', 'MaxTor\Content\Widgets\PostWidget')

@if($posts)
    @isset($attributes['title'])
        <h3 class="text-center h5 mb-3 text-bold">{{ $attributes['title'] }}</h3>
    @elseif(isset($category))
        <h3 class="text-center h5 mb-3 text-bold">{{ $category->name }}</h3>
    @endif

    <div class="row">
        @foreach($posts as $post)
            <div class="col-6 col-md-3">
                {!! $postWidget->handle(['view' => 'card', 'model' => $post]) !!}
            </div>
        @endforeach
    </div>
@endif