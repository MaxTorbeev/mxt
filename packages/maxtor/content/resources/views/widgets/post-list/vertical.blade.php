@if($posts)
    <div class="card card-contentList bg-light bg-light py-3 my-3">

        @isset($attributes['title'])
            <h3 class="text-center h5 mb-3 text-bold">{{ $attributes['title'] }}</h3>
        @elseif(isset($category))
            <h3 class="text-center h5 mb-3 text-bold">{{ $category->name }}</h3>
        @endif

        @foreach($posts as $post)
        <div class="card mx-3">
            <div class="card-body px-0 py-0">
                <div class="row no-gutters align-items-center">
                    <div class="col-sm-3">
                        <a href="{{ $post->frontend_url }}">
                            @isset($post->photo_preview->id)
                                <img src="{{ $post->photo_preview->getThumb(75, 60) }}" class="img-fluid"  alt="{{ $post->name }} ">
                            @else
                                <img src="{{ config('mxtcore.common.defaults.photo')  }}" class="img-fluid my-2" alt="{{ $post->name }} ">
                            @endisset
                        </a>
                    </div>
                    <div class="col-sm-9 pl-2 text-line-height-2">
                        <time datetime="{{ $post->published_at->format('Y-m-d\TH:i:sP') }}" class="text-left card-text-small text-muted text-size-1 mt-2 ">
                            {{ $post->published_at->format('Y-m-d') }}
                        </time>
                        <a href="{{ $post->frontend_url }}" title="{{ $post->name }}">
                            <h5 class="card-title h6 text-bold mb-0 pr-2 text-line-height-2">
                                {{ $post->name }}
                            </h5>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endif