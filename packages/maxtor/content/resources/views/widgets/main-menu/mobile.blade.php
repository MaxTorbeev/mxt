<nav id="main-menu">
    <ul>
        <li>
            <a href="{{ route('home') }}">Главная</a>
        </li>
        @if($services)
            <li>
                <a href="#">Услуги</a>
                <ul>
                    @foreach($services as $service)
                        <li>
                            <a href="{{ $service['url'] }}">{{ $service['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
            </li>
        @endif
        @foreach($content as $contentItem)
            <li>
                <a href="{{ $contentItem['url'] }}">{{ $contentItem['name'] }}</a>
            </li>
        @endforeach
    </ul>
</nav>