<ul>
    <li class="{{ Route::currentRouteName() === 'home' ? 'active' : '' }}">
        <a href="{{ route('home') }}">Главная</a>
    </li>
    <li class="{{ request()->is('services/*') ? 'active' : '' }}">
        <a id="servicesDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true">
            Услуги
            <i class="fas fa-chevron-down"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="servicesDropdown">
            @foreach($services as $service)
                <a class="dropdown-item" href="{{ $service['url'] }}">{{ $service['name'] }}</a>
            @endforeach
        </div>
    </li>
    @foreach($content as $contentItem)
        <li class="{{ url()->current() === $contentItem['url'] ? 'active' : '' }}">
            <a href="{{ $contentItem['url'] }}">{{ $contentItem['name'] }}</a>
        </li>
    @endforeach
</ul>
