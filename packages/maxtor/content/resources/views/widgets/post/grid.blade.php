<article class="card card-post">
    <div class="row no-gutters">
        <div class="col-6">
            @foreach($model->photos as $photo)
                @if($loop->first)
                    <div class="card-grid_photo-first">
                        <picture>
                            <source media="(max-width: 1439px)" srcset="{{ $photo->getThumb(384, 300) }}">
                            <source media="(max-width: 991px)" srcset="{{ $photo->getThumb(474, 230) }}">
                            <source media="(max-width: 480px)" srcset="{{ $photo->getThumb(224) }}">
                            <img src="{{ $photo->getThumb(260) }}" class="img-fluid" alt="{{ $photo->original_name }}">
                        </picture>
                    </div>
                    <div class="card-grid_title">
                        <a href="{{ $model->frontend_url }}" title="{{ $model->name }}">
                            <h3 class="h5 my-0">{{ $model->name }}</h3>
                            <span class="text-size-1">{{ $model->intro_text }}</span>
                        </a>
                    </div>
                @endif
                @break
            @endforeach
        </div>
        <div class="col-6">
            @foreach($model->photos as $photo)
                @if($loop->iteration >= 4)
                    @break
                @endif

                @if($loop->iteration > 1)
                    <div class="card-grid_photo">
                        <picture>
                            {{--<source media="(max-width: 1439px)" srcset="{{ $photo->getThumb(384, 200) }}">--}}
                            {{--<source media="(max-width: 991px)" srcset="{{ $photo->getThumb(474, 150) }}">--}}
                            <source media="(max-width: 480px)" srcset="{{ $photo->getThumb(224, 150) }}">
                            <img src="{{ $photo->getThumb(260, ($loop->iteration > 2 ) ? 200 : 148) }}" class="img-fluid" alt="{{ $photo->original_name }}">
                        </picture>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</article>