<article class="card card-noBorder card-post">
    <div class="card-img">
        <a href="{{ $model->frontend_url }}">
            @isset($model->photo_preview->id)
                <img class="card-img-top" src="{{ $model->photo_preview->getThumb(270, 150) }}" alt="">
            @else
                <img class="card-img-top" src="{{ config('mxtcore.common.defaults.photo')  }}" alt="">
            @endisset
        </a>
        <div class="card-img-info">
            <span class="card-img-info_item text-left">{{ $model->published_at }}</span>
            <span class="card-img-info_item text-right"><i class="far fa-eye"></i> {{ views($model)->count() }}</span>
        </div>
    </div>
    <div class="card-body px-0 py-1">
        <h3 class="cart-title h5 my-3">
            <a href="{{ $model->frontend_url }}">{{ $model->name }}</a>
        </h3>
        <p class="card-title text-line-height-3 text-size-1">
            {{ nl2br($model->intro_text) }}
        </p>
    </div>
</article>
