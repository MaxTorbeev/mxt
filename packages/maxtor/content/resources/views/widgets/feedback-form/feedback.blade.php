<div class="card card-withShadow bg-light card-sticky card-sticky-top-0 card-feedback-form">
    <div class="card-body">
        @isset($attributes['title'])
            <h3 class="card-title h4 text-center mb-3">
                <strong>{{ $attributes['title'] }}</strong>
            </h3>
        @endisset

        <form action="">
            <div class="form-group">
                <label class="sr-only" for="feedback-phone">Номер телефона</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">+7</div>
                    </div>
                    <input type="text" id="feedback-phone" class="form-control" placeholder="Укажите номер телефона">
                </div>
            </div>
            <div class="form-group">
                <label class="sr-only" for="feedback-email">Email</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <i class="far fa-envelope"></i>
                        </div>
                    </div>
                    <input type="text" id="feedback-phone" class="form-control"
                           placeholder="Укажите ваш email">
                </div>
            </div>
            <div class="form-group">
                <textarea name="" id="feedback-message" class="form-control" cols="30" rows="2" placeholder="Сообщение">

                </textarea>
            </div>
            <div class="form-group card-feedback-form_disclaimer">
                Нажимая кнопку «Оставить заявку», я принимаю условия Пользовательского соглашения и даю
                своё согласие сайту на обработку моих персональных данных, в соответствии с Федеральным
                законом от 27.07.2006 года №152-ФЗ «О персональных данных», на условиях и для целей,
                определенных Политикой конфиденциальности.
            </div>
            <button class="btn btn-success btn-block" role="button">
                Оставить заявку
            </button>
        </form>
    </div>
</div>
