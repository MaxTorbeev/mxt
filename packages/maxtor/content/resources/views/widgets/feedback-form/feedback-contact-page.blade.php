<div class="card bg-light card-feedback-form">
    <div class="card-body">
        @isset($attributes['title'])
            <h3 class="card-title h4 text-center mb-3">
                <strong>{{ $attributes['title'] }}</strong>
            </h3>
        @endisset

        <form action="">
            <div class="form-row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="sr-only" for="feedback-phone">Номер телефона</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">+7</div>
                            </div>
                            <input type="text" id="feedback-phone" class="form-control" placeholder="Укажите номер телефона">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="feedback-email">Email</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="far fa-envelope"></i>
                                </div>
                            </div>
                            <input type="text" id="feedback-phone" class="form-control" placeholder="Укажите ваш email">
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <textarea name="" id="feedback-message" class="form-control" cols="30" rows="3" placeholder="Сообщение">
                        </textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-9">
                    <div class="form-group card-feedback-form_disclaimer">
                        {{ sprintf(site_config('feedback_disclaimer')->value, 'Оставить заявку') }}
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-success btn-block" role="button">Оставить заявку</button>
                </div>
            </div>
        </form>
    </div>
</div>
