<div class="swiper-container"
     data-slides
     data-direction="horizontal"
     data-free-mode="true"
     data-slides-per-view="1"
     data-space-between="20"
>
    <div class="swiper-wrapper">
        @foreach ($model->photos as $photo)
            <div class="card card-withImageOverlay bg-dark text-white">
                <img class="card-img" src="{{ asset($photo->getThumb(1090, 500)) }}" alt="{{ $photo->original_name }}">
                <div class="card-img-overlay card-img-overlay-center px-5 py-5">
                    <h2 class="card-title">Собственное производство фальцевой кровли в Казани</h2>
                    <p class="card-text">Закажи прямо сейчас и получи выгоду.</p>
                    <p class="card-text">
                        <a href="#" class="btn btn-success px-4">
                            Узнать подробнее <i class="fas fa-angle-right"></i>
                        </a>
                    </p>
                </div>
            </div>
        @endforeach
    </div>

    <div class="sliderNavigators sliderNavigators-inWrapper swiper-navigation">
        <div class="prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
        <div class="next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
    </div>
</div>
