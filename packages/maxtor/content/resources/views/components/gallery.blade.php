@if($model->photos->count())
    <div class="carousel_gallery">
        <div class="swiper-container gallery-top mr-4">
            <div class="swiper-wrapper">
                @foreach($model->photos as $photo)
                    <div class="swiper-slide">
                        <a href="{{ $photo->url }}" data-fancybox="photo-{{ $model->id }}">
                            <picture>
                                <source media="(max-width: 1439px)" srcset="{{ $photo->getThumb(624, 460) }}">
                                <source media="(max-width: 480px)" srcset="{{ $photo->getThumb(450, 230) }}">
                                <img src="{{ $photo->getThumb(940, 460) }}" alt="">
                            </picture>
                        </a>
                    </div>
                @endforeach
            </div>
            {{-- Add Arrows --}}
            <div class="sliderNavigators sliderNavigators-inWrapper swiper-navigation">
                <div class="prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
                <div class="next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
            </div>
        </div>
        <div class="swiper-container gallery-thumbs">
            <div class="swiper-wrapper">
                @foreach($model->photos as $photo)
                    <div class="swiper-slide" style="background-image:url({{ $photo->getThumb(126, 120) }})"></div>
                @endforeach
            </div>
            <div class="sliderNavigators sliderNavigators-inWrapper swiper-navigation">
                <div class="prev"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
                <div class="next"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
            </div>
        </div>
    </div>
@endif
