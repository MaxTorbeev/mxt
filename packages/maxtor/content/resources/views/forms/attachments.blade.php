<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        @foreach($data as $dataKeyTab => $itemTab)
            <a id="nav-{{ $dataKeyTab }}-tab" class="nav-item nav-link {{ $loop->first ?  'active' : '' }} " data-toggle="tab" href="#{{ $dataKeyTab }}-tab"
               role="tab"
            >
                {{ $dataKeyTab }}
            </a>
        @endforeach
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    @foreach($data as $dataKey => $item)
        <div id="{{ $dataKey }}-tab" class="tab-pane show {{ $loop->first ?  'active' : '' }}">
            <upload-files url="{{ $item['upload-url'] }}" field-name="{{ $item['upload-field-name'] ?? $dataKey }}"></upload-files>
            <file-thumbs url="{{ $item['thumbs-url'] }}"></file-thumbs>
        </div>
    @endforeach
</div>
