@extends('layouts.app')

@inject('contactsWidget', 'MaxTor\Content\Widgets\ContactsWidget')

@section('head-meta')
    <title>@yield('title') - {{ site_config('site_name')->value }}</title>
    <meta name="keywords" content="@yield('meta-keywords')">
    <meta name="description" content="@yield('meta-desc')">
@endsection

@section('content')
    <h1 class="h1 text-bold">@yield('title')</h1>
    @if(View::hasSection('content_description'))
        <div class="lead">@yield('content_description')</div>
    @endif
    @yield('content_body')
@endsection

@section('aside_right')
 {!! $contactsWidget->handle() !!}
@endsection

