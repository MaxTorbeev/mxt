@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row align-items-center">
        </div>

        <div class="content_description mb-4">
            <div class="card card-aboutService card-withShadow">

                <div class="card-heading mt-4">
                    <h1 class="text-center">Оставить свой отзыв</h1>
                </div>

                <div class="card-body">

                    <div class="alert alert-info">
                        Любой отзыв перед публикацией будет проверен администраторами сайта.
                    </div>

                    {!! Form::model($review = new \MaxTor\Content\Models\Review(), ['url' => route('frontend.content.reviews.store')]) !!}
                    <div class="form-row">
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('user_name', 'Ваше имя:') !!}
                                {!! Form::text('user_name', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('user_email', 'Ваш email:') !!}
                                {!! Form::text('user_email', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('user_phone', 'Ваш номер телефона:') !!}
                                {!! Form::text('user_phone', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('rating', 'Ваша оценка:') !!}
                                {!! Form::select('rating',  [ 'Без оценки', '1', '2', '3', '4', '5' ], null, ['class'=>'form-control custom-select']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-12">

                            <div class="form-group">
                                {!! Form::label('name', 'Заголовок:') !!}
                                {!! Form::text('name', null, ['class'=>'form-control']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('body', 'Отзыв:') !!}
                                {!! Form::textarea('body', null, ['class'=>'form-control', 'rows'=>'6' ]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-2">
                        {!! Form::submit( 'Оставить отзыв', ['class' => 'btn btn-primary-withShadow btn-rounded btn-block']) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
@endsection


