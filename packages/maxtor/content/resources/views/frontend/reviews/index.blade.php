@extends('layouts.app')

@section('head-meta')
    <title>Отзывы</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
@endsection

@section('content')
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <h1>Отзывы </h1>
            </div>
            <div class="col-md-4">
                <a href="{{ route('frontend.content.reviews.create') }}" class="btn btn-danger-withShadow btn-rounded pull-right">
                    Оставить свой отзыв
                </a>
            </div>
        </div>

        <div class="content_description mb-4">
        </div>

        @forelse($reviews as $review)
            <div class="card card-withShadow card-hovered card-review mb-4">
                <div class="card-body">
                    <i class="fas fa-quote-right"></i>
                    <i>{{ $review->user_name }}</i> - <i>{{ $review->title }}</i> <br>
                    {{ $review->body }}
                    <i class="fas fa-quote-right"></i>
                </div>
            </div>
        @empty
            <div class="alert alert-info my-5">
                Пока что отзывы ни кто не оставлял, Вы сможете быть первым

                <a href="{{ route('frontend.content.reviews.create') }}" class="btn btn-rounded btn-success ml-2">
                    Оставить свой отзыв
                </a>
            </div>
        @endforelse

    </div>
@endsection


