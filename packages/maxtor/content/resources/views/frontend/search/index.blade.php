@extends('layouts.app')

@section('head-meta')
    <title>Поиск по сайту</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
@endsection

@section('breadcrumbs') {{ Breadcrumbs::render('home.search') }} @endsection

@section('content')
    <div class="container">
        <section class="section">
            <div class="row justify-content-center">
                <div class="col-md-12 col-lg-8">

                    <h1 class="">Поиск по сайту</h1>

                    <div class="card card-withShadow my-5">
                        <div class="card-body">
                            <form action="{{ route('frontend.content.search') }}" method="GET"
                                  class="form-row align-items-center">
                                <div class="col-10">
                                    <label class="sr-only" for="query">Username</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-search"></i></div>
                                        </div>
                                        <input id="query" name="query" value="{{ request('query') }}" type="text" class="form-control" placeholder="Поиск по сайту">
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <button type="submit" class="btn btn-block btn-primary mb-2">Поиск</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    @if($results)
                        <h3>Результаты поиска:</h3>
                        @foreach($results as $result)
                            <ul class="list-group">
                                @foreach($result as $item)
                                    <li class="list-group-item">
                                        <a href="{{ $item->frontend_url }}">{{ $item->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endforeach
                    @endif

                </div>
            </div>
        </section>
    </div>

@endsection