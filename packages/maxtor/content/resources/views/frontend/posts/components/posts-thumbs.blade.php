<div class="swiper-container"
     data-slides-per-view="auto"
     data-free-mode="true"
     data-space-between="30"
>
    <div class="swiper-wrapper">
        @forelse($posts as $post)
            <div class="swiper-slide">
                <div class="card card-contentItem text-white">
                    @isset($post->photo_preview)
                        <a href="{{ $post->frontend_url }}" class="card-contentItem_image" title="{{ $post->name }}">
                            <img class="card-img" src="{{ $post->photo_preview->thumb }}" alt="{{ $post->name }}">

                            @isset($post->published_at)
                            <time datatype="{{ $post->published_at->format('Y-m-d\TH:i:sP') }}" class="card-contentItem_image_publishedDate text-left text-muted">
                                {{ trans_choice('content.published_at', null, ['date' => $post->published_at->diffForHumans()]) }}
                            </time>
                            @endisset
                        </a>
                    @endisset

                    <a href="{{ $post->frontend_url }}" title="{{ $post->name }}" class="card-contentItem_title">
                        {{ $post->name }}
                    </a>
                </div>
            </div>
            @endforeach
    </div>

</div>