<div class="col-md-4">
    <img src="{{ $post->photo_preview->thumb }}" alt="{{ $post->photo_preview->name }}" class="img-fluid">

    <div class="content_published">
        <time datatype="{{ $post->published_at->format('Y-m-d\TH:i:sP') }}" class="text-left">
            {{ trans_choice('content.published_at', null, ['date' => $post->published_at->diffForHumans()]) }}
        </time>
    </div>

    <div class="content_name py-3">
        <a href="{{ $post->frontend_url }}" class="h1 my-5" title="{{ $post->name }}">{{ $post->name }}</a>
    </div>

    <div class="content_intro_text">
        {{ $post->intro_text }}
    </div>
</div>
