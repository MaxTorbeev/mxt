@extends('content::frontend.layouts.content')

@inject('postWidget', 'MaxTor\Content\Widgets\PostWidget')
@inject('postListWidget', 'MaxTor\Content\Widgets\PostListWidget')
@inject('goodsCategoryCarouselWidget', 'MaxTor\Trade\Widgets\GoodsCategoryCarouselWidget')

@section('meta-keywords'){{ $post->metakey }}@endsection
@section('meta-description'){{ $post->metadesc }}@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('home.content.categories.post', $post) }}
@endsection

@section('title'){{ $post->name }} @endsection

@section('content')
    @parent
    @if($post->photos->count() === 1)
        {!! $goodsCategoryCarouselWidget->handle([ 'view' => 'simple', 'model' => $post ]) !!}
    @elseif($post->photos->count() >= 2)
        @include('content::components.gallery', ['model' => $post])
    @endif

    {!! $post->full_text  !!}
    @foreach($post->tags as $tag)
        <a href="{{ route('frontend.content.tags.show', ['slug' => $tag->slug]) }}"
           class="btn btn-outline-dark btn-sm my-1">
            {{ $tag->name }}
        </a>
    @endforeach
@endsection

@section('aside_right')

    {!! $postListWidget->handle([
        'view' => 'vertical',
        'title' => $post->category->name,
        'posts' => $post->category->posts
    ]) !!}

    @parent
@endsection
