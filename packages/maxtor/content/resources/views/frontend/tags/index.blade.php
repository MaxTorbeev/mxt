@extends('content::frontend.layouts.content')

@section('meta-keywords')@endsection
@section('meta-description')@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('home.content.tags') }}
@endsection

@section('title') {{ __('content.tags') }} @endsection

@section('content_body')
    @foreach($tags as $tag)
        <a href="{{ route('frontend.content.tags.show', $tag) }}" class="btn btn-outline-dark btn-sm">
            {{ $tag->name }}
        </a>
    @endforeach
@endsection

@section('content')
    @parent
@endsection
