@extends('content::frontend.layouts.content')

@inject('goodsCard', 'MaxTor\Trade\Widgets\GoodsCardWidget')
@inject('postWidget', 'MaxTor\Content\Widgets\PostWidget')

@section('title') Материалы по тэгу «{{ $tag->head_title ? $tag->head_title : $tag->name }}» @endsection
@section('meta-keywords') {{ $tag->metakey }} @endsection
@section('meta-desc') {{ $tag->metadesc }} @endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('home.content.tags.show', $tag) }}
@endsection

@section('content_description')
    {!! $tag->description !!}
@endsection

@section('content_body')

    <div class="row">
        @foreach($tag->posts as $post)
            <div class="col-md-3">
                {!! $postWidget->handle(['view' => 'card', 'model' => $post]) !!}
            </div>
        @endforeach
    </div>

    <div class="section_divider mt-4 mb-2">
        <span class="section_divider_title">{{ __('trade.services') }}</span>
    </div>

    <div class="row">
        @foreach($tag->goods as $goods)
            <div class="col-md-3">
                {!! $goodsCard->handle(['view' => 'block', 'goods' => $goods]) !!}
            </div>
        @endforeach
    </div>
@endsection

@section('content')
    @parent
@endsection
