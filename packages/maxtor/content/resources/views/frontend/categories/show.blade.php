@extends('content::frontend.layouts.content')

@inject('postWidget', 'MaxTor\Content\Widgets\PostWidget')

@section('meta-keywords'){{ $category->metakey }}@endsection
@section('meta-description'){{ $category->metadesc }}@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('home.content.categories', $category) }}
@endsection

@section('title')
    {{ $category->name }}
@endsection

@section('content')
    @parent
    <div class="row">
        @foreach($categoryPosts as $post)
            <div class="col-6 col-xl-3  mb-4">
                {!! $postWidget->handle(['view' => 'card', 'model' => $post]) !!}
            </div>
        @endforeach
    </div>

    @if($category->description)
        <div class="section_description mb-md-5">
            {!! $category->description   !!}
        </div>
    @endif

    <ul class="list-group list-group-flush">
        @foreach($category->children as $children)
            <li class="list-group-item">
                <a href="{{ $children->frontend_url }}">{{ $children->name }}</a>
            </li>
        @endforeach
    </ul>
@endsection

@section('aside_right')
    @parent
@endsection


