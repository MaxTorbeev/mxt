@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.content.categories.edit', $category) }} @endsection

@section('content')
    <div class="card">
        <div class="card-header">
            Редактирование категория
        </div>

        <div class="card-body">
            {!! Form::model($category, ['url' => route('admin.content.categories.update', [$category->id]), 'files' => true]) !!}

            <div class="form-row">
                <div class="col-md-9">
                    @method('PATCH')
                    @include ('content::dashboard.categories.form', ['submitButtonText' => 'Редактировать категорию'])
                </div>
                <div class="col-md-3">
                    @include('content::forms.attachments',
                     [
                        'data' =>
                        [
                            'photos' => [
                                'upload-url' => route('admin.content.categories.photo', ['id' => $category->id]),
                                'thumbs-url' => route('api.content.photo.list', ['subject_id' => $category->id,'type' => $category->getPhotoType()])
                            ],
                            'attachments' => [
                                'upload-url' => route('admin.content.categories.attachment', ['id' => $category->id]),
                                'thumbs-url' => route('api.content.attachment.list',['subject_id' => $category->id,'type' => $category->getAttachmentType()])
                            ]
                        ]
                    ])
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
