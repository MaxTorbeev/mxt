<form action="{{ url()->full() }}" method="GET">
    <div class="row align-items-end">
        <div class="col">
            {!! Form::label('category', 'Фильтровать по категории:') !!}
            {!! Form::select('category', $categories, request('category'), ['class'=>'form-control']) !!}

            @if(request('page'))
                <input type="hidden" name="page" value="{{ request('page') }}">
            @endif
        </div>
        <div class="col-auto">
            <button type="submit" class="btn btn-primary">Ок</button>
        </div>
    </div>
</form>