@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.content.posts.index') }} @endsection

@section('content')

    <ul class="nav nav-pills mb-2">
        <li class="nav-item">
            <a class="nav-link btn btn-success" href="{{ route('admin.content.posts.create') }}">Добавить пост</a>
        </li>
    </ul>

    <div class="card">
        <div class="p-2">
            @include ('content::dashboard.posts._filters')
        </div>
    </div>

    <div class="card">
        <div class="p-2">
            {!! $table->build() !!}
        </div>
    </div>

@endsection