@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.content.posts.create') }} @endsection

@section('content')
    <div class="card">
        <div class="card-header">
            Новый пост
        </div>

        <div class="card-body">
            {!! Form::model($post = new \MaxTor\Content\Models\Post(), ['url' => route('admin.content.posts.store'), 'files' => true]) !!}
            @include ('content::dashboard.posts.form', ['submitButtonText' => 'Добавить новый пост' ])
            {!! Form::close() !!}
        </div>
    </div>
@endsection