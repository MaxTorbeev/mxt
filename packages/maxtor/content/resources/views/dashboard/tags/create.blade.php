@extends('mxtcore::layouts.dashboard')

@section('content')
    {!! Form::model($tag = new \MaxTor\Content\Models\Tag(), ['url' => route('admin.content.tags.store'), 'files' => true]) !!}
    @include ('content::dashboard.tags.form', ['submitButtonText' => 'Добавить новый тег' ])
    {!! Form::close() !!}

@stop