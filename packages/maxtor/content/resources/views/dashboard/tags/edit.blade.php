@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.content.tags.edit', $tag) }} @endsection

@section('content')
    <div class="card">
        <div class="card-header">
            Редактировать товар
        </div>
        <div class="card-body">
            {!! Form::model($tag , ['url' => route('admin.content.tags.update', ['id' => $tag->id]), 'files' => true]) !!}
            @method('PATCH')
            @include ('content::dashboard.tags.form', ['submitButtonText' => 'Редактировать тег' ])
            {!! Form::close() !!}
        </div>
    </div>
@endsection
