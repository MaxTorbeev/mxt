<div class="form-row">
    <div class="col-md-8">
        {!! Form::label('name', 'Название:', ['class' => 'sr-only']) !!}
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text">Название</div>
            </div>
            {!! Form::text('name', $tag->name, ['class'=>'form-control']) !!}
            @if ($errors->has('name'))
                <small class="form-control-feedback">{{ $errors->first('name') }}</small>
            @endif
        </div>
    </div>

    <div class="col-md-4">
        {!! Form::label('slug', 'Ссылка:', ['class' => 'sr-only']) !!}
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="fa fa-link" aria-hidden="true"></i></div>
            </div>
            {!! Form::text('slug', null, ['class'=>'form-control']) !!}
            @if ($errors->has('slug'))
                <small class="form-control-feedback">{{ $errors->first('slug') }}</small>
            @endif
        </div>
    </div>
</div>

<div class="form-row">
    <div class="col-md-9">
        <div class="form-group">
            <tinymce name="description" html="{!! $tag->description  !!}"></tinymce>
        </div>
    </div>
    <div class="col-md-3">
        @include ('mxtcore::forms.user-management', ['model' => $tag])
        @include ('content::forms.seo', ['model' => $tag])
    </div>
</div>

<div class="form-group">
    {!! Form::submit( $submitButtonText, ['class' => 'btn btn-sm btn-primary']) !!}

    <a href="{{ route('admin.content.tags.index') }}" class="btn btn btn-secondary">Отменить и перейти к списку</a>
</div>

