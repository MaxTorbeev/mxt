@extends('mxtcore::layouts.dashboard')

@section('content')

    <ul class="nav nav-pills mb-2">
        <li class="nav-item">
            <a class="nav-link btn btn-success" href="{{ route('admin.content.tags.create') }}">Добавить тег</a>
        </li>
    </ul>


    <div class="card">
        <div class="card-header">
            Посты
        </div>

        <div class="card-body">
            <table class="table table-sm table-responsive-sm table-hover table-outline mb-0">
                <thead class="thead-inverse">
                <tr>
                    <th>Тег</th>
                    <th>Автор</th>
                    <th>Алиас</th>
                    <th>id</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($tags as $tag)
                    <tr>
                        <td><a href="{{ route( 'admin.content.tags.edit', [$tag->id]) }}">{{ $tag->name }}</a></td>
                        <td>{{ $tag->author->name }}</td>
                        <td>{{ $tag->slug }}</td>
                        <th scope="row">{{ $tag->id }}</th>
                        <td>
                            <delete action="{{ route('admin.content.tags.destroy', ['id' => $tag->id]) }}">X</delete>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop