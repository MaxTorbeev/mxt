@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.content.carousels.index') }} @endsection

@section('content')

    <ul class="nav nav-pills mb-2">
        <li class="nav-item">
            <a class="nav-link btn btn-success" href="{{ route('admin.content.carousels.create') }}">Добавить слайдер</a>
        </li>
    </ul>

    <div class="card">
        <div class="card-header">
            Слайдеры
        </div>

        <div class="card-body">
            {!! $table->build() !!}
        </div>
    </div>
@stop