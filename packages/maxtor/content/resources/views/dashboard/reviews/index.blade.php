@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.content.reviews.index') }} @endsection

@section('content')

    <ul class="nav nav-pills mb-2">
        <li class="nav-item">
            <a class="nav-link btn btn-success" href="{{ route('admin.content.reviews.create') }}">Добавить отзыв</a>
        </li>
    </ul>

    <div class="card">
        <div class="card-header">
            Посты
        </div>

        <div class="card-body">

            {!! $table->build() !!}


            {{--@include('mxtcore::dashboard.system.data.table', [ 'models' => $reviews, 'attributes' => [--}}
                {{--], 'routes' => [--}}
                    {{--'edit_route' => 'admin.content.reviews.edit',--}}
                    {{--'delete_route' => 'admin.content.reviews.destroy',--}}
                {{--]--}}
            {{--])--}}
        </div>
    </div>

@endsection