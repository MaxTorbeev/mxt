@extends('mxtcore::layouts.dashboard')

@section('breadcrumbs') {{ Breadcrumbs::render('admin.content.reviews.create') }} @endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Добавить отзыв
        </div>

        <div class="card-body">
            {!! Form::model($review = new \MaxTor\Content\Models\Review(), ['url' => route('admin.content.reviews.store')]) !!}
            @include ('content::dashboard.reviews.form', ['submitButtonText' => 'Добавить отзыв' ])
            {!! Form::close() !!}
        </div>
    </div>

@endsection