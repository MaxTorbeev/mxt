<?php

namespace MaxTor\Content\Controllers\Admin;

use Illuminate\Http\UploadedFile;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Models\Carousel;
use Illuminate\Http\Request;
use MaxTor\Content\Layouts\Admin\SlidersListLayout;
use MaxTor\Content\Requests\PhotoRequest;
use MaxTor\MXTCore\Controllers\AbstractAdminController;
use MaxTor\MXTCore\Controllers\DashboardController;
use MaxTor\Content\ContentPackageHelper;

class CarouselsController extends AbstractAdminController
{
    protected $helper;

    public function __construct(ContentPackageHelper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carousels = Carousel::all();
        $table = new SlidersListLayout($carousels);

        return $this->view('index', ['table' => $table]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->view('create', compact('categories' ));
    }

    public function store(Carousel $carousel, Request $request)
    {
        $carousel = Carousel::create($request->all());

        return redirect(route('admin.content.carousels.edit', $carousel))->with('flash', 'Успешно');
    }

    public function edit(Carousel $carousel)
    {
        return $this->view('edit', ['model' => $carousel]);
    }

    public function update(Request $request, Carousel $carousel)
    {
        $carousel->update($request->all());

        return redirect(route('admin.content.carousels.edit', ['id' => $carousel->id]))->with('flash', 'Успешно');
    }

    public function destroy(Carousel $carousel)
    {
        if($carousel->delete()){
            return redirect(route('admin.content.carousels.index'))->with('flash', 'Удалено');
        }

        return abort(400, 'Что то пошло не так');
    }

    public function addPhoto($id, PhotoRequest $request)
    {
        $photo = $this->makePhoto($request->file('photos'));
        Carousel::where('id', $id)->firstOrFail()->addPhoto($photo);

        return 'Done';
    }

    protected function makePhoto(UploadedFile $file)
    {
        return Photo::named($file)->move($file);
    }
}
