<?php

namespace MaxTor\Content\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Gate;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

use MaxTor\Content\Models\Category;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Models\Post;
use MaxTor\Content\Models\Tag;
use MaxTor\Content\Requests\CategoryRequest;
use MaxTor\Content\Requests\PhotoRequest;
use MaxTor\Content\Requests\PostRequest;
use MaxTor\MXTCore\Controllers\DashboardController;

class PhotosController extends DashboardController
{
    public function move(Photo $photo, Request $request)
    {
        if ($request->has('move')) {
            Photo::rebuildTree($request->get('move'));

            return response()->json(['message' => 'Файл успешно перемещен']);
        }

        return response()->json(['message' => 'Не указан обязательный параметр'], 403);
    }

    public function update(Photo $photo, Request $request)
    {
        $photo->update([
            'attribs' => json_encode($request->input('attribs')),
            'original_name' => $request->input('original_name'),
            'description' => $request->input('description'),
        ]);

        return response()->json(['message' => 'Файл успешно отредактирован']);
    }

    public function destroy($id)
    {
        $photo = Photo::where('id', $id)->firstOrFail();

        if ($photo->delete()) {
            return response()->json(['message' => 'Файл успешно был удален']);
        }
    }
}
