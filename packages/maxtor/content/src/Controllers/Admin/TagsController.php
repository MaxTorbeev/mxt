<?php

namespace MaxTor\Content\Controllers\Admin;

use MaxTor\Content\ContentPackageHelper;
use MaxTor\Content\Models\Tag;
use MaxTor\Content\Requests\TagRequest;
use MaxTor\MXTCore\Controllers\AbstractAdminController;

class TagsController extends AbstractAdminController
{
    protected $helper;

    public function __construct(ContentPackageHelper $helper)
    {
        $this->helper = $helper;

        parent::__construct();
    }

    public function index()
    {
        return $this->view('index', [ 'tags' => Tag::all()]);
    }

    public function create()
    {
        $this->authorize('create_tag');

        return $this->view('create');
    }

    public function store(TagRequest $request)
    {
        $this->authorize('create_tag');

        $tag = Tag::create($request->all());

        return redirect(route('admin.content.tags.edit', ['id' => $tag->id]))
            ->with('flash', 'Тег создан успешно');
    }

    public function edit(Tag $tag)
    {
        $this->authorize('create_tag');

        return $this->view('edit', compact('tag') );
    }

    public function update(Tag $tag, TagRequest $request)
    {
        $this->authorize('create_tag');

        $tag->update($request->all());

        return redirect(route('admin.content.tags.edit', ['id' => $tag->id]))
            ->with('flash', 'Тег создан бы редактирован');
    }

    public function destroy(Tag $tag)
    {
        if ($tag->delete()){
            return response()->json(['message' => 'Тег успешно был удален']);
        }

        return response([], 401)->json(['message' => 'Невозможно удалить тег']);
    }
}
