<?php

namespace MaxTor\Content\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use MaxTor\Content\Layouts\Admin\ReviewsListLayout;
use MaxTor\Content\Mail\ReviewAdd;
use MaxTor\Content\Models\Content;
use MaxTor\Content\Models\Review;
use MaxTor\Content\Requests\ReviewRequest;
use MaxTor\MXTCore\Controllers\DashboardController;

class ReviewsController extends DashboardController
{
    public function index()
    {
        $table = new ReviewsListLayout(Review::all());

        return view('content::dashboard.reviews.index', compact('table'));
    }

    public function create()
    {
        return view('content::dashboard.reviews.create');
    }

    public function store(Review $review, ReviewRequest $request)
    {
        $review = $review::create($request->all());

//        Mail::to('MXTSoftProw@yandex.ru')->send(new ReviewAdd($review));

        return redirect(route('admin.content.reviews.edit', $review))->with('flash', 'Отзыв добавлен');
    }

    public function edit(Review $review)
    {
        return view('content::dashboard.reviews.edit', compact('review'));
    }

    public function update(Review $review, ReviewRequest $request)
    {
        $review->update($request->all());

        return view('content::dashboard.reviews.edit', compact('review'))->with('flash', 'Отзыв обновлен');
    }

    public function destroy(Review $review)
    {
        if ($review->delete() && request()->wantsJson()) {
            return response(['message' => 'Отзыв удален!'], 200);
        }
    }
}
