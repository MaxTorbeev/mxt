<?php

namespace MaxTor\Content\Controllers\Frontend;

use MaxTor\Content\Models\Tag;
use MaxTor\Content\ContentPackageHelper;
use MaxTor\MXTCore\Controllers\AbstractFrontendController;

class TagsController extends AbstractFrontendController
{
    protected $helper;

    public function __construct(ContentPackageHelper $helper)
    {
        $this->helper = $helper;
    }

    public function index()
    {
        $tags = Tag::published()->get();

        return $this->view('index', compact('tags'));
    }

    public function show(Tag $tag)
    {
        return $this->view('show', compact('tag'));
    }
}
