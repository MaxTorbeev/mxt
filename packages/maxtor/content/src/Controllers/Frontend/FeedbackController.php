<?php

namespace MaxTor\Content\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use MaxTor\Content\Mail\BackCall;
use MaxTor\Content\Models\Feedback;
use MaxTor\Content\Requests\BackCallRequest;
use MaxTor\MXTCore\Models\Role;

class FeedbackController extends Controller
{
    protected $rootUsersEmails = [];

    public function backCall(BackCallRequest $request)
    {
        if ($request->isMethod('get')) {
            return [
                'title' => 'Обратный звонок',
                'submit' => 'Отправить',
            ];
        }

        Mail::to($this->rootUsersEmails)->send(new BackCall($request));

        return response()->json(['message' => 'Сообщение отправлено!']);
    }

    public function consultation(Request $request)
    {
        $request->validate([
            'phone' => 'required',
        ]);

        Feedback::create($request->all());
    }
}
