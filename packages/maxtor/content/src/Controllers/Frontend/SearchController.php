<?php

namespace MaxTor\Content\Controllers\Frontend;

use App\Http\Flash;
use Gate;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;

use MaxTor\Content\Models\Category;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Models\Post;
use MaxTor\Content\Models\Tag;
use MaxTor\Content\Requests\PostRequest;
use MaxTor\Portfolio\Models\Location;
use MaxTor\Portfolio\Models\Object;

class SearchController extends Controller
{
    public function index()
    {
        $results = null;

        $query = request('query');

        if ($query) {
            $categories = Category::search($query)->published()->get();
            $posts = Post::search($query)->published()->get();
            $locations = Location::search($query)->published()->get();
            $objects = Object::search($query)->published()->get();

            $results = collect([$categories, $posts, $locations, $objects]);
        }

        return view('content::frontend.search.index', compact('results'));
    }
}