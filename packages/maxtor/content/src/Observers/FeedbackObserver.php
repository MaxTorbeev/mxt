<?php

namespace MaxTor\Content\Observers;

use Illuminate\Support\Facades\Mail;
use MaxTor\Content\Mail\BackCall;
use MaxTor\Content\Models\Feedback;
use MaxTor\MXTCore\Models\Role;

class FeedbackObserver
{
    public function __construct()
    {
        $this->rootUsersEmails = Role::where('name', 'root')->first()->users->pluck('email');
    }

    public function created(Feedback $feedback)
    {
        $feedback->html = (new BackCall($feedback))->render();
        $feedback->senders_ip = request()->ip();
        $feedback->senders_user_agent = request()->header('User-Agent');
        $feedback->where_sent = request()->headers->get('referer');

        try{
            Mail::to($this->rootUsersEmails)->send(new BackCall($feedback));
            $feedback->status = Feedback::STATUS_SENDING_SUCCESS;
            $feedback->update();

        } catch (\Exception $e){
            $feedback->status = Feedback::STATUS_SENDING_ERROR;
            $feedback->log = $e->getMessage();
            $feedback->update();

            throw new \Swift_TransportException($e->getMessage());
        }
    }
}
