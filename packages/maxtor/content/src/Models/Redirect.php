<?php

namespace MaxTor\Content\Models;

use Illuminate\Database\Eloquent\Model;

class Redirect extends Model
{
    protected $table = 'redirects';

    protected $guarded = ['id'];
}