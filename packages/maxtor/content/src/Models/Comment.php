<?php

namespace MaxTor\Content\Models;

use Illuminate\Database\Eloquent\Model;
use MaxTor\Content\Traits\ContentScopes;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\UserManagement;

class Comment extends Model
{
    use Cacheable, UserManagement, ContentScopes;

    protected $guarded = ['id'];
}
