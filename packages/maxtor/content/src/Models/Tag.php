<?php

namespace MaxTor\Content\Models;

use MaxTor\Content\Traits\ContentScopes;
use MaxTor\Content\Traits\Sluggable;
use Illuminate\Database\Eloquent\Model;
use MaxTor\MXTCore\Traits\UserManagement;
use MaxTor\Trade\Models\Goods;

class Tag extends Model
{
    use Sluggable, UserManagement, ContentScopes;

    protected $guarded = ['id'];

    public function getRouteKeyName()
    {
        if (request()->is('admin/*'))
            return 'id';

        return 'slug';
    }

    /**
     * Get the posts associated with the give tag.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->morphedByMany(Post::class, 'taggable')->withTimestamps();
    }

    public function goods()
    {
        return $this->morphedByMany(Goods::class, 'taggable')->withTimestamps();
    }
}
