<?php

namespace MaxTor\Content\Models;

use Illuminate\Database\Eloquent\Model;
use Laravelrus\LocalizedCarbon\Traits\LocalizedEloquentTrait;
use MaxTor\Content\Traits\ContentScopes;
use MaxTor\Content\Traits\RecordsPhoto;
use MaxTor\Content\Traits\RecordsTag;
use MaxTor\Content\Traits\Searchable;
use MaxTor\Content\Traits\Sluggable;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\UserManagement;

class Carousel extends Model
{
    use RecordsPhoto,
        RecordsTag,
        Sluggable,
        Cacheable,
        UserManagement,
        ContentScopes,
        LocalizedEloquentTrait,
        Searchable;

    protected $guarded = ['id'];

    public $cacheKeys = ['slider:main-page-top'];

}
