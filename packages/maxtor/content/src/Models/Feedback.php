<?php

namespace MaxTor\Content\Models;

use Illuminate\Database\Eloquent\Model;
use MaxTor\MXTCore\Traits\Cacheable;
use MaxTor\MXTCore\Traits\UserManagement;

class Feedback extends Model
{
    use Cacheable, UserManagement;

    public $mailSubject = 'Запись на консультация';
    public $mailTemplate = 'content::emails.backcall.index';
    public $sendingModel;

    const STATUS_SENDING_ERROR = 'sending_error';
    const STATUS_SENDING_SUCCESS = 'sending_success';

    private static $titles = [
        self::STATUS_SENDING_ERROR => 'Feedback cannot be sent',
        self::STATUS_SENDING_SUCCESS  => 'Feedback has been sending',
    ];

    protected $table = 'feedbacks';

    protected $guarded = ['id'];
}
