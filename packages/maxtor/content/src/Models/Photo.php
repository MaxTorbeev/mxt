<?php

namespace MaxTor\Content\Models;

use Illuminate\Support\Facades\File;
use Image;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Exception\NotReadableException;
use Laravelrus\LocalizedCarbon\LocalizedCarbon;
use MaxTor\Content\Models\Post;
use MaxTor\Content\Traits\ContentScopes;
use MaxTor\Content\Traits\Searchable;
use MaxTor\Content\Traits\RecordsPhoto;
use MaxTor\MXTCore\Traits\Cacheable;
use Intervention\Image\Exception\InvalidArgumentException;
use \Laravelrus\LocalizedCarbon\Traits\LocalizedEloquentTrait;

class Photo extends Model
{
    use LocalizedEloquentTrait, ContentScopes;

    protected $guarded = ['id'];

    protected $baseDir = 'upload/photos';

    protected $appends = [
        'thumb',
        'url',
        'updated_at_for_human',
        'created_at_for_human',
        'update',
        'delete'
    ];

    protected $cacheKeys = ['mapProductPhotos'];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($photo) {
            if (Photo::where('name', $photo->name)->count() === 1) {
                $photo = public_path($photo->makePath() . '/' . $photo->name);

                if (file_exists($photo))
                    unlink($photo);
            }
        });
    }

    public function posts()
    {
        return $this->belongsTo(Post::class);
    }

    public function getUrlAttribute()
    {
        return asset($this->path . '/' . $this->name);
    }

    public function getUpdateAttribute()
    {
        return route('admin.content.photos.update', ['id' => $this->id]);
    }

    public function getDeleteAttribute()
    {
        return route('admin.content.photos.destroy', ['id' => $this->id]);
    }

    public function getThumbAttribute()
    {
        return $this->getThumb(360, 310);
    }

    public function getThumb($width = null, $height = null, $params = [])
    {

        $width = intval($width) == 0 ? null : intval($width);
        $height = intval($height) == 0 ? null : intval($height);

        $image = $this->makeThumbnail($width, $height, $params);

        return $image;
    }

    public function setAttribsAttribute($value)
    {
        $this->attributes['attribs'] = json_encode($value);
    }

    public function getAttribsAttribute()
    {
        return json_decode(json_decode($this->attributes['attribs']));
    }

    /**
     *
     * (new static) === $photo = new static;
     * @param string $filename
     * @return mixed
     */
    public static function named($filename)
    {
        return (new static)->saveAs($filename);
    }

    protected function makePath()
    {
        if (!app()->environment('testing')) {
            return $this->baseDir . '/' . substr($this->name, 0, 2);
        }

        return 'public/' . $this->baseDir . '/' . substr($this->name, 0, 2);
    }

    protected function saveAs(UploadedFile $file)
    {
        $this->name = md5_file($file) . '.' . $file->getClientOriginalExtension();
        $this->path = $this->makePath();
        $this->original_name = $file->getClientOriginalName();

        return $this;
    }

    public function move(UploadedFile $file)
    {
        $file->move($this->makePath(), $this->name);

        return $this;
    }

    protected function clearThumbnail()
    {
        $thumbsFolder = "{$this->makePath()}/thumbs/";

        if (is_dir($thumbsFolder)) {
            File::deleteDirectory($thumbsFolder);
        }
    }

    protected function makeThumbnail($width = null, $height = null, $params = [])
    {
        $thumbsFolder = "{$this->makePath()}/thumbs/";

//        $this->clearThumbnail();

        if (isset($params['reset']) && $params['reset']) {
            $this->clearThumbnail();
        }

        if (!is_dir($thumbsFolder)) {
            try {
                File::makeDirectory($thumbsFolder);
            } catch (\Exception $e) {
                request()->session()->flash('flash-errors', "Файл {$this->original_name} не найден.");

                return null;
            }
        }

        if (isset($params['blur']) && $params['blur']) {
            $path = "{$thumbsFolder}{$width}_{$height}_blur_{$this->name}";
        } else {
            $path = "{$thumbsFolder}{$width}_{$height}_{$this->name}";
        }

        if (!file_exists($path)) {
            try {
                $this->image($path, $params, $width, $height);
            } catch (InvalidArgumentException $e) {
                request()->session()->flash('flash-errors', $e->getMessage());
                return "{$this->makePath()}/{$this->name}";
            } catch (NotReadableException $e) {

                return null;
            }
        }

        return asset($path);
    }

    protected function image($path, array $params = [], $width = null, $height = null)
    {
        $image = \Image::make("{$this->makePath()}/{$this->name}");

        if (isset($params['scale']) && $params['scale']) {
            $image->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        } else {
            $image->fit($width, $height, function ($constraint) {
                $constraint->upsize();
            });
        }

        if (isset($params['blur']) && $params['blur']) {
            $image->blur($params['blur']);
        }

        return $image->save($path);
    }

}
