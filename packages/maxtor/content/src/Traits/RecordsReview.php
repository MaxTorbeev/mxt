<?php

namespace MaxTor\Content\Traits;

use Illuminate\Support\Facades\DB;
use MaxTor\Content\Models\Photo;
use MaxTor\Content\Models\Review;
use MaxTor\Content\Models\Tag;

trait RecordsReview
{
    public function reviews()
    {
        return $this->morphMany(Review::class, 'subject');
    }

    public function addReview(Review $review)
    {
        $review->type = $this->getReviewType();
        $review->user_ip = request()->ip();

        return $this->reviews()->save($review);
    }

    public function getReviewType(): string
    {
        return strtolower((new \ReflectionClass($this))->getShortName());
    }

    protected static function getReviewsToRecord()
    {
        return ['created', 'updated'];
    }
}