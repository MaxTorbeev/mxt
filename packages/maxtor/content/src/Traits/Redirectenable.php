<?php

namespace MaxTor\Content\Traits;

use MaxTor\Content\Models\Redirect;

trait Redirectenable
{
    protected static function bootRedirectenable()
    {
        if (request()->has('redirect_from')) {
            foreach (static::getRedirectSectionsToRecord() as $event) {
                static::$event(function ($model) use ($event) {
                    $model->redirect()->delete();
                    $redirect = Redirect::firstOrCreate([
                        'redirect_from' => ltrim(trim(request('redirect_from'), '/'), '/')
                    ]);
                    $model->addRedirect($redirect);
                });
            }
        }

        foreach (['deleting'] as $event) {
            static::$event(function ($model) use ($event) {
                $model->redirect()->delete();
            });
        }
    }

    protected static function getRedirectSectionsToRecord()
    {
        return ['created', 'updated', 'saved'];
    }

    public function redirectenable()
    {
        return $this->morphTo();
    }

    public function redirect()
    {
        return $this->morphOne(Redirect::class, 'subject');
    }

    public function addRedirect(Redirect $redirect)
    {
        return $this->redirect()->save($redirect);
    }
}