<?php

namespace MaxTor\Content\Mail;

use http\Env\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use MaxTor\Content\Models\Feedback;
use MaxTor\Content\Requests\BackCallRequest;

class BackCall extends Mailable
{
    use Queueable, SerializesModels;

    public $feedback;

    public function __construct(Feedback $feedback)
    {
        $this->feedback = $feedback;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('me@maxtor.name')
            ->subject($this->feedback->mailSubject)
            ->markdown($this->feedback->mailTemplate)
            ->with([
                'feedback' => $this->feedback,
            ]);
    }
}
