<?php

namespace MaxTor\Content\Layouts\Admin;

use App\User;
use MaxTor\Content\Models\Category;
use MaxTor\Content\Models\Post;
use MaxTor\MXTCore\Layouts\AbstractTable;
use MaxTor\MXTCore\Fields\TD;

class PostsListLayout extends AbstractTable
{

    /**
     * @var string
     */
    public $data = '';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('id')->title('Код'),
            TD::name('name')->title('Название')
                ->width(550)
                ->setRender(function ($model) {
                    return $this->editAnchor($model);
                }),
            TD::name('category_id')->title('Категория')
                ->setRender(function ($model) {
                    return $this->editCategoryAnchor($model['category_id']);
                }),
            TD::name('hits')->title('Просмотры'),

            TD::name('author')->title('Автор')
                ->setRender(function ($model) {
                    return '<a href="' . route('admin.users.edit', ['id' => $model['created_user_id']]) . '">'
                        . User::whereId($model['created_user_id'])->first()->name .
                        '</a>';
                }),
            TD::name('enabled')->title('Вкл.')
                ->setRender(function ($model) {
                    return $model['enabled'] ? 'Да' : 'Нет';
                }),
            TD::name('updated_at')->title('Обновлено'),
            TD::name('created_at')->title('Создано'),
            TD::name('delete')->title('')
                ->setRender(function ($model) {
                    return $this->deleteButtonHtml(route('admin.content.posts.destroy', ['id' => $model['id']]));
                }),
        ];
    }

    private function editAnchor($model)
    {
        return '<a href="' . route('admin.content.posts.edit', ['id' => $model['id']]) . '">' . $model['name'] . '</a>';
    }

    private function editCategoryAnchor($categoryId)
    {
        if(Category::whereId($categoryId)->exists()){
            return '<a href="' . route('admin.content.categories.edit', ['id' => $categoryId]) . '">'. Category::whereId($categoryId)->first()->name .'</a>';
        }

        return 'Нет категории';
    }

    private function featuredIcon($model)
    {
        if ($model['featured'] > 0) {
            return '<i class="fa fa-star" data-toggle="tooltip" data-placement="top" title=" ' . Post::$featuredTypes[$model['featured']] . '"></i>';
        }
    }
}