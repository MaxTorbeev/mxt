<?php

namespace MaxTor\Content\Layouts\Admin;

use App\User;
use MaxTor\Content\Models\Category;
use MaxTor\Content\Models\Carousel;
use MaxTor\MXTCore\Layouts\AbstractTable;
use MaxTor\MXTCore\Fields\TD;

class SlidersListLayout extends AbstractTable
{

    /**
     * @var string
     */
    public $data = '';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('id')->title('Код'),

            TD::name('name')->title('Название')
                ->setRender(function ($model) {
                    return $this->editAnchor($model['id']);
                }),

            TD::name('published')->title('')
                ->setRender(function ($model) {
                    return $this->publishedBadgeHtml($model);
                }),


            TD::name('author')->title('Автор')
                ->setRender(function ($model) {
                    return '<a href="' . route('admin.users.edit', ['id' => $model['created_user_id']]) . '">'
                        . User::whereId($model['created_user_id'])->first()->name .
                        '</a>';
                }),

            TD::name('updated_at')->title('Обновлено'),
            TD::name('created_at')->title('Создано'),

            TD::name('delete')->title('')
                ->setRender(function ($model) {
                    return $this->deleteButtonHtml( route('admin.content.carousels.destroy', ['id' => $model['id']]) );
                }),
        ];
    }

    private function editAnchor($id)
    {
        return '<a href="' . route('admin.content.carousels.edit', ['id' => $id]) . '">'
                    . Carousel::whereId($id)->first()->name .
                '</a>';
    }
}