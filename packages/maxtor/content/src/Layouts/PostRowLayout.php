<?php

namespace App\Layouts;

use MaxTor\MXTCore\Exceptions\TypeException;
use MaxTor\MXTCore\Layouts\AbstractRows;
use MaxTor\MXTCore\Fields\TD;

class PostRowLayout extends AbstractRows
{
    public function fields(): array
    {
        return [];
    }
}