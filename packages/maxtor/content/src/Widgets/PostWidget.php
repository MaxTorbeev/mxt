<?php

namespace MaxTor\Content\Widgets;

use MaxTor\Content\Models\Category;
use MaxTor\MXTCore\Widgets\Widget;

class PostWidget extends Widget
{
    public $packageName = 'content';

    public function handle($attributes = null)
    {
        parent::handle($attributes);

        return $this->view($this->view, [
            'attributes' => $this->attributes,
            'model' => $this->model
        ]);
    }
}
