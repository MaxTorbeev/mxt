<?php

namespace MaxTor\Content\Widgets;

use MaxTor\Content\Models\Category;
use MaxTor\MXTCore\Widgets\Widget;

class YoutubeWidget extends Widget
{
    public $packageName = 'content';


    public function handle($attributes = null)
    {
        $this->attributes = $attributes;

        return $this->view($this->view, [
            'attributes' => $this->attributes
        ]);
    }
}
