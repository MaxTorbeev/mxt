<?php

namespace MaxTor\Content\Widgets;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use MaxTor\Content\Models\Category;
use MaxTor\MXTCore\Widgets\Widget;
use MaxTor\Trade\Models\GoodsCategory;

class FeedbackFormWidget extends Widget
{
    public $packageName = 'content';

    public function handle($attributes = null)
    {
        parent::handle($attributes);

        return $this->view($this->view, ['attributes' => $attributes,]);
    }
}
