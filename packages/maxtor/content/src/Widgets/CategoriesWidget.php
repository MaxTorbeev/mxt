<?php

namespace MaxTor\Content\Widgets;

use Illuminate\Support\Facades\Cache;
use MaxTor\Content\Models\Category;
use MaxTor\MXTCore\Widgets\Widget;

class CategoriesWidget extends Widget
{
    public $packageName = 'content';
    public $view = 'horizontal';

    /**
     * @return mixed
     */
    public function handle($attributes = null)
    {
        $this->attributes = $attributes;

        return $this->view($this->view, ['categories' => $this->getCategories()]);
    }

    protected function getView()
    {
        $this->cacheKey = $this->attributes['cacheKey'] ?? $this->getDefaultCacheKey();
        $this->view = $this->attributes['view'] ?? $this->view;

        if ($this->cacheKey !== null) {
            return Cache::remember($this->cacheKey, $this->cacheTime, function () {
                return $this->getCategories();
            });
        } else {
            throw new \InvalidArgumentException('Ключ кеша не указан');
        }
    }

    protected function getCategories()
    {
        return Category::published()->whereNull('parent_id')->get();
    }
}