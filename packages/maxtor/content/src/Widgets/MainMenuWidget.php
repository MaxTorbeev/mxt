<?php

namespace MaxTor\Content\Widgets;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use MaxTor\Content\Models\Category;
use MaxTor\MXTCore\Widgets\Widget;
use MaxTor\Trade\Models\GoodsCategory;

class MainMenuWidget extends Widget
{
    public $packageName = 'content';

    public function handle($attributes = null)
    {
        parent::handle($attributes);

        $contentCategories =  Cache::rememberForever($this->getDefaultCacheKey() . ':content_categories', function () {
            return Category::published()
                ->select('name', 'id', 'slug', '_lft')
                ->where('parent_id', null)
                ->orderBy('_lft')
                ->get();
        });

        $goodsCategories =  Cache::rememberForever($this->getDefaultCacheKey() . ':goods_categories', function () {
            return GoodsCategory::published()
                ->select('name', 'id', 'slug', '_lft')
                ->where('parent_id', null)
                ->orderBy('_lft')
                ->get();
        });

        return $this->view($this->view, [
            'attributes' => $this->attributes,
            'services' => $this->prepareArray($goodsCategories),
            'content' => $this->prepareArray($contentCategories),
        ]);
    }

    protected function prepareArray(Collection $collection): array
    {
        $array = [];

        foreach ($collection as $item){
            $array[] = [
                'name' => $item->name,
                'url' => $item->frontend_url
            ];
        }

        return $array;
    }
}
