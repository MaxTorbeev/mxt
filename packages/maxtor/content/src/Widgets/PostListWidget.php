<?php

namespace MaxTor\Content\Widgets;

use MaxTor\Content\Models\Category;
use MaxTor\MXTCore\Widgets\Widget;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class PostListWidget extends Widget
{
    public $packageName = 'content';

    protected $category;

    public function handle($attributes = null)
    {
        parent::handle($attributes);

        return $this->view($this->view, [
            'attributes' => $this->attributes,
            'posts' => $this->attributes['posts'] ?? $this->getPosts(),
            'category' => $this->category
        ]);
    }

    protected function getPosts()
    {
        $posts = null;
        $this->cacheKey = $this->attributes['cacheKey'] ?? $this->getDefaultCacheKey() . '::' . $this->attributes['category_slug'] ?? rand(10,20);

        if ($this->cacheKey !== null) {
            $posts = \Cache::remember($this->cacheKey, $this->cacheTime, function () {
                if (isset($this->attributes['category_slug']) && $this->attributes['category_slug']) {
                    $this->category = Category::where('slug', $this->attributes['category_slug'])->latest()->published()->first();
                    return $this->category
                        ->posts()
                        ->limit($this->attributes['limit'] ?? 10)
                        ->get();
                }
            });
        } else {
            throw new \InvalidArgumentException('Cache key is not specified');
        }

        return $posts;
    }
}
