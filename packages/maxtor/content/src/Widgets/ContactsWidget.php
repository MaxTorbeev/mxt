<?php

namespace MaxTor\Content\Widgets;

use MaxTor\MXTCore\Widgets\Widget;

class ContactsWidget extends Widget
{
    public $packageName = 'content';

    public function handle($attributes = null)
    {
        parent::handle($attributes);

        return $this->view($this->view, [
            'attributes' => $this->attributes
        ]);
    }
}