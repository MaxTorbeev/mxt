<?php
/**
 * Configure file for MXTCore
 */
return [
    'baseUrl'       => 'e-s-h.ru',
    'title'         => '',
    'keywords'      => '',
    'description'   => '',
    'generator'     => 'MXTCore',
    'common'        => [
        'defaults'  => [
            'avatar' => '/images/templates/personal/no-photo.svg',
            'photo' => '/images/templates/esh/no-photo.svg'
        ]
    ],
    'dashboard'     => [
        'logo'      => [
                'minimized' => '/images/templates/dashboard/logo/logo_small.svg',
                'full' => '/images/templates/dashboard/logo/logo.svg',
        ]
    ],
    'frontend'     => [
        'logo'      => [
            'full' => '/images/templates/esh/logo/full.svg?0507',
        ],
        'socials'  => [
            'vk' => '//vk.com/',
            'instagram' => '//www.instagram.com/',
        ]
    ],
    'paginate'     => 18,
    'watermark'    => null,
];
