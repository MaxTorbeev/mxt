<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use MaxTor\Content\Models\Category;
use MaxTor\Trade\Models\GoodsCategory;

class PagesController extends Controller
{
    public function home()
    {
        $roofCategory = GoodsCategory::whereSlug('roof')->first();
        $woodenHousesBuilding = GoodsCategory::whereSlug('wooden-house-building')->first();
        $rentalSpecialEquipment = GoodsCategory::whereSlug('rental-special-equipment')->first();

        return view('pages.home', compact('roofCategory', 'woodenHousesBuilding', 'rentalSpecialEquipment'));
    }
}
