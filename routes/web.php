<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'namespace' => 'Web',
], function () {
    Route::get('/', [
        'as' => 'home',
        'uses' => 'PagesController@home'
    ]);
});

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Главная', route('home'));
});

Auth::routes();
