<section class="section section_roof-services">
    <h2 class="h1 text-center my-4">Услуги</h2>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-9">
                <div class="card-group">
                    <article class="card card-aboutService card-withShadow card-hovered mx-md-3">
                        <div class="card-body pb-5">
                            <h3 class="h4 text-center text-bold">Проектирование и расчет</h3>
                            <p class="card-text text-center text-line-height-3">
                                Оказываем услуги по проектированию и расчету проектов,
                                услуга включает в себя бесплатный выезд специалиста на замер!
                            </p>
                        </div>
                    </article>
                    <article class="card card-aboutService card-withShadow card-hovered mx-md-3">
                        <div class="card-body pb-5">
                            <h3 class="h4 text-center text-bold">Производство фальцевой кровли</h3>
                            <p class="card-text text-center text-line-height-3">
                                Производство фальцевой кровли и доборных элементов на заказ. Комплектуем объект под ключ
                                по самым низким ценам!
                            </p>
                        </div>
                    </article>
                    <article class="card card-aboutService card-withShadow card-hovered mx-md-3">
                        <div class="card-body pb-5">
                            <h3 class="h4 text-center text-bold">Монтаж кровли</h3>
                            <p class="card-text text-center text-line-height-3">
                                Производим монтаж фальцевой кровли любой сложности и архитектурной формы, по собственным проектам и готовым проектам заказчика
                            </p>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</section>