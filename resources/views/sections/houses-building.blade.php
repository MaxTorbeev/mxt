@inject('goodsListWidget', 'MaxTor\Trade\Widgets\GoodsListWidget')

<div class="section_divider mt-4 mb-2">
    <span class="section_divider_title">Услуги</span>
</div>

<section class="section section_houses-building">
    <div class="row">
        <div class="col-md-9">
            <h2 class="h2 section_name">
                {{ $model->page_title }}
            </h2>
            <div class="section_description mb-3">
                Деревянные дома, представленные <a href="#">в нашем каталоге</a>, изготавливаются на собственном
                производстве с исопользованием современного оборудования, высококвалифицированным персоналом, по
                технологиям двойного и мин и бруса.
            </div>

            {!! $goodsListWidget->handle(['view' => 'rows', 'card-view' => 'row', 'category_id' => $model->id]) !!}

            <a href="{{ $model->frontend_url }}" class="btn btn-block btn-danger">
                <i class="fas fa-inbox mx-3"></i> Посмотреть весь каталог
            </a>
        </div>
        <div class="col-md-3">
            <section class="section">
                <h4 class="h4 text-center mb-4">Так же оказываем услуги</h4>

                <div class="card bg-light card-sticky card-sticky-top-0 mb-4">
                    <div class="card-body">
                        <div class="card-title card-title-size-3 text-center">
                            Изготовление стенокомплектов
                        </div>
                        <div class="card-text">
                            Изготвливаем стенокомплекты на современном оборудовании, из которых вы сами сможете
                            собрать свой собственный дом
                        </div>
                    </div>
                </div>
                <div class="card bg-light card-sticky card-sticky-top-0 mb-4">
                    <div class="card-body">
                        <div class="card-title card-title-size-3 text-center">
                            Мотаж стенокомплектов
                        </div>
                        <div class="card-text">
                            Мы поможем вам осуществить монтаж качественно и в срок.
                        </div>
                    </div>
                </div>
                <div class="card bg-light card-sticky card-sticky-top-0 mb-4">
                    <div class="card-body">
                        <div class="card-title card-title-size-3 text-center">
                            Монтаж кровли
                        </div>
                        <div class="card-text">
                            Так же, мы поможем вам осуществить монтаж файльцевой кровли, которая изготавливается нами, на зарубежном оборудовании
                        </div>
                    </div>
                </div>
                <div class="card bg-light card-sticky card-sticky-top-0 mb-4">
                    <div class="card-body">
                        <div class="card-title card-title-size-3 text-center">
                            Электрика и отопление
                        </div>
                        <div class="card-text">
                            Для комфортного проживание вам может потребоваться проектирование и монтаж систем отопления, а так же электрики.
                        </div>
                    </div>
                </div>

                {!!
                $feedbackFormWidget->handle([
                    'view' => 'consultation',
                    'action-url' => route('frontend.content.feedback.consultation')
                ])
                !!}
            </section>
        </div>
    </div>
</section>
