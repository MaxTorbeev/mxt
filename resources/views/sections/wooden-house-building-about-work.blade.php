<section class="section">
    <h2 class="h1 text-center my-4">Как мы работаем</h2>
    <div class="card-group">

        <article class="card card-aboutService card-withArrow card-hovered">
            <div class="card-body ">
                <div class="card-text text-center text-line-height-2">
                    <span class="step_number d-block text-size-6 text-bold">1</span>
                    <span class="step_title d-block text-uppercase text-muted mb-3">шаг</span>
                </div>
                <h3 class="h5 text-center ">1 день</h3>
                <p class="card-text text-center text-line-height-3">
                    Составляем техническое задание на проектирование и обсуждение проекта
                </p>
            </div>
        </article>

        <article class="card card-aboutService card-withArrow card-hovered">
            <div class="card-body ">
                <div class="card-text text-center text-line-height-2">
                    <span class="step_number d-block text-size-6 text-bold">2</span>
                    <span class="step_title d-block text-uppercase text-muted mb-3">шаг</span>
                </div>
                <h3 class="h5 text-center ">1 день</h3>
                <p class="card-text text-center text-line-height-3">
                    Подготовка предвариетельной стоимости строительства
                </p>
            </div>
        </article>

        <article class="card card-aboutService card-withArrow card-hovered">
            <div class="card-body ">
                <div class="card-text text-center text-line-height-2">
                    <span class="step_number d-block text-size-6 text-bold mb-3">3</span>
                    <span class="step_title d-block text-uppercase text-muted">шаг</span>
                </div>
                <h3 class="h5 text-center ">до 15 дней</h3>
                <p class="card-text text-center text-line-height-3">
                    Разработка проекта (рабочая документация + точная стоимость)
                </p>
            </div>
        </article>

        <article class="card card-aboutService card-withArrow card-hovered">
            <div class="card-body ">
                <div class="card-text text-center text-line-height-2">
                    <span class="step_number d-block text-size-6 text-bold mb-3">4</span>
                    <span class="step_title d-block text-uppercase text-muted">шаг</span>
                </div>
                <h3 class="h5 text-center ">до 3х недель</h3>
                <p class="card-text text-center text-line-height-3">
                    Изготовление домокомплекта
                </p>
            </div>
        </article>

        <article class="card card-aboutService card-withArrow card-hovered">
            <div class="card-body ">
                <div class="card-text text-center text-line-height-2">
                    <span class="step_number d-block text-size-6 text-bold mb-3">5</span>
                    <span class="step_title d-block text-uppercase text-muted">шаг</span>
                </div>
                <h3 class="h5 text-center ">до 3х недель</h3>
                <p class="card-text text-center text-line-height-3">
                    Монтаж домокомплекта
                </p>
            </div>
        </article>

        <article class="card card-aboutService card-withArrow card-hovered">
            <div class="card-body ">
                <div class="card-text text-center text-line-height-2">
                    <span class="step_number d-block text-size-6 text-bold mb-3">6</span>
                    <span class="step_title d-block text-uppercase text-muted">шаг</span>
                </div>
                <h3 class="h5 text-center ">1 день</h3>
                <p class="card-text text-center text-line-height-3">
                    Сдача дома заказчику
                </p>
            </div>
        </article>
    </div>
</section>