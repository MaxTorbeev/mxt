<section class="section">
    <h2 class="h1 text-center my-4">Как мы работаем</h2>
    <div class="container">
        <div class="row card-group">
            <article class="card card-aboutService card-withImageOverlay card-withShadow steps card-hovered mx-md-3">
                <img src="{{ asset('images/templates/sections/roofs/mail.jpg') }}" alt="" class="card-img-overlay">
                <div class="card-body pb-5">
                    <p class="card-text text-center text-line-height-2">
                        <span class="step_number d-block text-size-6 text-bold">1</span>
                        <span class="step_title d-block text-uppercase">шаг</span>
                    </p>
                </div>
                <div class="card-footer text-center py-3">
                    Вы оставляете заявку и мы связываемся с Вами
                </div>
            </article>
            <article class="card card-aboutService card-withImageOverlay card-withShadow steps card-hovered mx-md-3">
                <img src="{{ asset('images/templates/sections/roofs/builder.jpg') }}" alt="" class="card-img-overlay">
                <div class="card-body pb-5">
                    <p class="card-text text-center text-line-height-2">
                        <span class="step_number d-block text-size-6 text-bold">2</span>
                        <span class="step_title d-block text-uppercase">шаг</span>
                    </p>
                </div>
                <div class="card-footer text-center py-3">
                    Выезжаем на объект, производим замеры и делаем расчет стоимости на материалы
                </div>
            </article>
            <article class="card card-aboutService card-withImageOverlay card-withShadow steps card-hovered mx-md-3">
                <img src="{{ asset('images/templates/sections/roofs/builder2.jpg') }}" alt="" class="card-img-overlay">
                <div class="card-body pb-5">
                    <p class="card-text text-center text-line-height-2">
                        <span class="step_number d-block text-size-6 text-bold">3</span>
                        <span class="step_title d-block text-uppercase">шаг</span>
                    </p>
                </div>
                <div class="card-footer text-center py-3">
                    Изготавливаем продукцию на основе Ваших размеров
                </div>
            </article>
            <article class="card card-aboutService card-withImageOverlay card-withShadow steps card-hovered mx-md-3">
                <img src="{{ asset('images/templates/sections/roofs/delivery.jpg') }}" alt="" class="card-img-overlay">
                <div class="card-body pb-5">
                    <p class="card-text text-center text-line-height-2">
                        <span class="step_number d-block text-size-6 text-bold">4</span>
                        <span class="step_title d-block text-uppercase">шаг</span>
                    </p>
                </div>
                <div class="card-footer text-center py-3">
                    Изготавливаем продукцию на основе Ваших размеров
                </div>
            </article>
        </div>
    </div>
</section>