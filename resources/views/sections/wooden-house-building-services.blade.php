<section class="section">
    <h2 class="h1 text-center my-4">Наши услуги</h2>
    <div class="card-group">
        <article class="card card-aboutService bg-light card-hovered mx-md-3">
            <div class="card-body ">
                <h3 class="h4 text-center text-bold">Изготовление стенокомплектов</h3>
                <p class="card-text text-center text-line-height-3">
                    Изготвливаем стенокомплекты на современном оборудовании, из которых вы сами сможете
                    собрать свой собственный дом
                </p>
            </div>
        </article>
        <article class="card card-aboutService bg-light card-hovered mx-md-3">
            <div class="card-body ">
                <h3 class="h4 text-center text-bold">Мотаж стенокомплектов</h3>
                <p class="card-text text-center text-line-height-3">
                    Мы поможем вам осуществить монтаж качественно и в срок.
                </p>
            </div>
        </article>
        <article class="card card-aboutService bg-light card-hovered mx-md-3">
            <div class="card-body ">
                <h3 class="h4 text-center text-bold">Монтаж кровли</h3>
                <p class="card-text text-center text-line-height-3">
                    Так же, мы поможем вам осуществить монтаж файльцевой кровли, которая изготавливается нами,
                    на зарубежном оборудовании
                </p>
            </div>
        </article>
        <article class="card card-aboutService bg-light card-hovered mx-md-3">
            <div class="card-body ">
                <h3 class="h4 text-center text-bold">Электрика и отопление</h3>
                <p class="card-text text-center text-line-height-3">
                    Для комфортного проживание вам может потребоваться проектирование и монтаж систем отопления,
                    а так же электрики.
                </p>
            </div>
        </article>
    </div>
</section>