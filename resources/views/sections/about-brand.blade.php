<section class="container section section_about-brand">
    <div class="card bg-dark text-white" style="background-image: url({{ asset('images/templates/sections/abouts/image.jpg') }})">
        <div class="card-body py-0">
            <div class="row align-content-center">
                <div class="col-md-5 col-lg-4 col-xl-5 d-md-none d-lg-block align-self-center">
                    <div class="brand">
                        <span class="brand_image">
                            <img src="{{ config('mxtcore.frontend.logo.full') }}" alt="">
                        </span>
                        <span class="brand_name">Евростройхолдинг Казань</span>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 col-xl-5 align-self-center">
                    <p class="card-text brand_text">
                        Строительно-монтажные работы, выполняемые компанией, включают в себя полный
                        комплекс необходимых мероприятий.
                        Компания располагает квалифицированным штатом сотрудников от руководителей,
                        инженерно-технических работников до рабочих - профессионалов в своей отрасли.
                    </p>
                </div>
                <div class="col-md-2 d-md-none d-lg-block align-self-end">
                    <div class="brand_builder">
                        <picture>
                            <source media="(max-width: 1439px)" srcset="{{ asset('images/templates/sections/abouts/builder_120.png') }}">
                            <img src="{{ asset('images/templates/sections/abouts/builder_160.png') }}" alt="">
                        </picture>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
