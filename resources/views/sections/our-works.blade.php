@inject('postListWidget', 'MaxTor\Content\Widgets\PostListWidget')

<section class="section">
    <h2 class="h1 text-center my-4">Наши работы</h2>
    <div class="container">
        {!! $postListWidget->handle(['view' => 'grids', 'category_slug' => 'our-works']) !!}
    </div>
</section>