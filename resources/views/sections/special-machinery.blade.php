@inject('goodsListWidget', 'MaxTor\Trade\Widgets\GoodsListWidget')

<div class="section_divider mt-4 mb-2">
    <span class="section_divider_title">Услуги</span>
</div>

<section class="section section_special-machinery">
    <h2 class="section_name">{{ $model->page_title }}</h2>

    {!! $goodsListWidget->handle(['category_id' => $model->id, 'limit' => 8]) !!}
</section>
