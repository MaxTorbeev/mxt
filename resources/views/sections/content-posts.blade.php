@inject('postListWidget', 'MaxTor\Content\Widgets\PostListWidget')

<section class="container section section_content-posts mt-4">
    <h2 class="h2 section_name">
        Новости и статьи
    </h2>

    {!!
    $postListWidget->handle([
        'view' => 'cards',
        'category_slug' => 'articles',
    ])
    !!}
</section>
