<section class="section">
    <h2 class="h1 text-center my-4">Наши преимущества</h2>
    <div class="row">
        <div class="col-md-12 col-lg-4">
            <article class="card card-withSvg">
                <div class="row no-gutters">
                    <div class="col-3 col-md-3 col-lg-12">
                        <img src="{{ asset('images/templates/esh/icons/cog-repair.svg') }}" class="card-img-top pt-3"
                             alt="">
                    </div>
                    <div class="col-9 col-md-9 col-lg-12">
                        <div class="card-body text-lg-center">
                            <h4 class="card-title text-bold">Современное оборудование</h4>
                            <div class="card-text text-line-height-3">
                                Используем самое современное оборудование компаний New Tech Machinery Corp. (США),
                                DIMOS (Франция), Schlebach (Германия), SchechTL (Германия), единственное в Поволжье.
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>

        <div class="col-md-12 col-lg-4">
            <article class="card card-withSvg">
                <div class="row no-gutters">
                    <div class="col-3 col-md-3 col-lg-12">
                        <img src="{{ asset('images/templates/esh/icons/partners.svg') }}" class="card-img-top pt-3"
                             alt="">
                    </div>
                    <div class="col-9 col-md-9 col-lg-12">
                        <div class="card-body text-lg-center">
                            <h4 class="card-title text-bold">Лучшие поставщики</h4>
                            <div class="card-text text-line-height-3">
                                Наши поставщики - ПАО “Северсталь” (оцинкованная сталь, оцинкованая с полимерным
                                покрытием),
                                SSAB (финская сталь с различными типами покрытий), Aurubis Finland и KME Group S.p.A.
                                (медь).
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>

        <div class="col-md-12 col-lg-4">
            <article class="card card-withSvg">
                <div class="row no-gutters">
                    <div class="col-3 col-md-3 col-lg-12">
                        <img src="{{ asset('images/templates/esh/icons/partners-cog.svg') }}" class="card-img-top pt-3"
                             alt="">
                    </div>
                    <div class="col-9 col-md-9 col-lg-12">
                        <div class="card-body text-lg-center">
                            <h4 class="card-title text-bold">Большой опыт</h4>
                            <div class="card-text text-line-height-3">
                                Более 100 обустроенных объектов, среди них такие, как Казанский кремль, Казанский цирк,
                                представительство РТ в г. Москва, ДК Меньжинского, Храм Иконы Божьей Матери.
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>

        <div class="col-md-12 col-lg-4">
            <article class="card card-withSvg">
                <div class="row no-gutters">
                    <div class="col-3 col-md-3 col-lg-12">
                        <img src="{{ asset('images/templates/esh/icons/puzzle.svg') }}" class="card-img-top pt-3"
                             alt="">
                    </div>
                    <div class="col-9 col-md-9 col-lg-12">
                        <div class="card-body text-lg-center">
                            <h4 class="card-title text-bold">Большой выбор</h4>
                            <div class="card-text text-line-height-3">
                                Чтобы Вам было из чего выбирать мы предлагаем на Ваш вкус 24 цвета фальцевой кровли.
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>

        <div class="col-md-12 col-lg-4">
            <article class="card card-withSvg">
                <div class="row no-gutters">
                    <div class="col-3 col-md-3 col-lg-12">
                        <img src="{{ asset('images/templates/esh/icons/hands.svg') }}" class="card-img-top pt-3"
                             alt="">
                    </div>
                    <div class="col-9 col-md-9 col-lg-12">
                        <div class="card-body text-lg-center">
                            <h4 class="card-title text-bold">Индивиудальный подход</h4>
                            <div class="card-text text-line-height-3">
                                Готовы реализовывать проекты любой архитектурной формы и сложности, ограничением является только Ваша фантазия.
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>

        <div class="col-md-12 col-lg-4">
            <article class="card card-withSvg">
                <div class="row no-gutters">
                    <div class="col-3 col-md-3 col-lg-12">
                        <img src="{{ asset('images/templates/esh/icons/hands-money.svg') }}" class="card-img-top pt-3"
                             alt="">
                    </div>
                    <div class="col-9 col-md-9 col-lg-12">
                        <div class="card-body text-lg-center">
                            <h4 class="card-title text-bold">Выгода</h4>
                            <div class="card-text text-line-height-3">
                                Благодаря самым современным программам экономим Ваши деньги за счет минимизации
                                потерь кровли при раскрое.
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div>
</section>

