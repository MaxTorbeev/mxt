<section class="section section_feedback-red">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-2 icon text-right">
                <i class="far fa-comments"></i>
            </div>
            <div class="col-md-2 text-line-height-2">
                <div class="text-size-5">Нужна</div>
                <div class="text-size-3">консультация?</div>
            </div>
            <div class="col-md-5">
                Подробно расскажем о наших услугах, видах работ и типовых проектах,
                рассчитаем стоимость и подготовим индивидуальное предложение!
            </div>
            <div class="col-md-3">
                <a href="#" class="btn btn-outline-light btn-block">
                    Заказать консультацию
                </a>
            </div>
        </div>
    </div>
</section>
