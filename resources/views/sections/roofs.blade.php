<div class="section_divider mt-4 mb-2">
    <span class="section_divider_title">Услуги</span>
</div>

<section class="section section_roofs">
    <h2 class="h1 section_name mt-3">{{ $model->page_title }}</h2>

    <div class="row">
        <div class="col-md-9">
            <div class="section_divider mb-2">
                <span class="section_divider_title">{{ $model->name }}</span>
            </div>
            <div class="row mb-3">
                @foreach($model->goods as $goods)
                    <div class="col-md-6">
                        {!! $goodsCard->handle(['view' => 'roof-row', 'goods' => $goods]) !!}
                    </div>
                @endforeach
            </div>
            @foreach($model->children as $child)
                <div class="section_divider mb-2">
                    <span class="section_divider_title">{{ $child->name }}</span>
                </div>
                <div class="row mb-3">
                    @foreach($child->goods as $goods)
                        <div class="col-md-4">
                            {!! $goodsCard->handle(['view' => 'roof-card', 'goods' => $goods]) !!}
                        </div>
                    @endforeach
                    <div class="col-md-4">
                        <div class="card card-goods mb-3">
                            <ul class="roofs_rals_columns">
                                @foreach(config('trade.RALS') as $ral)
                                    <li class="roofs_rals_item  {{ 'roofs_rals-' . $ral }}">{{ $ral }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="col-md-3 card-sticky card-sticky-top-0 ">
            <div class="card bg-light  mb-4">
                <div class="card-body">
                    <div class="card-text">
                        <strong>Собственное производство фальцевой кровли</strong>, а так же производством
                        кляммеров.
                        В производстве используются станки NTM из Соедененных Штатов Америки,
                        высококвалифицированным персоналом
                    </div>
                    <a href="{{ $model->frontend_url }}" class="btn btn-success btn-block btn-large mt-3">Подробнее об услуге</a>
                </div>
            </div>

            {!!
            $feedbackFormWidget->handle([
                'view' => 'consultation',
                'action-url' => route('frontend.content.feedback.consultation')
            ])
            !!}

            @foreach($model->attachments as $attachment)
                <a href="{{ $attachment->url }}" class="btn btn-danger btn-block">{{ $attachment->original_name }}</a>
            @endforeach
        </div>
    </div>
</section>
