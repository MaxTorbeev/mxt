<section class="section">
    <h2 class="h1 text-center my-4">Наши партнеры</h2>
        <div class="section_divider mt-4 mb-2"><span class="section_divider_title">Кровля и водосточная система</span></div>
        <div class="row align-items-center">
            <div class="col-md-2">
                <img src="{{ asset('images/templates/sections/whb-partners/tegola.png') }}" class="img-fluid" alt="">
            </div>
            <div class="col-md-2">
                <img src="{{ asset('images/templates/sections/whb-partners/katepal.png') }}" class="img-fluid" alt="">
            </div>
            <div class="col-md-2">
                <img src="{{ asset('images/templates/sections/whb-partners/roofshield.png') }}" class="img-fluid" alt="">
            </div>
            <div class="col-md-2">
                <img src="{{ asset('images/templates/sections/whb-partners/tilcor.png') }}" class="img-fluid" alt="">
            </div>
            <div class="col-auto">
                <img src="{{ asset('images/templates/sections/whb-partners/aquasystems.png') }}" class="img-fluid" alt="">
            </div>
        </div>
    <div class="row">
        <div class="col-md-4">
            <div class="section_divider mt-4 mb-2">
                <span class="section_divider_title">Кровля и водосточная система</span>
            </div>

            <div class="row align-items-center">
                <div class="col-md-6">
                    <img src="{{ asset('images/templates/sections/whb-partners/fakro.png') }}" class="img-fluid" alt="">
                </div>
                <div class="col-md-6">
                    <img src="{{ asset('images/templates/sections/whb-partners/valux.png') }}" class="img-fluid" alt="">
                </div>
            </div>
        </div>

        <div class="col-md-auto">
            <div class="section_divider mt-4 mb-2">
                <span class="section_divider_title">Фасадные панели</span>
            </div>

            <div class="row align-items-center">
                <div class="col-md-auto">
                    <img src="{{ asset('images/templates/sections/whb-partners/kmew.png') }}" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </div>
</section>