<section class="section">
    <h2 class="h1 text-center my-4">Наши преимущества</h2>
    <div class="row">
        <div class="col-md-12 col-lg-4">
            <article class="card card-withSvg">
                <div class="row no-gutters">
                    <div class="col-3 col-md-3 col-lg-12">
                        <img src="{{ asset('images/templates/esh/icons/factory.svg') }}" class="card-img-top pt-3" alt="">
                    </div>
                    <div class="col-9 col-md-9 col-lg-12">
                        <div class="card-body text-lg-center">
                            <h4 class="card-title text-bold">Соверенное оборудование</h4>
                            <div class="card-text text-line-height-3">
                                В производстве мы используем высококачественное оборудование
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        <div class="col-md-12 col-lg-4">
            <article class="card card-withSvg">
                <div class="row no-gutters">
                    <div class="col-3 col-md-3 col-lg-12">
                        <img src="{{ asset('images/templates/esh/icons/hands-money.svg') }}" class="card-img-top pt-3" alt="">
                    </div>
                    <div class="col-9 col-md-9 col-lg-12">
                        <div class="card-body text-lg-center">
                            <h4 class="card-title text-bold">Поэтапная оплата</h4>
                            <div class="card-text text-line-height-3">
                                При составлении стоимости проекта, мы вам предлагаем оплату поэтапно
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        <div class="col-md-12 col-lg-4">
            <article class="card card-withSvg">
                <div class="row no-gutters">
                    <div class="col-3 col-md-3 col-lg-12">
                        <img src="{{ asset('images/templates/esh/icons/cog-time.svg') }}" class="card-img-top pt-3" alt="">
                    </div>
                    <div class="col-9 col-md-9 col-lg-12">
                        <div class="card-body text-lg-center">
                            <h4 class="card-title text-bold">Соблюдение сроков</h4>
                            <div class="card-text text-line-height-3">
                                Гарантируем соблюдения сроков, установленные в договоре
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div>
</section>