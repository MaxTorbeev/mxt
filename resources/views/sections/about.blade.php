<div class="section_divider mt-4 mb-2">
    <span class="section_divider_title">О нас</span>
</div>

<section class="section section_about">
    <h2 class="h1 section_name my-3">Евростройхолдинг Казань</h2>
    <div class="row">
        <div class="col-md-3">
            <img src="{{ asset('images/templates/sections/abouts/building_image.jpg') }}" alt="" class="img-fluid">
        </div>
        <div class="col-md-9">
            <div class="section_about_text">
                Наш многолетний опыт работы позволяет нам нам строить и сдавать в эксплуатацию объекты любой
                сложности
                в соответствии ссовременными технологическими разработками и требованиями законодательства. В
                качестве
                генерального подрядчика мы проводим также реконструкцию промышленных и коммерческих зданий и
                сооружений,
                реставрацию объектов культурного наследия федерального и регионального значения.
            </div>
            <div class="section_about_buttons mt-3">
                <a href="#" class="btn btn-outline-success">Контакты</a>
                <a href="#" class="btn btn-success">Подробнее</a>
            </div>
        </div>
    </div>
</section>
