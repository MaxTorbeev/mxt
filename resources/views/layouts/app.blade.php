@inject('mainMenuWidget', 'MaxTor\Content\Widgets\MainMenuWidget')


<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-100">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('layouts.partials.favicon')

    @if(!View::hasSection('head-meta'))
        <title>{{ site_config('site_name')->value }}</title>
        <meta name="keywords" content="{{ site_config('metakey')->value }}">
        <meta name="description" content="{{ site_config('metadesc')->value }}">
    @endif

    @yield('head-meta')

    <style>
        #page{
            transition: 0.3s;
            visibility: hidden;
            opacity: 0;
        }
        #page.hasBeenLoad{
            visibility: visible;
            opacity: 1;
        }
    </style>

    <script>
        window.mxtcore = @json(['csrfToken' => csrf_token()])
    </script>
</head>
<body class="app h-100 d-flex flex-column">

<div id="page" class="page h-100 d-flex flex-column">
    @if(agent()->isMobile())
        @include('layouts.partials.header.mobile_header')
    @else
        @include('layouts.partials.header.header')
    @endif

    <div id="app" class="loaded" style="flex: 1 0 auto;">
        @yield('top')
        <div class="container">
            @yield('breadcrumbs')
            @yield('content_top')
            <div class="row">
                <div class="col-md-12 {{ (View::hasSection('aside_right') && $__env->getSections()['aside_right']) ? 'col-lg-8 col-xl-9' : '12'}}">
                    @yield('content')
                </div>
                @hasSection('aside_right')
                    <div class="col-md-12 col-lg-4 col-xl-3">
                        <div class="card-sticky card-sticky-top-0">
                            @yield('aside_right')
                        </div>
                    </div>
                @endif
            </div>
            @yield('content_bottom')
        </div>

        @yield('bottom')

        <modal-dialog></modal-dialog>
    </div>

    @include('layouts.partials.footer')
</div>

@if(agent()->isMobile())
    {!! $mainMenuWidget->handle(['view' => 'mobile', 'mobile' => true]) !!}
@endif

<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<script src="{{ mix('js/app.js') }}"></script>

@if(app()->environment('production'))
    @include('layouts.partials.metrics')
@endif
</body>

<!-- {{ (microtime(true) - LARAVEL_START) }} -->
