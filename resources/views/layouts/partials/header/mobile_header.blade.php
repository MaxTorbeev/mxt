<header class="header header-dark mb-4">
    <div class="row no-gutters justify-content-between">
        <div class="col-auto align-self-center">
            <div class="header_brand">
                <a href="{{ route('home') }}" class="header_brand">
                    <img src="{{ config('mxtcore.frontend.logo.full') }}" class="img-fluid header_brand_img" alt="">
                </a>
            </div>
        </div>

        <div class="col-auto align-self-center">
            <a href="#" class="header_phone"
               onclick='event.preventDefault(); modalShow("back-call",  @json(["apiUrl" => route("frontend.content.feedback.backcall")]))'
            >
                <i class="fas fa-phone-alt"></i>
            </a>

            <a href="#main-menu" class="header_menuToggle mburger">
                <b></b>
                <b></b>
                <b></b>
            </a>
        </div>
    </div>
</header>
