@inject('mainMenuWidget', 'MaxTor\Content\Widgets\MainMenuWidget')

<header class="header header-dark mb-4">
    <div class="container">

        @if(View::hasSection('header_top_phone') )
            <div class="row text-center text-sm-left pt-3 align-items-center justify-content-between header_top">
                <div class="col-md-3">
                    @yield('header_top_description')
                </div>
                <div class="col-md-6 col-lg-7 text-right">
                    @yield('header_top_phone')
                </div>
                <div class="col-md-3 col-lg-2">
                    <a href="#" class="btn btn-success btn-block"
                       onclick='event.preventDefault(); modalShow("back-call",  @json(["apiUrl" => route("frontend.content.feedback.backcall")]))'
                    >
                        Заказать обратный звонок
                    </a>
                </div>
            </div>
        @endif

        <div class="row text-center text-sm-left align-items-center py-2">
            <div class="col-md-3">
                <a href="{{ route('home') }}" class="header_brand">
                    <img src="{{ config('mxtcore.frontend.logo.full') }}" style="height: 70px" class="img-fluid py-1" alt="">
                </a>
            </div>
            <div class="col-md-8">
                <nav class="header_navbar">
                    {!! $mainMenuWidget->handle(['mobile' => false]) !!}
                </nav>
            </div>
            <div class="col-auto">
                <ul class="socialMenu socialMenu-inHeader">
                    @foreach(config('mxtcore.frontend.socials') as $name => $url)
                        <li>
                            <a href="{{ url($url) }}"
                               class="socialMenu_item socialMenu_item-{{ $name }}"
                               target="_blank"
                               rel="nofollow">
                                <i class="fab fa-{{$name}}"></i>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</header>
