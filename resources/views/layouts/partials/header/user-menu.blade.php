<ul class="nav navbar-nav ml-auto">
    @can('access_dashboard')
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.menu.index') }}">
                <i class="nav-icon icon-speedometer"></i>
            </a>
        </li>
    @endcan

    <li class="nav-item dropdown">
        <div class="dropdown-menu dropdown-menu-right">
            @isset(auth()->user()->contact)
                <a class="dropdown-item" href="{{ auth()->user()->contact->frontend_url }}">
                    <i class="fa fa-user"></i> Моя станица
                </a>
            @endisset

            <div class="divider"></div>
            <a class="dropdown-item" href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fa fa-lock"></i> Выход
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
            </a>
        </div>
    </li>
</ul>