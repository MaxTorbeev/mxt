@if(!request()->is('admin/*'))
    <ol class="breadcrumb">
        @if (count($breadcrumbs))
            <div class="swiper-container breadcrumb-swiper"
                 data-slides
                 data-direction="horizontal"
                 data-free-mode="true"
                 data-slides-per-view="auto"
                 data-space-between="0"
                 data-nav-next-el="null"
                 data-nav-nrev-el="null"
            >
                <div class="swiper-wrapper">
                    @foreach ($breadcrumbs as $breadcrumb)
                        @if ($breadcrumb->url && !$loop->last)
                            <li class="breadcrumb-item swiper-slide">
                                <a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a>
                            </li>
                        @else
                            <li class="breadcrumb-item active swiper-slide">
                                {{ $breadcrumb->title }}
                            </li>
                        @endif
                    @endforeach
                </div>
            </div>
        @endif
    </ol>
@else
    <ol class="breadcrumb">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$loop->last)
                <li class="breadcrumb-item">
                    <a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a>
                </li>
            @else
                <li class="breadcrumb-item active">
                    {{ $breadcrumb->title }}
                </li>
            @endif
        @endforeach

        <li class="breadcrumb-menu d-md-down-none">
            <div class="btn-group" role="group" aria-label="Button group">
                <a class="btn" href="#" onclick="event.preventDefault(); mxtcore.cacheClear();"><i class="icon-refresh"></i> &nbsp;Очистить кэш</a>
                <a class="btn" href="{{ route('admin.site-configs.index') }}"><i class="icon-settings"></i> &nbsp;Настройки</a>
            </div>

        </li>
    </ol>
@endif
