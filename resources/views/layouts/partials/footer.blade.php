<footer class="footer mt-auto" style="flex-shrink: none;">
    <div class="container">
        <div class="footer_content pt-4 pb-2">
            <div class="row">
                <address class="col-md-6">
                    г. Казань, ул. Петра Виттера <br>
                    <a href="{{ route('frontend.content.category', ['categories' => 'contacts']) }}">Схема проезда</a>
                    <br>
                    +7 (843)  204 16 53 | +7 (843) 204 16 53 <br>
                    Заказать звонок <br>
                </address>
                <div class="col-md-6">
                    <ul class="footer_menu" style="height: 100px">
                        <li><a href="{{ route('home') }}">Главная</a></li>
                        <li><a href="{{ route('frontend.content.category', ['categories' => 'about']) }}">О нас</a></li>
                        <li><a href="{{ route('frontend.trade.index') }}">Услуги</a></li>
                        <li><a href="{{ route('frontend.content.category', ['categories' => 'our-works']) }}">Наши работы</a></li>
                        <li><a href="{{ route('frontend.content.category', ['categories' => 'articles']) }}">Статьи</a></li>
                        <li><a href="{{ route('frontend.content.category', ['categories' => 'contacts']) }}">Контакты</a></li>
                    </ul>
                </div>
            </div>

            <div class="row footer-textSmall">
                <div class="col-md-8"></div>
            </div>
        </div>
    </div>

    <div class="footer_content footer_content-darken footer-textSmall py-2">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-9">
                    <a href="{{ route('home') }}">© 2009-{{ date('Y') }}, «Евростройхолдинг Казань».</a>
                    <a href="#">Пользовательское соглашение</a> |
                    <a href="#">Условия обработки и хранения персональных данных. </a> <br>
                    Информация, представленная на сайте не является договором публичной оферты.
                </div>
                <div class="col-md-3 text-right">
                    <a href="{{route('frontend.content.sitemap')}}" class="ml-4"> Карта сайта</a>
                    @can('access_dashboard')
                        <a href="{{route('dashboard')}}" class="ml-4">Для администраторов</a>
                    @endcan
                    @auth
                        <a href="{{ url('/logout') }}" class="ml-4"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-lock"></i>
                            Выход
                            <form id="logout-form"
                                  action="{{ url('/logout') }}"
                                  method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </a>
                    @else
                        <a class="ml-4" href="{{ route('login') }}">
                            <i class="icon-login"></i> Вход
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</footer>
