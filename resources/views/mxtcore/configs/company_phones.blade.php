@foreach($config->extra_attributes as $name => $attribute)
<div class="form-group form-row">
    <label class="col-sm-2 col-form-label">{{ $attribute['name'] }}</label>
    <input type="hidden" name="extra_attributes[{{ $name }}][name]" value="{{ $attribute['name'] }}">
    <div class="col-sm-6">
        <input type="text" value="{{ $attribute['label'] }}" name="extra_attributes[{{ $name }}][label]" class="form-control" placeholder="Заголовок">
    </div>
    <div class="col-sm-4">
        <input type="text" value="{{ $attribute['value'] }}"  name="extra_attributes[{{ $name }}][value]"  class="form-control" placeholder="Телефон">
    </div>
</div>
@endforeach
