@extends('trade::frontend.catalog.categories.default')

@inject('postListWidget', 'MaxTor\Content\Widgets\PostListWidget')
@inject('feedbackFormWidget', 'MaxTor\Content\Widgets\FeedbackFormWidget')
@inject('goodsCard', 'MaxTor\Trade\Widgets\GoodsCardWidget')
@inject('goodsCategoriesWidget', 'MaxTor\Trade\Widgets\GoodsCategoriesWidget')
@inject('goodsCategoryCarouselWidget', 'MaxTor\Trade\Widgets\GoodsCategoryCarouselWidget')

@section('header_top_phone')
    @include('helpers.phone', ['phone' => site_config_extra('company_phones.roof.value')])
@endsection

@section('title')
    {{ $category->name }}
@endsection

@section('breadcrumbs') {{ null  }}@endsection

@section('content')

    {{ Breadcrumbs::render('home.trade.catalog.categories', $category) }}

    {!!
        $goodsCategoryCarouselWidget->handle([
           'model' => $category,
           'description' => 'Закажи прямо сейчас и получи выгоду.'
        ])
    !!}

    <div class="lead mt-3 mb-4">
        {!! $category->description !!}
    </div>

    <div class="card card-legend card-withShadow">
        <div class="legend-title legend-title-blue">
            Вам может пригодится
        </div>
        <div class="card-body">
            {!! $goodsCategoriesWidget->handle(['exclude_id' => $category->id]) !!}
        </div>
    </div>

    <h2 class="h1 text-center my-4">Наша продукция</h2>

    <div class="section_divider mb-2">
        <span class="section_divider_title">Фальцевая кровля</span>
    </div>

    <div class="row mb-3">
        @foreach($category->goods as $goods)
            <div class="col-md-6">
                {!! $goodsCard->handle(['view' => 'roof-row', 'goods' => $goods]) !!}
            </div>
        @endforeach
    </div>
    @foreach($category->children as $child)
        <div class="section_divider mb-2">
            <span class="section_divider_title">{{ $child->name }}</span>
        </div>
        <div class="row mb-3">
            @foreach($child->goods as $goods)
                <div class="col-md-4">
                    {!! $goodsCard->handle(['view' => 'roof-card', 'goods' => $goods]) !!}
                </div>
            @endforeach
            <div class="col-md-4">
                <div class="card card-goods mb-3">
                    <ul class="roofs_rals_columns">
                        @foreach(config('trade.RALS') as $ral)
                            <li class="roofs_rals_item  {{ 'roofs_rals-' . $ral }}">{{ $ral }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endforeach

    @include('sections.roof-advantages')
@endsection

@section('bottom')
    @include('sections.roof-about-work')
    @include('sections.roof-services')
    <div class="my-4"></div>
    @include('sections.feedback-red')
    @include('sections.our-works')
@endsection

@section('aside_right')
    {!! $feedbackFormWidget->handle([
        'view' => 'feedback',
        'action_url' => route('frontend.content.feedback.backcall'),
        'title' => 'Оставьте заявку и получите скидку на наши услуги'
    ]) !!}

    <div class="my-3">
        @foreach($category->attachments as $attachment)
            <a href="{{ $attachment->url }}" class="btn btn-danger btn-block">{{ $attachment->original_name }}</a>
        @endforeach
    </div>

    {!! $postListWidget->handle([
        'view' => 'vertical',
        'category_slug' => 'articles'
    ]) !!}
@endsection

