@extends('trade::frontend.catalog.categories.default')

@inject('postListWidget', 'MaxTor\Content\Widgets\PostListWidget')
@inject('feedbackFormWidget', 'MaxTor\Content\Widgets\FeedbackFormWidget')
@inject('goodsCard', 'MaxTor\Trade\Widgets\GoodsCardWidget')
@inject('goodsListWidget', 'MaxTor\Trade\Widgets\GoodsListWidget')
@inject('goodsCategoriesWidget', 'MaxTor\Trade\Widgets\GoodsCategoriesWidget')
@inject('goodsCategoryCarouselWidget', 'MaxTor\Trade\Widgets\GoodsCategoryCarouselWidget')

@section('header_top_phone')
    @include('helpers.phone', ['phone' => site_config_extra('company_phones.roof.value')])
@endsection

@section('title')
    {{ $category->name }}
@endsection

@section('content')
    @parent

    {!!
    $goodsListWidget->handle([
        'title' => 'Дома и цены',
        'description' => 'Большой выбор проектов домов из двойного и мини бруса. А так же примеры их реализации. Благодаря нашему удобному каталогу Вы легко найдете то, что вам нужно.',
        'category_id' => $category->id,
        'view' => 'rows',
        'card-view' => 'row',
    ])
    !!}

    @include('sections.wooden-house-building-advantages')

@endsection

@section('bottom')
    <div class="container">
        @include('sections.wooden-house-building-services')
        @include('sections.wooden-house-building-about-work')
        @include('sections.wooden-house-building-partners')
    </div>
    @include('sections.feedback-red')
@endsection

@section('aside_right')
    @parent
@endsection
