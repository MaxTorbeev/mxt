@extends('layouts.app')

@inject('goodsCard', 'MaxTor\Trade\Widgets\GoodsCardWidget')
@inject('feedbackFormWidget', 'MaxTor\Content\Widgets\FeedbackFormWidget')
@inject('goodsCategoriesWidget', 'MaxTor\Trade\Widgets\GoodsCategoriesWidget')

@section('top')
    @include('sections.about-brand')
@endsection

@section('content_top')
    <div class="section_divider mt-4 mb-2">
        <span class="section_divider_title">Услуги</span>
    </div>

    <div class="row">
        <div class="col-md-8 col-lg-9">
            {{--Services carousel widget--}}
            <div class="swiper-container"
                 data-slides
                 data-direction="horizontal"
                 data-free-mode="true"
                 data-slides-per-view="1"
                 data-space-between="20"
            >
                <div class="swiper-wrapper">
                    <div class="card card-withImageOverlay bg-dark text-white">
                        <img class="card-img" src="{{ asset('images/templates/esh/carousel/roof.jpg') }}" alt="">
                        <div class="card-img-overlay card-img-overlay-center px-5 py-5">
                            <h2 class="card-title">Собственное производство фальцевой кровли в Казани</h2>
                            <p class="card-text">Закажи прямо сейчас и получи выгоду.</p>
                            <p class="card-text">
                                <a href="#" class="btn btn-success px-4">
                                    Узнать подробнее <i class="fas fa-angle-right"></i>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="sliderNavigators sliderNavigators-inWrapper swiper-navigation">
                    <div class="prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
                    <div class="next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
                </div>
            </div>

            {{--Services cards widget--}}
            {!! $goodsCategoriesWidget->handle(['cacheKey' => 'homepage']) !!}
        </div>
        <div class="col-md-4 col-lg-3">
            {{--Contact card widget--}}
            <div class="card bg-light card-contact card-sticky card-sticky-top-0 mb-4">
                <div class="card-body">
                    <div class="contact_position">
                        {{ site_config_extra('company_phones.common.label') }}
                    </div>
                    <div class="phone mb-3">
                        @include('helpers.phone', ['phone' => site_config_extra('company_phones.common.value')])
                    </div>
                    <a onclick='event.preventDefault(); modalShow("back-call",  @json(["apiUrl" => route("frontend.content.feedback.backcall")]))'
                       class="btn btn-success btn-block">
                        Заказать обратный звонок
                    </a>
                </div>
            </div>

            {!! $feedbackFormWidget->handle([
                'view' => 'feedback',
                'title' => 'Оставьте заявку и получите скидку на наши услуги'
            ]) !!}
        </div>
    </div>
@endsection

@section('content')
    @include('sections.about')
    @include('sections.roofs', ['model' => $roofCategory])
    @include('sections.houses-building', ['model' => $woodenHousesBuilding])
    @include('sections.special-machinery', ['model' => $rentalSpecialEquipment])
    @include('sections.ventilation')
@endsection

@section('bottom')
    @include('sections.feedback-red')
    @include('sections.content-posts')
    @include('sections.our-works')
@endsection


