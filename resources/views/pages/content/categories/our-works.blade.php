@extends('content::frontend.categories.show')

@inject('postWidget', 'MaxTor\Content\Widgets\PostWidget')

@section('title')
    {{ $category->name }}
@endsection

@section('content')
    <h1 class="h1">{{ $category->name }}</h1>
    <div class="row">
        @foreach($categoryPosts as $post)
            <div class="col-12 col-md-6  mb-4">
                {!! $postWidget->handle(['view' => 'grid', 'model' => $post]) !!}
            </div>
        @endforeach
    </div>
@endsection