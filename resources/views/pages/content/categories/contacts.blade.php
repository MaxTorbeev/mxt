@extends('content::frontend.categories.show')

@inject('feedbackFormWidget', 'MaxTor\Content\Widgets\FeedbackFormWidget')
@inject('goodsCategoriesWidget', 'MaxTor\Trade\Widgets\GoodsCategoriesWidget')

@section('content')
    <div class="row">
        <div class="col-md-5">
            <h1 class="h1 text-bold">{{ $category->name }}</h1>
            <div class="summary">
                <ul class="summary-list">
                    <li class="summary_row">
                        <div class="summary_title">{{ site_config('company_address')->label }}</div>
                        <div class="summary_value">
                            {{ site_config('company_address')->value }}
                        </div>
                    </li>
                    @foreach(site_config('company_phones')->extra_attributes as $phone)
                        <li class="summary_row">
                            <div class="summary_title">{{ $phone['label'] }}</div>
                            <div class="summary_value">
                                <a href="tel:{{ parse_phone($phone['value'], true, ['plain' => true]) }}">
                                    {{ parse_phone($phone['value']) }}
                                </a>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>

            <div class="row">
                @foreach($category->attachments as $attachment)
                    <div class="col-md-6">
                        <a href="{{ $attachment->url }}" class="btn btn-info btn-sm btn-block"
                           title="{{ $attachment->name }}" role="button" target="_blank">
                            {{ $attachment->original_name }}
                        </a>
                    </div>
                @endforeach
            </div>

            <h2 class="h2 mt-4 text-bold">Об услугах</h2>

            {!! $goodsCategoriesWidget->handle(['cacheKey' => 'contactpage', 'view' => 'row']) !!}
        </div>
        <div class="col-md-7">
            <h2 class="h1 text-bold">Мы на карте</h2>

            <div class="yandex-map" data-map-coordinate="55.854077,49.117472" data-zoom="9" style="height: 400px"></div>

            <h2 class="h1 mt-4 text-bold">Обратная связь</h2>

            {!! $feedbackFormWidget->handle([
                'view' => 'feedback-contact-page',
            ]) !!}
        </div>
    </div>
@endsection

@section('aside_right')@endsection