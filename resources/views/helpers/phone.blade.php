<a href="tel:{{ parse_phone($phone, true, ['plain' => true]) }}" class="phone">
    {{ parse_phone($phone, true, ['asArray' => true])['code'] }}
    {{ parse_phone($phone, true, ['asArray' => true])['area'] }}
    <span class="phone-large">
        {{ parse_phone($phone, true, ['asArray' => true])['prefix'] }}
        {{ parse_phone($phone, true, ['asArray' => true])['number'] }}
    </span>
</a>
