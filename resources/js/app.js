import './bootstrap';

import Vue from 'vue';
import notify from 'Core/Notification'
import VModal from 'vue-js-modal'

import ModalDialog from 'Components/MXTCore/ModalDialog'
import BackCall from 'Components/MXTCore/Feedbacks/BackCall'
import Consultation from 'Components/MXTCore/Feedbacks/Consultation'
import AddToCart from 'Components/Trade/Goods/Components/AddToCart'

window.Vue = Vue;
window.events = new Vue();

Vue.use(VModal, {
    dynamic: true,
    dynamicDefaults: {clickToClose: false},
    injectModalsContainer: true,
});

new Vue({
    el: '#app',
    components: {
        AddToCart,
        ModalDialog,
        Consultation
    }
});

window.flash = function (message, level = 'success') {
    notify.create(message, level).show();
};

window.flash = function (message, level = 'success') {
    notify.create(message, level).show();
};

window.modalShow = function (component = 'back-call', attributes = null) {
    window.events.$emit('modalDialog:open', {component, attributes});
};
