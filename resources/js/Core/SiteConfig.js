class SiteConfig {

    constructor(configs) {
        this.configs = configs;
    }

    get(configName = null, attribute = 'value') {
        let config = this.configs.filter(function (currentValue) {
            if (currentValue.name === configName)
                return currentValue;
        });

        if (config.length) {
            return config[0][attribute];
        }
    }
}

export default SiteConfig;
