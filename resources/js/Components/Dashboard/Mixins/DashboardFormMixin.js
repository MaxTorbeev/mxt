import Asset from 'Core/Asset'
import Form from 'Core/Form'

export default {
    props: {
        sendMethod: {
            default: 'post',
            type: String
        },
        sendUrl: {
            default: null,
            type: String
        },
        deleteUrl: {
            default: '',
            type: String
        }
    },
    data() {
        return {
            form: new Form({}),
            item: {
                default: null,
                type: Object
            },
            hasLoaded: {
                default: false,
                type: Boolean
            },
            params: {
                published: {
                    0: 'Нет',
                    1: 'Да'
                }
            }
        }
    },

    methods: {
        onSubmit() {
            if(!this.sendUrl) {
                flash('Необходимо указать sendUrl', 'error');
                return;
            }

            this.form.submit(this.sendMethod.toLowerCase(), this.sendUrl)
                .then((response) => {

                    this.sendMethod = 'put';

                    this.form = new Form(response.data);
                })
                .catch(errors => {
                    flash(errors.message, 'error')
                })
        },
    },
}