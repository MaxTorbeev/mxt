import Vue from 'vue'
import Router from 'vue-router'

import FlatsPage from './../Pages/FlatsPage'
import RoomsPage from './../Pages/RoomsPage'
import HousesPage from './../Pages/HousesPage'
import LandPlotsPage from './../Pages/LandPlotsPage'
import CommercialPage from './../Pages/CommercialPage'
import GaragesPage from './../Pages/GaragesPage'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/flats',
            name: 'flats',
            component: FlatsPage,
            props: {
                category: 'flats',
            }
        },
        {
            path: '/rooms',
            name: 'rooms',
            component: RoomsPage,
            props: {
                action: '/',
                method: 'post'
            }
        },
        {
            path: '/houses',
            name: 'houses',
            component: HousesPage,
            props: {
                action: '/',
                method: 'post'
            }
        },
        {
            path: '/land-plots',
            name: 'land-plots',
            component: LandPlotsPage,
            props: {
                action: '/',
                method: 'post'
            }
        },
        {
            path: '/commercial',
            name: 'commercial',
            component: CommercialPage,
            props: {
                action: '/',
                method: 'post'
            }
        },
        {
            path: '/garages',
            name: 'garages',
            component: GaragesPage,
            props: {
                action: '/',
                method: 'post'
            }
        },
    ]
})