import AbstractNativeComponent from 'Core/NativeComponent'
import Mmenu from 'mmenu-js/src/mmenu'
import 'mmenu-js/src/mmenu.scss'

class MMenu extends AbstractNativeComponent {
    init() {
        if (!this.elements.length) {
            return;
        }

        new Mmenu(this.selector, {
            "extensions": [
                "position-right"
            ]
        });
    }
}

export default MMenu;
