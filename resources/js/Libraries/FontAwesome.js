import AbstractNativeComponent from 'Core/NativeComponent';
import Asset from 'Core/Asset';

import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/fontawesome'

export default class FontAwesome extends AbstractNativeComponent {
    init() {}
}