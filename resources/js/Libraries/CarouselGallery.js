import AbstractNativeComponent from 'Core/NativeComponent'
import Swiper from 'swiper'
import 'swiper/dist/css/swiper.min.css'

export default class CarouselGallery extends AbstractNativeComponent {
    init() {
        const galleryThumbs = new Swiper('.gallery-thumbs', {
            direction: 'vertical',
            spaceBetween: 10,
            slidesPerView: 4,
            loop: true,
            freeMode: true,
            // watchSlidesVisibility: true,
            // watchSlidesProgress: true,
            navigation: {
                nextEl: '.gallery-thumbs .swiper-navigation .next',
                prevEl: '.gallery-thumbs .swiper-navigation .prev'
            },
            breakpoints: {
                768: {
                    direction: 'horizontal',
                },
                480: {
                    direction: 'horizontal',
                    slidesPerView: 2,
                }
            },
            on: {
                init: () => {
                    // element.classList.add('d-hasBeenLoad')
                },
            }
        });

        new Swiper('.gallery-top', {
            spaceBetween: 10,
            // loop: true,
            // loopedSlides: 5, //looped slides should be the same
            navigation: {
                nextEl: '.gallery-top .swiper-navigation .next',
                prevEl: '.gallery-top .swiper-navigation .prev'
            },
            breakpoints: {
                480: {
                    slidesPerView: 1,
                }
            },
            thumbs: {
                swiper: galleryThumbs,
            },
        });
    }
}

