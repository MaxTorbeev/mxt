import * as moment from 'moment';

import AbstractNativeComponent from 'Core/NativeComponent'
import 'bootstrap4-datetimepicker/src/js/bootstrap-datetimepicker'
import 'bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.min.css'
import $ from 'jquery'

class Datepicker extends AbstractNativeComponent {
    init() {

        this.elements.forEach((element) => {
            element.setAttribute('autocomplete', 'off')
        });

        $(this.selector).datetimepicker({
            locale: 'ru',
            viewMode: 'days',
            format: this.dateFormat(),
            icons: {
                time: 'fa fa-clock-o',
                up: 'fa fa-chevron-circle-up',
                down: 'fa fa-chevron-circle-down',
                next: 'fa fa-chevron-circle-right',
                previous: 'fa fa-chevron-circle-left',

                today: 'fa fa-calendar-o',
                date: 'fa fa-calendar',
                clear: 'fa fa-eraser',
                close: 'fa fa-times-circle',
            }
        }).on('dp.change', function (e) {
            // let specifiedDate = new Date(e.date);

            // console.log(this.data('default-date'));
        });
    }

    getDefaultDate() {
        return new Date()
    }

    dateFormat() {
        if ($(this.selector).data('timepicker'))
            return 'YYYY-MM-DD H:mm:ss';

        return 'YYYY-MM-DD'
    }
}

export default Datepicker;
