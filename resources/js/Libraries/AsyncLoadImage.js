import AbstractNativeComponent from 'Core/NativeComponent';

/**
 * class AsyncLoadImage.
 *
 * @package NativeComponent
 * @see https://developers.google.com/web/fundamentals/performance/lazy-loading-guidance/images-and-video/
 */
export default class AsyncLoadImage extends AbstractNativeComponent {
    init() {
        let lazyImages = [].slice.call(document.querySelectorAll(this.selector));

        if (window.IntersectionObserver) {
            let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
                entries.forEach(function(entry) {
                    if (entry.isIntersecting) {
                        let lazyImage = entry.target;
                        lazyImage.src = lazyImage.dataset.src;
                        lazyImage.srcset = lazyImage.dataset.srcset;
                        lazyImage.classList.remove("lazy");
                        lazyImageObserver.unobserve(lazyImage);
                    }
                });
            });

            lazyImages.forEach(function(lazyImage) {
                lazyImageObserver.observe(lazyImage);
            });
        } else {
            this.elements.forEach((element) => {
                let img = new Image();

                element.src = element.dataset.asyncLoadSrc;
                element.alt = element.dataset.asyncLoadAlt;
            })
        }
    }
}
