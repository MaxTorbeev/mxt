import AbstractNativeComponent from 'Core/NativeComponent'

class Tooltip extends AbstractNativeComponent {
    init() {
        $(this.selector).tooltip()
    }
}

export default Tooltip;