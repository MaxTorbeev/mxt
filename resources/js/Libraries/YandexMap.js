import Asset from 'Core/Asset'
import AbstractNativeComponent from 'Core/NativeComponent'

export default class YandexMap extends AbstractNativeComponent {
    beforeInit() {
        this.elements.forEach((element) => {
            element.style.height = element.dataset.mapHeight;
            element.style.overflow = 'hidden';
        })
    }

    init() {
        if (window.ymaps === undefined) {
            Asset.scriptUrl('//api-maps.yandex.ru/2.1/?lang=ru_RU')
                .then(() => ymaps.ready(() => this.initMap()));
        } else {
            ymaps.ready(() => this.initMap());
        }
    }

    initMap() {
        this.elements.forEach((element) => {
                let coordinates = element.dataset.mapCoordinate.split(',');

                let map = new ymaps.Map(element, {
                    center: coordinates,
                    zoom: element.dataset.zoom || 9
                });

                // Создание экземпляра маршрута.
                let multiRoute = new ymaps.multiRouter.MultiRoute({
                    // Точки маршрута.
                    // Обязательное поле.
                    referencePoints: [
                        [55.850343, 49.110975],
                        coordinates,
                    ]
                }, {
                    // Автоматически устанавливать границы карты так,
                    // чтобы маршрут был виден целиком.
                    boundsAutoApply: true
                });

                // Добавление маршрута на карту.
                map.geoObjects.add(multiRoute);
            }
        )
    }
}
