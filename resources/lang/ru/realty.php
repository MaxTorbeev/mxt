<?php

return [

//    Результаты поиска: найдено {{ $realty->total() }} предложений
    'search' => [
        'results' => 'Результаты поиска: найдено :count предложений'
    ],

    'apartment' => [
        'name' => ':rooms-комнатная квартира, :area'
    ],

    'studio' => [
        'name' => 'студия, :area'
    ],

    'cost' => [
        'cost-per-unit' => '~ :value руб. за 1 :unit',
        'total-cost' => ':value руб.'
    ],

    'areas' => [
        'area' => 'Площадь',
        'room-space' => 'Жилая площадь',
        'kitchen-space' => 'Площадь кухни',
        'lot-area' => 'Площадь участка',
        'units' => [
            'сотка' => '{-1} сотку|{1} сотка|[2,4] :count сотки|[5,*] :count соток',
            'квм' => '{-1} м²|[1.*]:count м²'
        ],
        'units-only' => [
            'сотка' => '{-1} сотку|{1} сотка|[2,4] :count сотки|[5,*] :count соток',
            'квм' => ':count м²'
        ]
    ],
    'dwelling_description' => [
        'apartments' => 'Апартаменты',
        'studio' => 'Студия',
        'open_plan' => 'Свободная планировка',
        'rooms_type' => 'Тип комнат',
        'window_view' => 'Вид из окон',
        'balcony' => 'Тип балкона',
        'bathroom_unit' => 'Тип санузла',
        'air_conditioner' => 'Наличие системы кондиционирования',
        'phone' => 'Наличие телефона',
        'internet' => 'Наличие интернета',
        'room_furniture' => 'Наличие мебели',
        'kitchen_furniture' => 'Наличие мебели на кухне',
        'television' => 'Наличие телевизора',
        'washing_machine' => 'Наличие стиральной машины',
        'dishwasher' => 'Наличие посудомоечной машины',
        'refrigerator' => 'Наличие холодильника',
        'built_in_tech' => 'Встроенная техника',
        'floor_covering' => 'Покрытие пола',
        'with_children' => 'Проживание с детьми',
        'with_pets' => 'Проживание с животными',
    ],
    'building_description' => [
        'building_section' => 'Корпус дома',
        'ceiling_height' => 'Высота потолков в метрах',
        'guarded_building' => 'Закрытая территория',
        'pmg' => 'Возможность ПМЖ',
        'access_control_system' => 'Наличие пропускной системы',
        'lift' => 'Лифт',
        'rubbish_chute' => 'Мусоропровод',
        'electricity_supply' => 'Электричество',
        'water_supply' => 'Водопровод',
        'gas_supply' => 'Газ',
        'sewerage_supply' => 'Канализация',
        'heating_supply' => 'Отопление',
        'toilet' => 'Туалет',
        'shower' => 'Душ',
        'pool' => 'Бассейн',
        'billiard' => 'Бильярд',
        'sauna' => 'Сауна',
        'parking' => 'Наличие охраняемой парковки',
        'parking_places' => 'Количество предоставляемых парковочных мест',
        'parking_place_price' => 'Стоимость парковочного места',
        'parking_guest' => 'Наличие гостевых парковочных мест',
        'parking_guest_places' => 'Количество гостевых парковочных мест',
        'alarm' => 'Наличие сигнализации в доме',
        'flat_alarm' => 'Наличие сигнализации в квартире',
        'security' => 'Наличие охраны',
        'is_elite' => 'Элитная недвижимость',
    ],
    'garage_description' => [
        'garage_name' => 'Название гаражно-строительного кооператива',
        'parking_type' => 'Тип парковки',
        'heating_supply' => 'Наличие отопления',
        'electricity_supply' => 'Наличие электроснабжения',
        'water_supply' => 'Наличие водопровода',
        'security' => 'Наличие охраны',
        'automatic_gates' => 'Наличие автоматических ворот',
        'cctv' => 'Наличие видеонаблюдения',
        'fire_alarm' => 'Наличие пожарной сигнализации',
        'access_control_system' => 'Наличие пропускной системы',
        'inspection_pit' => 'Наличие смотровой ямы',
        'cellar' => 'Наличие подвала или погреба',
        'car_wash' => 'Наличие автомойки',
        'auto_repair' => 'Наличие автосервиса',
        'new_parking' => 'Признак гаража в новостройке',
    ]
];
